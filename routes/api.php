<?php

use Illuminate\Http\Request;
use Moodhu\Repositories\AccommodationRepository;
use Moodhu\Repositories\SearchFieldsRepository;
use Moodhu\Repositories\SpecialOfferRepository;
use Moodhu\Repositories\TourRepository;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('/bookings', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::post('/customize', function(Request $request, AccommodationRepository $repository)
{
    $params = [];
    if($request->has('current_page'))
    {
        $params['CurrentPage'] = $request['current_page'];
    }
    if($request->has('from_date'))
    {
        $params['StartDate'] = \Carbon\Carbon::parse($request->get('from_date'))->getTimestamp();
        $params['EndDate'] = \Carbon\Carbon::parse($request->get('to_date'))->getTimestamp();
    }

    if($request->has('number_of_adults'))
    {
        $params['UnitAttributeFilterList']['AttributeFilter'] = [
            [
                'AttributeID' => 120,
                'AttributeValue' => $request->get('number_of_adults'),
                'ComparisonType' => 'GreaterOrEqualThan'
            ]
        ];

    }

    if($request->has('number_of_children'))
    {
        $params['children'] = $request->get('number_of_children');
    }

    if($request->has('category_id'))
    {
        $params['categoryID'] = $request->get('category_id');
    }

    return $repository->paginate($params);
});

Route::get('/filters', function(SearchFieldsRepository $repository){
    return $repository->getLists();
});

Route::get('/special-offers', function(SpecialOfferRepository $repository)
{
    return $repository->getSpecialOffers([

    ]);
});

Route::get('/tours', function(TourRepository $repository)
{
    return $repository->getTours([

    ]);
});


Route::get('/accommodations', function(AccommodationRepository $repository, Request $request)
{
    $pageSize = $request->get('page-size');
    $currentPage = $request->get('current-page');
    return $repository->paginate(['PageSize' => $pageSize, 'CurrentPage'=>$currentPage]);
});

Route::get('/accommodation/{id}', function(AccommodationRepository $repository, $id)
{
    return $repository->getDetails($id);
});

Route::get('/tour/{id}', function(TourRepository $repository, $id)
{
    return $repository->getDetails($id);
});