<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Moodhu\Repositories\AccommodationRepository;
use Moodhu\Repositories\SpecialOfferRepository;
use Moodhu\Repositories\TourRepository;
use Illuminate\Http\Request;

require_once __DIR__.'/../ITravel/iTravelAPI.php';
Route::get('/', ['role'=>'*', function () {
    return view('welcome');
}]);


Route::get('/customized', function(Request $request, AccommodationRepository $repository) {
    $filters = $request->all();

    $params = [];

    if ($request->has('current_page')) {
        $params['CurrentPage'] = $request['current_page'];
    }
    if ($request->has('from_date')) {
        $params['StartDate'] = \Carbon\Carbon::parse($request->get('from_date'))->getTimestamp();
        $params['EndDate'] = \Carbon\Carbon::parse($request->get('to_date'))->getTimestamp();
    }

    if ($request->has('name'))
    {
        $params['ObjectAttributeFilterList']['AttributeFilter'] = [
            [
                'AttributeID' => 121,
                'AttributeValue' => $request->get('name'),
                'ComparisonType' => 'Like'
            ]
        ];
    }
    if($request->has('number_of_adults'))
    {
        $params['UnitAttributeFilterList']['AttributeFilter'] = [
            [
                'AttributeID' => 120,
                'AttributeValue' => $request->get('number_of_adults'),
                'ComparisonType' => 'GreaterOrEqualThan'
            ]
        ];

    }

    if($request->has('number_of_children'))
    {
        $params['children'] = $request->get('number_of_children');
    }

    if($request->has('category_id'))
    {
        $params['categoryID'] = $request->get('category_id');
    }

    $data = [
        'data' => []
    ];

    $request->session()->flush('message');

    try
    {
        $data = $repository->paginate($params);

    }
    catch (Exception $e)
    {
        $request->session()->flash("message", $e->getMessage());
    }



	return view('customized', array_merge($filters,['data'=>$data, 'filters' => $filters]));
});

Route::get('/destinations', function()
{
	$s = new \GetAllDestinations();
	dd ($s->GetAllDestinations()->GetAllDestinationsResult);
});

Route::get('/test', function()
{
	$s = new \GetSearchResults();
	$SearchResults = $s->GetSearchResults();
	$results =  $SearchResults->GetSearchResultsResult->AccommodationObjectList->AccommodationObject;

	dd($SearchResults);

	return $s->GetJSON();

	$response = [];

	if(count($results) > 0)
	{
		foreach ($results as $item) {

			
			$response[] = formatResponse($item);// $hotel;

			

		}
	}

	// return [];
	return $response;
	// return view('mails.correction_approved', [ 'model' => \App\Correction::first()]);
});

Route::get('/inquiry/{id}', ['as'=>'book', function($id)
{
    $start = (\Carbon\Carbon::now()->timestamp* 10000000) + 621355968000000000;
    $end = (\Carbon\Carbon::now()->timestamp* 10000000) + 621355968000000000;

    $persons = '';

    if(request()->has('from_date'))
    {
        $start = (\Carbon\Carbon::parse(request()->get('from_date'))->timestamp * 10000000) + 621355968000000000;
    }

    if(request()->has('to_date'))
    {
        $end = (\Carbon\Carbon::parse(request()->get('to_date'))->timestamp * 10000000) + 621355968000000000;
    }


    if(request()->has('number_of_rooms') && request()->has('number_of_adults'))
    {
        $no_of_peeps_per_room = request()->has('number_of_adults') / request()->has('number_of_rooms');
        
        $persons = (\Carbon\Carbon::parse(request()->get('to_date'))->timestamp * 10000000) + 621355968000000000;
    }

    

	return view('book',['UnitID'=>$id, 'start' => $start, 'end' => $end]);
}]);

Route::get('/accommodation/{id}', function(AccommodationRepository $repository, $id)
{
	$model = $repository->getDetails($id);
	return view('details.accommodation', compact('model'));
});

Route::get('/tour/{id}', function(TourRepository $repository, $id)
{
	$model = $repository->getDetails($id);
	return view('details.tour', compact('model'));
});




Route::get('/terms_and_conditions', function()
{
    return view('terms_and_conditions');
});

Route::get('/like_and_win', function()
{
    return view('like_and_win');
});


Route::post('/like_and_win', function(\Moodhu\Repositories\CustomerRepository $repository)
{

    request()->flash();
    $client = new GuzzleHttp\Client();
    $res = $client->post('https://www.google.com/recaptcha/api/siteverify',
        ['form_params' =>  [
            'secret' => '6Ldb-xsUAAAAAC7sdGglhqlFHaS_427_HpiOU7as',
            'response' => request()->get('g-recaptcha-response'),
            'remoteip' => request()->ip()
        ]]
    );
    $item =  \GuzzleHttp\json_decode($res->getBody());

    if($item->success)
    {
        if($repository->createCustomer(request()->all()))
        {
            return view('liked');
        }
        else
        {
            request()->session()->flash("exists", "A customer exists with the details you have provided.");

        }
    }
    else
    {
        request()->session()->flash("error", "Need the human verification done!");
    }

    return view('like_and_win')->withInput(request()->all());
});

//


Route::get('/privacy_policy', function()
{
    return view('privacy_policy');
});


Auth::routes();

Route::group(['prefix'=>'tools'], function()
{
    Route::get('/customer', function(\Moodhu\Repositories\CustomerRepository $rep)
    {
        $countries = app('api')->GetAllCountries([
            'getAllCountriesParameter' => [ 'SearchQuery' => 's' ]
        ])->GetAllCountriesResult->CountryList;


        foreach ($countries as $country) {
            dd($country,0);
        }

        dd();
        $cus = new CustomerInsert();
        dd($cus->InsertCustomer(), $rep->createCustomer(['name' => 'Yameen','mobile_number' => '9696470','country_id' => 54]));
//        dd();
    });
});


Route::get('/page/{slug}', [ 'as' => 'page', 'uses' => 'PageController@page']);
Route::any('/register','HomeController@index');


Route::group(['prefix'=>'admin'], function()
{
    // Auth::routes();
    Route::get('/login', 'Auth\LoginController@showLoginForm' );
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout');

    Route::group(['middleware'=>'auth'], function()
    {
        Route::get('/pages/{pages}/destroy/confirm',['as'=>'pages.confirm','uses'=>'PageController@confirm']);
        Route::resource('/pages','PageController',['only' => ['index', 'show', 'create', 'store','edit', 'update', 'destroy','confirm']]);
        Route::get('/navigations/{navigations}/destroy/confirm',['as'=>'navigations.confirm','uses'=>'NavigationController@confirm']);
        Route::resource('/navigations','NavigationController',['only' => ['index', 'show', 'create', 'store','edit', 'update', 'destroy','confirm']]);


        // Route::get('login' , 'Auth\AuthController@getLogin' ) ;
        // Route::post('registrar', 'Auth\AuthController@postLogin') ;
        Route::get('/', function()
        {
            return view('home');
        });

    });

    Route::get('/norole', function()
    {
        return view('auth.norole');
    });
});





