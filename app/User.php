<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use baseFunctions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','mobile', 'password','role','department'
    ];
    public $search = ['name','email'];

    public function corrections()
    {
        return $this->hasMany('App\Correction','created_by');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($roles)
    {

        if(gettype($roles) == 'array')
            return in_array($this->role, $roles);

        // dd($roles, $this->role);

        return $roles == '*' ? true : ($this->role == $roles);
    }

    public function info()
    {
        return sprintf("User: %s <br> Email: %s", $this->name, $this->email);
    }

}
