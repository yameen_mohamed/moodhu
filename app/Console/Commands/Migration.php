<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Migration extends BaseCommand
{


    protected $properties;
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return 'Stubs.migration';
    }


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:migration 
                            {model : Model name singular E.g. User}
                            {--fields= : These are the fields. E.g. string:name, integer:age}
                            {--soft-deletes=true}
                            {--s|show}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This generates a migration for a model.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = Str::title($this->argument('model'));
        $this->properties['model'] = $model;
        $this->info("$model loaded");
        $name = $this->getNamespace();
        
        $fields = [];
        $field = $this->option('fields');
            if(!is_null($field)){
                foreach (explode(', ', $field) as $word) {
                    $keyVal = explode(':', $word);
                    if(count($keyVal) == 2)
                        $fields[$keyVal[1]] = $keyVal[0];
                    else if(count($keyVal) == 3 || count($keyVal) == 4 || count($keyVal) == 5)
                        $fields[$keyVal[1]] = $keyVal;
                    else{
                        $this->error("Check the fields. Mistake in the field: {$keyVal[1]}");
                        exit;
                    }

                }
            }



        $args = [
            'pluralized_model' => Str::plural($model),
            'lower_pluralized_model' => Str::lower(Str::plural($model)),
            'id' => Str::plural($model),
            'softDeletes' => $this->option('soft-deletes'),
            'fields' => $fields
        ];
        
        $root = realpath(__DIR__.'/../../../database/migrations//');
        $filePrefix = date('Y_m_d_Hmis');
        $filename = "{$root}/{$filePrefix}_create_".Str::snake(Str::plural($model))."_table.php";
        $this->pushToFile($filename, $this->runTemplate($args));
        $this->info("Migration for $model successfully created.");

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the class'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['fields', 'f', InputOption::VALUE_REQUIRED, 'Specify column details such as datatype:fieldname(:nullable|:foreigntable(:nullable|:forignkey(:nullable))).', null], 
            ['root', 'r', InputOption::VALUE_REQUIRED, 'Spcifies the root location of the model to be created.', null], 
            ['show', 's', InputOption::VALUE_NONE, 'Just display on the console rather than write to disk.', null], 
        ];
    }


    private function ScanFileForClass($class){
        $migrationDirectory = base_path().'/database/migrations';
        foreach (glob("$migrationDirectory/*.php") as $file) {
            $content = file_get_contents($file);
            $classes = $this->get_php_classes($content);
            if(in_array($class, $classes))
                return $file;
        }
        $this->error("Could not find any such class such as '$class'");
        exit; 
    }

    function get_php_classes($php_code)
    {
        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++)
        {
            if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING && !($tokens[$i - 3] && $i - 4 >= 0 && $tokens[$i - 4][0] == T_ABSTRACT))
            {
                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
            }
        }
        return $classes;
    }
}
