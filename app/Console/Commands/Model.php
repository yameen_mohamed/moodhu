<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Model extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:model {model} 
                            {--root : Specify the directory relative to the root to store the generated file }
                            {--s|show}
                            ';

   /**
     * Get the stub file for the generator.
     *
     * @return string
     */

    protected $table;
    protected function getStub()
    {
        return 'Stubs.model';
    }

    public function getTable()
    {
        return $this->table;
    }

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Helps developer to put the model from db.';
    


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->argument('model');
        $this->table = Str::snake(Str::plural($model));


        $this->info("{$this->table} being located ");
        
        $fields = [];
        $foreignFields = [];
        foreach ($this->getProperties() as $key => $value) {
            if(in_array($value->column_name, $this->systemAttr))
                continue;
            if(!$value->IsPK){
                $fields[] = $value;
            }

            if(!is_null($value->fkColumn))
                $foreignFields[] = $value;
        }
        $root = __DIR__.'/../../'.$this->option('root')."/{$model}.php";
        $content = $this->runTemplate([
                'model' => $model,
                'fields'=>$fields,
                'foreignFields'=>$foreignFields,
                'dateFormat'=>'Y-m-h'
            ]);
        $this->pushToFile($root, $content);
        // dd();
    }
}
