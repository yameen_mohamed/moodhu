<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Controller extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:controller {model}
                            {--root : Specify the directory relative to the root to store the generated file }
                            {--p|prefix= : Specify the prefix added to the route. E.g. admin }
                            {--s|show}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builds the controller for the developer.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    protected function getStub()
    {
        return 'Stubs.controller';
    }

    protected $stub = "
        Route::get('/%lowerCommonName%/{%lowerCommonName%}/destroy/confirm',['as'=>'%lowerCommonName%.confirm','uses'=>'%shortClassName%Controller@confirm']);
        Route::resource('/%lowerCommonName%','%shortClassName%Controller',['only' => ['index', 'show', 'create', 'store','edit', 'update', 'destroy','confirm']]);
    ";


    public function parseStub($arguments)
    {
        foreach ($arguments as $arg=>$value) {
            $this->stub = preg_replace("/%$arg%/", $value, $this->stub);
        }
        return $this->stub;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->argument('model');
        if(class_exists($model)){

            $root = __DIR__.'/../../'.$this->option('root');
            $rclass = new \ReflectionClass($model);
            $shortClassName = $rclass->getShortName();
            $commonName = Str::plural($shortClassName);
            $lowerCommonName = Str::lower($commonName);
            $lowerProperName = Str::lower($shortClassName);
            $prefix = $this->option('prefix');
            if(strlen($prefix)>0) $prefix.='.';
            $this->pushToFile("{$root}/Http/Controllers/{$shortClassName}Controller.php", $this->runTemplate([
                    'model'=>$shortClassName,
                    'namespacedModel'=>$model,
                    'commonName' => $commonName,
                    'lowerCommonName' => $lowerCommonName,
                    'lowerProperName' => $lowerProperName,
                    'prefix'=>$prefix,
                ]));
            $this->info($this->parseStub(compact('lowerCommonName','shortClassName')));
        }else{
            $this->error("The class $model does not exist");
        }
    }
}
