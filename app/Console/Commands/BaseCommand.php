<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

abstract class BaseCommand extends Command
{

    protected $systemAttr = ['deleted_at','created_at','updated_at'];
    protected $protectedAttr = ['deleted_at','created_at','updated_at'];
    abstract protected function getStub();

    public static function datatypeBasedValidation($dataType){
        $ret = '|';
        switch ($dataType) {
            case 'int':
                $ret.='integer';
                break;
            case 'datetime':        
            case 'date':        
                $ret.='date';
                break;
            case 'varchar':     
            case 'nvarchar':        
                $ret.='';
                break;
            case 'bit':     
                $ret.='boolean';
                break;
            default:
                $ret = '';
                break;
        }
        return $ret!='|'?$ret:'';
    }


    public static function tableNameToModel($table_name){
        return Str::title(Str::singular($table_name));
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function pushtofile($loc, $content){

        if($this->option('show')){
            echo "showing: ";
            echo $content;
        }
        else{
            if(file_exists($loc)){
                if ($this->confirm('File exists! Do you wish to replace it?(if not file will be amended) [no|yes]', false)){
                    file_put_contents($loc, $content, FILE_APPEND);
                }
                else{
                    $this->writeToFile($loc,$content);
                }
            }
            else
                $this->writeToFile($loc,$content);
        }
    }

    public function runTemplate($args, $stub=null)
    {
        $args['ns'] = $this->getNamespace();

        view()->addNamespace('generator', app()->path().'/Console/Commands');
        return view()->make("generator::".(is_null($stub) ? $this->getStub() : $stub),$args);
    }
    private function writeToFile($loc, $content){
        if(!file_exists(dirname($loc))){
            $this->info("Folder does not exist. So creating...");
            mkdir(dirname($loc), 0777, true);
        }
        $this->info("Writing file: $loc");
        $file = fopen($loc, "wc") or die("Unable to open file!");
        fwrite($file, $content);
        fclose($file);
        $this->info("File created: $loc");
    }
    public function GetProperties()
    {
        $ret = \DB::raw("select a.ORDINAL_POSITION, a.data_type, a.column_name,  a.is_nullable,b.REFERENCED_COLUMN_NAME as fkColumn,
                        if(a.COLUMN_KEY='PRI',true, false) IsPK, b.REFERENCED_TABLE_NAME as referencedTable from INFORMATION_SCHEMA.COLUMNS 
                            a left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b
                        on a.TABLE_SCHEMA = b.TABLE_SCHEMA and a.table_name = b.TABLE_NAME and a.COLUMN_NAME=b.COLUMN_NAME
                        where a.table_name = '".$this->getTable()."' and a.TABLE_SCHEMA='".getenv('DB_DATABASE')."' order by a.ordinal_position");
        return \DB::select($ret);
    }

    public function getNamespace()
    {
        $composer = json_decode(file_get_contents(app()->basePath() . '/composer.json'), true);

        foreach ((array)data_get($composer, 'autoload.psr-4') as $namespace => $path) {
            foreach ((array)$path as $pathChoice) {
//                if (realpath($this->path()) == realpath(app()->basePath() . '/' . $pathChoice)) {
                    return  $namespace;
//                }
            }
        }

        throw new RuntimeException('Unable to detect application namespace.');
    }

    function autoload($className)
    {
        $className = ltrim($className, '\\');
        $fileName  = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        require $fileName;
    }
}
