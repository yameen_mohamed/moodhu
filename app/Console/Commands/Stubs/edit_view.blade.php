<?php echo '@';?>extends('layouts.app')
<?php echo '@';?>section('content')

<?php $opentag = "{{"; $closetag = "}}"; $opentagEnc = "{!!"; $closetagEnc = "!!}";?>

<form method="post" action="{{$opentagEnc}} isset($model) ? route('{{$pluralModel}}.update', $model->id) : route('{{$pluralModel}}.store') {{$closetagEnc}}">
  <div class="row">
    <div class="spread">
      <h3 class="heading">{{isset($model) ? 'Add a new' : 'Update'}} {{$lowerModel}}</h3>
    </div>
  </div>
  <fieldset>
      <div class="row">
      @foreach($fields as $field)
          <div class="col-md-6">
                <div class="form-group {{$opentagEnc}} $errors->has('{{$field->column_name}}') ? ' has-error' : '' {{$closetagEnc}}">
                    <label class=" control-label">
                      <?php echo str_replace('_', ' ', Illuminate\Support\Str::title($field->column_name)); ?>
                    </label>

                        <input type="text" class="form-control" name="{{$field->column_name}}" value="{{$opentagEnc}} old('{{$field->column_name}}', isset($model) ? $model->{{$field->column_name}} : null ) {{$closetagEnc}}" placeholder="<?php echo str_replace('_', ' ', Illuminate\Support\Str::title($field->column_name)); ?>">

                        <?php echo '@';?>if ($errors->has('{{$field->column_name}}'))
                            <span class="help-block">
                                <strong>{{$opentagEnc}} $errors->first('{{$field->column_name}}') {{$closetagEnc}}</strong>
                            </span>
                        <?php echo '@';?>endif
                </div>
          </div>
      @endforeach
        
      </div>
      <div class="row">
        <div class="form-group">
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>Add
                </button>
            </div>
        </div>
      </div>
  </fieldset>
  {{$opentagEnc}} csrf_field() {{$closetagEnc}}
  <?php echo '@';?>if(isset($model))
  <input type="hidden" name="_method" value="PUT"></input>
  <?php echo '@';?>endif
</form>
<?php echo '@';?>stop


