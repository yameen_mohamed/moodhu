<?<?php echo "php "; ?>   namespace {{ $ns }};
use App\BaseModel;
class {{ $model }} extends BaseModel {
	@include('generator::Stubs.header')
	protected $fillable = [
@foreach($fields as $field)
		'{{ $field->column_name }}',
@endforeach
	];

	protected static $rules = [
@foreach($fields as $field)
		'{{ $field->column_name }}'=>'@if($field->is_nullable == 'NO'){{ "required" }}@endif{{ \App\Console\Commands\BaseCommand::datatypeBasedValidation($field->data_type) }}',
@endforeach
	];

@foreach($foreignFields as $field)
	function {{$field->referencedTable}}(){
		return $this->belongsTo('{{$ns}}{{\App\Console\Commands\BaseCommand::tableNameToModel($field->referencedTable)}}','{{ $field->column_name }}','{{ $field->fkColumn }}');
	}
@endforeach

	public function getDates(){
		return array_merge(parent::getDates(),[@foreach($fields as $field)@if(in_array($field->data_type,['date','datetime','timestamp']))
			
			'{{ $field->column_name }}', @endif @endforeach 
			]);
	}

}
