<?php $opentag = "{{"; $closetag = "}}"; $opentagEnc = "{!!"; $closetagEnc = "!!}";?>
{{$opentagEnc}} Form::open(array('route' => array('admin.{{$pluralModel}}.store'),'files'=>true)) {{$closetagEnc}}
  <h3 class="heading">Add a new Post</h3>
  <div class="row">
@foreach($fields as $field)
    <p class="col-md-6">
 		<?php echo Illuminate\Support\Str::title($field->column_name); ?>
    <br>
          {{$opentagEnc}}
              Form::text('{{$field->column_name}}',\Input::old('{{$field->column_name}}'),['class'=>'form-control'])
          {{$closetagEnc}}
    </p>
@endforeach
  </div>
  <div>
     <p class="col-md-6">Tags<br>
      <input type="text" name="tags" data-role="tagsinput" class="form-control" value="news"></p>
    <input type="hidden" name="_token" value="{{$opentag}} csrf_token() {{$closetag}}">
  </div>
  <button>Submit</button>
</form>  

