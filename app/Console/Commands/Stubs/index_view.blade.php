<?php echo '@';?>extends('layouts.app')
<?php echo '@';?>section('content')
 <table class="table"> <?php $opentag = "{{"; $closetag = "}}";?>
 	<thead><tr>
@foreach($fields as $field)
 		<th><?php echo str_replace('_', ' ',Illuminate\Support\Str::title($field->column_name)); ?></th>
@endforeach
 		<th>Created On</th>
 		<th>Action</th>
 	</tr>
 	
	</thead>
 	<tbody>
 	<?php echo "@foreach";?>($models as $model)
 	<tr>
@foreach($fields as $field)
 		<td>{{$opentag}}$model->{{$field->column_name}}{{$closetag}}</td>
@endforeach 		
 		<td>{{$opentag}}$model->created_at{{$closetag}}</td>
 		<td>
 			<a href="/{{$pluralModel}}/{{$opentag}}$model->id{{$closetag}}/edit" class="btn btn-info btn-sm glyphicon glyphicon-pencil"></a>
 			<a href="/{{$pluralModel}}/{{$opentag}}$model->id{{$closetag}}/destroy/confirm" class="btn btn-warning btn-sm glyphicon glyphicon-trash"></a>
 		</td>
 	</tr>
 	<?php echo "@endforeach";?>
	</tbody>
</table>
<?php echo '@';?>stop
