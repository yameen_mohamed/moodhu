<?<?php echo "php "; ?>

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create{{$pluralized_model}}Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('{{$lower_pluralized_model}}', function (Blueprint $table) {
            $table->increments('id');
@foreach ($fields as $field=>$datatype)
@if(is_array($datatype))
@if(end($datatype) == 'nullable')
@if(count($datatype) == 3)
            $table->{{ $datatype[0] }}('{{ $field }}')->nullable();
@else
            $table->{{ $datatype[0] }}('{{ $field }}')->nullable()->unsigned()->index();
@if(count($datatype)==5)
            $table->foreign('{{ $datatype[1] }}')->references('{{ $datatype[3] }}')->on('{{ $datatype[2] }}');
@else
            $table->foreign('{{ $datatype[1] }}')->references('id')->on('{{ $datatype[2] }}');
@endif      
@endif
@else
            $table->{{ $datatype[0] }}('{{ $field }}')->unsigned()->index();
@if(count($datatype)==4)
            $table->foreign('{{ $datatype[1] }}')->references('{{ $datatype[3] }}')->on('{{ $datatype[2] }}');
@else
            $table->foreign('{{ $datatype[1] }}')->references('id')->on('{{ $datatype[2] }}');
@endif
@endif
@else
            $table->{{ $datatype }}('{{ $field }}');
@endif
@endforeach
@if($softDeletes == true)
            $table->softDeletes();
@endif
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('{{$lower_pluralized_model}}');
    }
}
