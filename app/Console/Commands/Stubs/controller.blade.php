<?<?php echo "php "; ?>  namespace {{$ns}}Http\Controllers;

use \Illuminate\Routing\Router;
use {{$namespacedModel}};

class {{$model}}Controller extends Controller {

	/**
	 * Holds the model
	 */
	protected $model;

	public function __construct({{$model}} $model, Router $router)
	{
		$this->model = $model;
		$this->setViewPrefix('{{$prefix}}{{$lowerProperName}}');
		parent::__construct($router);
	}

	public function confirm(${{$lowerCommonName}})
	{
		$resource = '{{$prefix}}{{$lowerCommonName}}.destroy';
		$id = ${{$lowerCommonName}};
		return view('app.confirm', compact('resource', 'id'));
	}


}
