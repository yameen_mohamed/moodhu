<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class View extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:view {model}
                            {--views=ALL : Specify view here. E.g. index|edit|add}
                            {--r|root= : Specify the directory relative to the root to store the generated file }
                            {--s|show}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates views.';

    protected $fields = [];
    protected $table ;
    protected $rootPath;
    protected $lowerModel;
    protected $model;
    protected $viewsPath;

    protected function getStub(){
        return 'Stubs.%action%_view';
    }


    public function getTable()
    {
        return $this->table;
    }
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->viewsPath = realpath(storage_path()."/../resources/views");

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->model = $this->argument('model');
        $this->rootPath = $this->option('root');
        if(strlen($this->rootPath)) $this->rootPath = "/{$this->rootPath}";
        $this->table = Str::snake(Str::plural(Str::lower($this->model)));
        $this->lowerModel = Str::snake(Str::lower($this->model));

        $this->info("{$this->table} being located ");
        
        $this->fields = [];
        foreach ($this->getProperties() as $key => $value) {
            if(in_array($value->column_name, $this->systemAttr))
                continue;
            if(!$value->IsPK){
                $this->fields[] = $value;
            }
        }
        if($this->option('views') == 'edit')
            $this->edit();
        else
            $this->index();
    }

    public function index()
    {
        $this->pushToFile("{$this->viewsPath}{$this->rootPath}/{$this->lowerModel}/index.blade.php", $this->runTemplate(
            [
                'fields'=>$this->fields,
                'pluralModel'=>  Str::plural($this->lowerModel),
            ], str_replace('%action%', 'index', $this->getStub())));
        
    }

    public function add()
    {
        $this->pushToFile("{$this->viewsPath}{$this->rootPath}/{$this->lowerModel}/add.blade.php", $this->runTemplate(
            [
                'fields'=>$this->fields,
                'lowerModel'=> $this->lowerModel,
                'pluralModel'=>  Str::plural($this->lowerModel),
            ], str_replace('%action%', 'add', $this->getStub())));
        
    }
    public function edit()
    {
        $this->pushToFile("{$this->viewsPath}{$this->rootPath}/{$this->lowerModel}/write.blade.php", $this->runTemplate(
            [
                'fields'=>$this->fields,
                'lowerModel'=> $this->lowerModel,
                'pluralModel'=>  Str::plural($this->lowerModel),
            ], str_replace('%action%', 'edit', $this->getStub())));
        
    }
}
