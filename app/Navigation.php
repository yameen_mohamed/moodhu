<?php    namespace App;
use App\BaseModel;
class Navigation extends BaseModel {
	/*
	 *	This is the eloquent model for Navigation
	 *  Author: Yameen Mohamed (yaamynu@gmail.com)
	 *  Date: Saturday 8th of April 2017 12:32:26 AM	 *
	 */
	protected $fillable = [
		'title',
		'link',
		'parent_id',
		'active',
	];

	protected static $rules = [
		'title'=>'required',
		'link'=>'required',
		'parent_id'=>'integer',
		'active'=>'',
	];

	function parent_menu(){
		return $this->belongsTo('App\Navigation','parent_id','id');
	}

	public function getDates(){
		return array_merge(parent::getDates(),[     
			]);
	}

}
