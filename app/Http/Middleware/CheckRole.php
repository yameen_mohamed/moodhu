<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get the required roles from the route
        // return next($request);

        $roles = $this->getRequiredRoleForRoute($request->route());

        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.
        if($roles == '*' || (auth()->check() && $request->user()->hasRole($roles) )|| !$roles)
        {
            return $next($request);
        }
        return redirect('/norole');
    }

    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();
        // dd($actions);
        if( array_key_exists('controller', $actions) && strstr($actions['controller'], '@') > -1)
        {
            list($controller, $action) = explode('@', $actions['controller']);
            $cntrl = \App::make($controller);


                // dd($cntrl->roles, array_keys($cntrl->roles), $action, array_key_exists($action, ($cntrl->roles)));

            if(property_exists($controller, 'roles') && array_key_exists($action, $cntrl->roles))
            {
                $roles = $cntrl->roles[$action];
                return $roles;
            }
        }

        return isset($actions['role']) ? $actions['role'] : null;
    }
}
