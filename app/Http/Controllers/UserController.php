<?php   namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use \Illuminate\Routing\Router;
use App\User;

class UserController extends cController {

	public  $roles = [
		'index' => 'admin',
		'create' => 'admin',
		'edit' =>'admin'
	];
	/**
	 * Holds the model
	 */
	protected $model;

	protected $request;

	public function __construct(User $model, Router $router, Request $req)
	{
		$this->model = $model;
		$this->setViewPrefix('user');
		$this->request = $req;
		parent::__construct($router);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return  Response
	 */
	public function admin_store()
	{
		$modelClass = get_class($this->model);
		$data = \Request::all();

		$model = new $modelClass;
		$model->fill(array_filter(\Request::all()));
		$model->password = bcrypt('P@ssw0rd');
		if ( $model->save() ){
			\Session::flash('message', 'Successfully created!');
			return \Redirect::action("\\{$this->controller}@index");			
		}
		else{
			return view("{$this->viewPrefix}.write", compact('model'))->withRequest($data)->withErrors($model->getErrors());
		}
	}

	public function confirm($users)
	{
		$resource = 'users.destroy';
		$id = $users;
		$model = $this->model->find($id);
		return view('app.confirm', compact('resource', 'id','model'));
	}

	public function reset_password()
	{
		return view('user.reset_password');
	}
	
	public function activate($id)
	{
		$user = $this->model->find($id);
		$user->approved = !$user->approved;
		$user->save();
		if($user->approved)
		{
        	\Mail::to($user)->send(new \App\Mail\AccountAcivated($user));

		}

		$this->flash('User is '. ($user->approved ? 'activated now' : 'inactivated now'  ) );
		return redirect()->back();
	}

	public function reset($id)
	{
		$user = $this->model->find($id);
		$this->flash('Password Successfully Reset.');
		$user->password = bcrypt('P@ssw0rd');
		$user->save();
		return redirect()->back();
	}

	public function profile()
	{
		$model = $this->request->user();
		return view('user.profile',compact('model'));
	}

	public function post_profile()
	{
		$user = \Auth::user();
		$validator = $user->getValidator()->make($this->request->all(), [
			'email' => ('required|email|unique:users,email,'.$user->id.',id'),
			'contact' => ["regex:/^(9|7)[0-9]{6}$/"],
			'department' => 'in:Land,Marine',
		]);

 
        if ($validator->passes())
        {
			$this->flash('Profile Updated');
			$user->mobile = $this->request->get('contact');
			$user->email = $this->request->get('email');
			$user->department = $this->request->get('department');
			$user->save();
			return redirect()->back();
        }
 
		return redirect()->back()
			->withInput($this->request->all())
			->withErrors($validator->messages());
	}

	public function post_reset_password()
	{
		$user = \Auth::user();
		$validator = $user->getValidator()->make($this->request->all(), [
			'password' => 'required|confirmed',
			'password_confirmation' => 'required',
		]);

 
        if ($validator->passes())
        {
			$this->flash('Password Successfully Changed');
			$user->password = bcrypt($this->request->get('password'));
			$user->save();
			return redirect()->back();
        }
 
		return redirect()->back()
			->withInput($this->request->all())
			->withErrors($validator->messages());
	}


}
