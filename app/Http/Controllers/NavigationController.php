<?php   namespace App\Http\Controllers;

use \Illuminate\Routing\Router;
use App\Navigation;

class NavigationController extends BaseController {

	/**
	 * Holds the model
	 */
	protected $model;

	public function __construct(Navigation $model, Router $router)
	{
		$this->model = $model;
		$this->setViewPrefix('navigation');
		parent::__construct($router);
	}

	public function confirm($navigations)
	{
		$resource = 'navigations.destroy';
		$id = $navigations;
		return view('app.confirm', compact('resource', 'id'));
	}


}
