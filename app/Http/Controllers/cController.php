<?php namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;

abstract class cController extends Controller {
	use ValidatesRequests;

    protected $viewPrefix;
    protected $controllerAction;
    protected $controller;
    protected $action;

    public function flash($message)
    {
		\Session::flash('message', $message);	
    }

    public function getControllerAction()
    {
    	list($this->controller, $this->action) = explode('@', $this->controllerAction['controller']);
    }

    public function __construct(Router $router)
    {
        // $this->middleware('auth');
    	if(!is_null($router->current())){
	    	$this->controllerAction = $router->current()->getAction();
	    	$this->getControllerAction();
    	}
    }

    public function setViewPrefix($prefix)
    {
    	$this->viewPrefix = "{$prefix}.";
    }
    	/**
	 * Display a listing of the resource.
	 *
	 * @return  Response
	 */
	public function index()
	{
		$search = '';
		$query = $this->model->orderBy('id', 'desc');

		if(\Request::has('q'))
		{
			$q = \Request::get('q');
			if(property_exists($this->model, 'search') && count($this->model->search) > 0){
				$search = "Search results for '$q'";

				$searchable =  $this->model->search;

				$query->where('id',"$q");
				$qbits = explode(' ', $q);
				foreach ($searchable as $field) {
					foreach ($qbits as $bit) {
						$query->orWhere("$field",'like',"%$bit%");
					}
				}
			} 
		}
		return $query->paginate(10);
	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id, \Request $request)
    {
        return $this->model->findOrFail($id);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param    int  $id
	 * @return  Response
	 */
	public function update($id)
	{
		$controllerAction = $this->getControllerAction();
		$model = $this->model->find($id);
		$model->fill(array_filter(\Request::all(), 'strlen'));
		if ( $model->save() ){
			\Session::flash('message', 'Successfully updated!');
			return \Redirect::action("\\{$this->controller}@index");
		}
		else{
			return view("{$this->viewPrefix}.write", compact('model'))->withErrors($model->getErrors());
		}
	}

	public function create()
	{
		return view("{$this->viewPrefix}.write");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return  Response
	 */
	public function store()
	{
		$modelClass = get_class($this->model);
		$data = \Request::all();

		$model = new $modelClass;
		$model->fill(array_filter(\Request::all()));
		if ( $model->save() ){
			\Session::flash('message', 'Successfully created!');
			return \Redirect::action("\\{$this->controller}@index");			
		}
		else{
			return view("{$this->viewPrefix}.write", compact('model'))->withRequest($data)->withErrors($model->getErrors());
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param    int  $id
	 * @return  Response
	 */
	public function show($id)
	{
		$model = $this->model->find($id);
		if ( ! empty($model) ) 
		return view("{$this->viewPrefix}.details", compact('model'));
		else abort(404);
	}	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param    int  $id
	 * @return  Response
	 */
	public function destroy($id)
	{
		$model = $this->model->find($id);
		if(method_exists($model, 'remove'))
			$model->remove();
		else
			$model->delete();
		\Session::flash('message', 'Successfully deleted!');
		return \Redirect::to(\Request::get('ref'));						
	}

	public function verify($model)
	{

		$model = $this->model->find($model);
		$model->verified = true;
		$model->save();

		if(\Request::ajax())
		{
			return \Response::json(['msg'=>sprintf('%s [%s] marked as verified.', $model->name, $model->getSerial()) ,'status'=>true], 201);
		}
		\Session::flash('message', 'Entity marked as verified!');
		return \Redirect::back();
	}
}
