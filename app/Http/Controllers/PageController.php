<?php   namespace App\Http\Controllers;

use \Illuminate\Routing\Router;
use App\Page;

class PageController extends BaseController {

	/**
	 * Holds the model
	 */
	protected $model;

	public function __construct(Page $model, Router $router)
	{
		$this->model = $model;
		$this->setViewPrefix('page');
		parent::__construct($router);
	}

	public function confirm($pages)
	{
		$resource = 'pages.destroy';
		$id = $pages;
		return view('app.confirm', compact('resource', 'id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return  Response
	 */
	public function store()
	{
		$modelClass = get_class($this->model);
		$data = \Request::all();

		$model = new $modelClass;
		$model->fill(array_filter(\Request::all()));

		$model->created_by = auth()->user()->id;
		$model->status = 'draft';
		if ( $model->save() ){
			\Session::flash('message', 'Successfully created!');
			return \Redirect::action("\\{$this->controller}@index");			
		}
		else{
			return view("{$this->viewPrefix}.write", compact('model'))->withRequest($data)->withErrors($model->getErrors());
		}
	}

	public function page($slug, Page $page)
	{
		$page = $page->where('slug', $slug)->firstOrFail();
		return view('page', compact('page'));

	}


}
