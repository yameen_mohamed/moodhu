<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

trait baseFunctions
{
    /**
     * Error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Validation rules
     * 
     * @var Array
     */
    protected static $rules = array();

    /**
     * Validator instance
     * 
     * @var Illuminate\Validation\Validators
     */
    protected $validator;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->validator = \App::make('validator');
    }
    


    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Listen for save event
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            return $model->validate();
        });
    }

    /**
     * Validates current attributes against rules
     */
    public function validate()
    {
        $replace = ($this->getKey() > 0) ? $this->getKey() : '';
        foreach (static::$rules as $key => $rule)
        {
            static::$rules[$key] = str_replace(':id', $replace, $rule);
        }
 
        // dd(get_class($this->validator));
        $validator = $this->validator->make($this->attributes, static::$rules);

 
        if ($validator->passes()) return true;
 
        $this->setErrors($validator->messages());

        return false;
    }

    /**
     * Set error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Retrieve error message bag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Inverse of wasSaved
     */
    public function hasErrors()
    {
        return ! empty($this->errors);
    }


    public function GetProperties()
    {

//      dd("select a.ORDINAL_POSITION, a.data_type, a.column_name,  a.is_nullable,b.REFERENCED_COLUMN_NAME as fkColumn, if(a.COLUMN_KEY='PRI',true, false) IsPK, b.REFERENCED_TABLE_NAME as fkTable from INFORMATION_SCHEMA.COLUMNS         a left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b on a.TABLE_SCHEMA = b.TABLE_SCHEMA and a.table_name = b.TABLE_NAME and a.COLUMN_NAME=b.COLUMN_NAME  where a.table_name = '".$this->getTable()."' order by a.ordinal_position");

        $table = $this->getTable();
        $ret = \DB::raw("select a.ORDINAL_POSITION, a.data_type, a.column_name,  a.is_nullable,b.REFERENCED_COLUMN_NAME as fkColumn,
                        if(a.COLUMN_KEY='PRI',true, false) IsPK, b.REFERENCED_TABLE_NAME as referencedTable from INFORMATION_SCHEMA.COLUMNS 
                            a left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b
                        on a.TABLE_SCHEMA = b.TABLE_SCHEMA and a.table_name = b.TABLE_NAME and a.COLUMN_NAME=b.COLUMN_NAME
                        where a.table_name = '".$this->getTable()."' and a.TABLE_SCHEMA='vivent' order by a.ordinal_position");
        return \DB::select($ret);
    }
    public static function GetSProperties($table)
    {
        $ret = \DB::raw("show columns from $table");
        return \DB::select($ret);
    }
}

class BaseModel extends Model {

    /**
     * Error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Validation rules
     * 
     * @var Array
     */
    protected static $rules = array();

    /**
     * Validator instance
     * 
     * @var Illuminate\Validation\Validators
     */
    protected $validator;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->validator = \App::make('validator');
    }

    /**
     * Listen for save event
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            return $model->validate();
        });
    }

    /**
     * Validates current attributes against rules
     */
    public function validate()
    {
        $replace = ($this->getKey() > 0) ? $this->getKey() : '';
        foreach (static::$rules as $key => $rule)
        {
            static::$rules[$key] = str_replace(':id', $replace, $rule);
        }
 
        // dd(get_class($this->validator));
        $validator = $this->validator->make(array_filter($this->attributes), static::$rules);
        // dd($this->attributes, $validator);

 
        if ($validator->passes()) return true;
 
        $this->setErrors($validator->messages());

        return false;
    }

    /**
     * Set error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Retrieve error message bag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Inverse of wasSaved
     */
    public function hasErrors()
    {
        return ! empty($this->errors);
    }


    public function GetProperties()
    {

//      dd("select a.ORDINAL_POSITION, a.data_type, a.column_name,  a.is_nullable,b.REFERENCED_COLUMN_NAME as fkColumn, if(a.COLUMN_KEY='PRI',true, false) IsPK, b.REFERENCED_TABLE_NAME as fkTable from INFORMATION_SCHEMA.COLUMNS         a left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b on a.TABLE_SCHEMA = b.TABLE_SCHEMA and a.table_name = b.TABLE_NAME and a.COLUMN_NAME=b.COLUMN_NAME  where a.table_name = '".$this->getTable()."' order by a.ordinal_position");

        $table = $this->getTable();
        $ret = \DB::raw("select a.ORDINAL_POSITION, a.data_type, a.column_name,  a.is_nullable,b.REFERENCED_COLUMN_NAME as fkColumn,
                        if(a.COLUMN_KEY='PRI',true, false) IsPK, b.REFERENCED_TABLE_NAME as referencedTable from INFORMATION_SCHEMA.COLUMNS 
                            a left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE b
                        on a.TABLE_SCHEMA = b.TABLE_SCHEMA and a.table_name = b.TABLE_NAME and a.COLUMN_NAME=b.COLUMN_NAME
                        where a.table_name = '".$this->getTable()."' and a.TABLE_SCHEMA='vivent' order by a.ordinal_position");
        return \DB::select($ret);
    }
    public static function GetSProperties($table)
    {
        $ret = \DB::raw("show columns from $table");
        return \DB::select($ret);
    }

    // public function newEloquentBuilder($query)
    // {
    //    return new \Opos\ExtendedBuilder($query);
    // }

}