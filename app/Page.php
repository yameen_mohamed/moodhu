<?php    namespace App;
use App\BaseModel;
class Page extends BaseModel {
	/*
	 *	This is the eloquent model for Page
	 *  Author: Yameen Mohamed (yaamynu@gmail.com)
	 *  Date: Friday 7th of April 2017 11:27:23 PM	 *
	 */
	protected $fillable = [
		'title',
		'slug',
		'content',
		'created_by',
		'status',
		'private_password',
	];

	protected static $rules = [
		'title'=>'required',
		'slug'=>'required',
		'content'=>'required',
		'created_by'=>'required|integer',
		'status'=>'required',
		'private_password'=>'',
	];

	function user(){
		return $this->belongsTo('App\User','created_by','id');
	}

	public function getDates(){
		return array_merge(parent::getDates(),[       
			]);
	}

}
