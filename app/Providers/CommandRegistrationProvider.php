<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CommandRegistrationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Define commands in the env file in the format
        // App/Namespace/Class,App/Namespace/NextClass
        $commands = array_filter(explode(',', getenv('COMMANDS')));

        if(count($commands)){
            $this->commands($commands);

        }
    }
}
