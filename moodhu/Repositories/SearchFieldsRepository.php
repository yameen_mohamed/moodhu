<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/6/16
 * Time: 11:28 PM
 */

namespace Moodhu\Repositories;


class SearchFieldsRepository extends Repository
{
    protected $payload;

    protected $categories;
    protected $countries;
    protected $destinations;
    protected $regions;

    public function getLists()
    {
        $this->setClient(app('api'));
        $this->payload = $this->SearchFields($this->getArguments([]));

        if (property_exists($this->payload->Categories->CategoryList, 'Category'))
        {
            foreach ($this->payload->Categories->CategoryList->Category as $category) {
                $this->categories[] = $this->factory->makeFilter()->setId($category->CategoryID)->setName($category->CategoryName)->getAttributes();
            }
        }
        if (property_exists($this->payload->CountryList, 'Country')) {
            foreach ($this->payload->CountryList->Country as $country) {
                $this->countries[] = $this->factory->makeFilter()->setId($country->CountryID)->setName($country->CountryName)->getAttributes();
            }
        }
        if (property_exists($this->payload->RegionList, 'Region')) {
            foreach ($this->payload->RegionList->Region as $region) {
                $this->regions[] = $this->factory->makeFilter()->setId($region->RegionID)->setName($region->RegionName)->getAttributes();
            }
        }
        if (property_exists($this->payload->DestinationList, 'Destination')) {
            foreach ($this->payload->DestinationList->Destination as $destination) {
                $this->destinations[] = $this->factory->makeFilter()->setId($destination->DestinationID)->setName($destination->DestinationName)->getAttributes();
            }
        }

        return [
            'categories' => $this->categories,
            'countries' => $this->countries,
            'regions'   => $this->regions,
            'destinations'  => $this->destinations,
        ];

    }

}