<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 4/7/17
 * Time: 1:29 PM
 */

namespace Moodhu\Repositories;


use Carbon\Carbon;
use Moodhu\Entities\Customer;

class CustomerRepository extends Repository
{
    public function makeCustomer()
    {
        return new Customer();
    }
    public function createCustomer($data = [])
    {
        $customer = $this->makeCustomer();
        $customer->fill($data);
        return $this->save($customer)->CustomerInsertResult->Status->Code == 'OK';
    }


    private function save(Customer $customer)
    {
        $this->setClient(app('api'));

        return $this->getClient()->CustomerInsert(
            [
                'customerInsertParameters' => [
                    'DoNotAllowDuplicateInsertByEmail' => true,
                    'Customer' =>   [
                        'PersonName' =>  $customer->getName(),
                        'PersonSurname' => $customer->getName(),
                        'Email' => $customer->getEmail(),
                        'MobilePhoneNumber' => $customer->getMobileNumber(),
                        'UniqueIdentificationNumber' => $customer->getMobileNumber(),
                        'CountryID' => $customer->getCountryId(),
                        'TaxPayerType' => 0,
                        'CustomerID' => 0,
                        'IsCustomer' => true,
                        'IsSupplier' => false,
                        'IsPartner' => false,
                        'CustomerType' => 0,
                        'ContractType' => 1,
                        'CreatedDate' => Carbon::now()->format('Y-m-d'),
                        'BirthDate' => Carbon::now()->format('Y-m-d')
                    ]
                ]
            ]
        );
    }

}