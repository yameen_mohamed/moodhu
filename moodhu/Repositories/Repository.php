<?php namespace Moodhu\Repositories;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Moodhu\Factories\Factory;
use SoapClient;

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 12:47 AM
 */

 class Repository
 {
     protected $client;


     protected $factory;

     protected $destinations = [];
     protected $regions = [];
     protected $countries = [];

     protected $global_default_arguments = [
         'IgnorePriceAndAvailability' => true,
         'LanguageID' => 'en',
         'PageSize' => 10,
         'PhotoWidth' => 1020,
         'PhotoHeight' => 400,
     ];

     protected $default_arguments = [
     ];

     protected $total;
     protected $current_page;
     protected $page_size;


     /**
      * AccommodationRepository constructor.
      * @param Factory $factory
      */
     public function __construct(Factory $factory)
     {
         $this->factory = $factory;
     }

     /**
      * @param mixed $client
      * @return Repository
      */
     public function setClient(SoapClient $client)
     {
         $this->client = $client;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getClient()
     {
         return $this->client;
     }


     protected function getArguments($args)
     {
         $merged = array_merge($this->global_default_arguments, $this->default_arguments);
         return array_merge($merged, $args);
     }


     public function paginate($args)
     {
         return [
             'data' => $this->getData($args),
             'total' => $this->total,
             'current_page' => $this->current_page,
             'last_page' => ceil($this->total/$this->page_size),
             'page_size' => $this->page_size,

         ];
     }

     function __call($name, $arguments)
     {
         $method = "Get$name";
         $payload = $this->client->$method([
             "get{$name}Parameters" => $arguments[0]
         ]);
         $resultObject = "Get{$name}Result";

         if(property_exists($payload, $resultObject))
         {
             if(property_exists($payload->$resultObject, 'Status'))
             {
                 if($status = $payload->$resultObject->Status ) {
                     if ($status->Code == 'OK') {
                         if(property_exists($payload->$resultObject, 'TotalNumberOfResults'))
                         {
                             $this->total =  $payload->$resultObject->TotalNumberOfResults;
                             $this->current_page = $payload->$resultObject->CurrentPage;
                             $this->page_size = $payload->$resultObject->PageSize;
                         }

                     } else {
                         throw new Exception("An error occurred");
                     }
                 }
             }

             return $payload->$resultObject;
         }
         return $payload;
     }

     public function makeKey($key)
     {
         return snake_case(studly_case($key));
     }
     /**
      * @param $accommodationObject
      * @param $accommodation
      */
     protected function extractProperties($accommodationObject, $accommodation)
     {
         foreach ($accommodationObject->AttributeGroupList->AttributeGroup as $attributeLists) {

             if($attributeLists->GroupName == 'Location' && count($attributeLists->AttributeList->Attribute))
             {
                 $location = $this->factory->makeLocation();
                 $location->setLatitude($attributeLists->AttributeList->Attribute[0]->AttributeValue);
                 $location->setLongitude($attributeLists->AttributeList->Attribute[1]->AttributeValue);
                 $accommodation->setLocation($location);
             }
             else
             {
                 foreach ($attributeLists->AttributeList->Attribute as $attributes) {

                         $accommodation->addProperty(
                             $this->makeKey($attributes->AttributeName),
                             $attributes->AttributeOriginalValue,
                             $this->makeKey($attributeLists->GroupName)
                         );
                 }
             }
         }
     }

     /**
      * @param $sourceObject
      * @param $destinationObject
      * @return mixed
      */
     protected function extractPhotos($sourceObject, $destinationObject)
     {
         if (property_exists($sourceObject, 'PhotoList')) {
             if (property_exists($sourceObject->PhotoList, 'Photo')) {
                 foreach ($sourceObject->PhotoList->Photo as $photo) {
                     $destinationObject->addPhoto(
                         $this->factory->makePhoto()->setImageUrl($photo->PhotoUrl)->setThumbnailUrl($photo->ThumbnailUrl)
                     );
                 }
             }
         }
     }


     protected function prepareLists()
     {

         if ( property_exists( $this->payload, 'CountryList') && property_exists( $this->payload->CountryList, 'Country'))
         {
             foreach ($this->payload->CountryList->Country  as $country) {
                 $this->extractCountry($country);
             }
         }elseif ( property_exists( $this->payload, 'Country'))
         {
             $this->extractCountry($this->payload->Country);

         }


         if (property_exists( $this->payload, 'RegionList') && property_exists( $this->payload->RegionList, 'Region')) {
             foreach ($this->payload->RegionList->Region as $region) {
                 $this->extractRegion($region);
             }
         }elseif ( property_exists( $this->payload, 'Region'))
         {
             $this->extractRegion($this->payload->Region);

         }


         if (property_exists( $this->payload, 'DestinationList') && property_exists( $this->payload->DestinationList, 'Destination')) {

             foreach ($this->payload->DestinationList->Destination as $destination) {
                 $this->extractDestination($destination);
             }
         }elseif ( property_exists( $this->payload, 'Destination'))
         {
             $this->extractDestination($this->payload->Destination);

         }
     }

     /**
      * @param $country
      */
     protected function extractCountry($country)
     {
         $cou = $this->factory->makeCountry();
         $cou->setName($country->CountryName);
         $cou->setId($country->CountryID);
         $this->countries[$country->CountryID] = $cou;
     }

     /**
      * @param $region
      */
     protected function extractRegion($region)
     {
         $reg = $this->factory->makeRegion();
         $reg->setName($region->RegionName);
         $reg->setId($region->RegionID);
         $reg->setCountry($this->countries[$region->CountryID]);
         $this->regions[$region->RegionID] = $reg;
     }

     /**
      * @param $destination
      */
     protected function extractDestination($destination)
     {
         $dest = $this->factory->makeDestination();
         $dest->setName($destination->DestinationName);
         $dest->setId($destination->DestinationID);
         $dest->setRegion($this->regions[$destination->RegionID]);
         $this->destinations[$destination->DestinationID] = $dest;
     }


 }
