<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 1:35 AM
 */

namespace Moodhu\Repositories;


use Illuminate\Support\Collection;

class AccommodationRepository extends Repository
{
    protected $payload;
    protected $data;
    protected $default_arguments = [
        "InPriceType" => "Total",
        "CurrencyID" => 978,
        // "ToDisplaySpecialOffers" => true,
        "OnlyOnSpecialOffer" => false,
        "UserID" => null,
        "OutParameterList" =>  [
            [
                "ResponseDetail" => "ObjectDescription",
                "NumberOfResults" => "100"
            ],
            [
              "ResponseDetail" => "ObjectPhotos",
              "NumberOfResults" => "100"
            ],
            [
              "ResponseDetail" => "ObjectDetailedAttributes",
              "NumberOfResults" => "10"
            ],
            [
              "ResponseDetail" => "UnitDescription",
              "NumberOfResults" => "1"
            ],

//            [
//              "ResponseDetail" => "CalculatedPriceInfo",
//              "NumberOfResults" => "1"
//            ],
            [
              "ResponseDetail" => "UnitPhotos",
              "NumberOfResults" => "100"
            ]
        ],
        "SortParameterList" => [
        [
            "SortBy" => "Priority",
            "SortOrder" => "Descending"
        ]
      ]
    ];


    public function getAccommodations($args)
    {
        return $this->getData($args);
    }


    protected function getData($args)
    {
        if(is_null($this->data))
        {
            $this->setClient(app('api'));
            $this->loadPayload($args);
            return $this->process()->data;
        }
        return $this->data;
    }


    public function loadPayload($args)
    {
        return $this->setPayload($this->SearchResults($this->getArguments($args)));
    }

    /**
     * @param mixed $payload
     * @return AccommodationRepository
     */
    public function setPayload($payload)
    {
        if(!isset($this->payload)){

            $this->payload = $payload;

            $this->prepareLists();

        }

//        dd($this->payload);

        return $this;
    }

    private function process()
    {
        if(isset($this->payload))
        {
            if(property_exists($this->payload->AccommodationObjectList,'AccommodationObject'))
            {
                $this->data = new Collection();
                foreach ($this->payload->AccommodationObjectList->AccommodationObject as $accommodationObject) {
                    $this->data[] = $this->makeAccommodation($accommodationObject);
                }

            }
            else
            {
                throw new \Exception("Search criteria does not meet any accommodations!");
            }

        }
        else
        {
            throw new \Exception("API server did not return a valid payload. Check your parameters");
        }
        return $this;
    }

    /**
     * @param $accommodationObject
     * @return \Moodhu\Entities\Accommodation
     */
    public function makeAccommodation($accommodationObject)
    {
        $this->setPayload($accommodationObject);


        $accommodation = $this->factory->makeAccommodation();
        $accommodation->setName($accommodationObject->Name);
        $accommodation->setDestination($this->destinations[$accommodationObject->DestinationID]);
        if(property_exists($accommodationObject,'Description'))
            $accommodation->setDescription($accommodationObject->Description);
        if(property_exists($accommodationObject,'ShortDescription'))
            $accommodation->setShortDescription($accommodationObject->ShortDescription);

        $accommodation->setType($accommodationObject->ObjectType->ObjectTypeName);
        $accommodation->setId($accommodationObject->ObjectID);

        if (property_exists($accommodationObject, 'AttributeGroupList')) {
            $this->extractProperties($accommodationObject, $accommodation);
        }

        if (property_exists($accommodationObject, 'CancellationPolicy')) {
            $accommodation->addPolicy('cancellation_policy', $accommodationObject->CancellationPolicy);
        }





        $this->extractPhotos($accommodationObject, $accommodation);

        if (property_exists($accommodationObject, 'UnitList')) {

            foreach ($accommodationObject->UnitList->AccommodationUnit as $unit) {
                $unitObject = $this->factory->makeUnit();


                $unitObject->setID($unit->UnitID);

                $unitObject->setBookingUrl($unit->BookingAddress);

                if(property_exists($unit->Type,"UnitTypeName"))
                    $unitObject->setType($unit->Type->UnitTypeName);

                $this->extractProperties($unit, $unitObject);



//                if(property_exists($unit,'Description'))
//                    $unitObject->setDescription($unit->Description);
//                elseif($unitObject->hasType())
//                {
//                    if($desc = $unitObject->getProperty('description',$this->makeKey($unitObject->getType())))
//                        $unitObject->setDescription($desc);
//                    else
//                    {
//                        $unitObject->setDescription(str_replace('_',' ', title_case( array_keys($unitObject->getPropertyByIndex(0))[0])));
//                    }
//                }

                $unitObject->setDescription(array_values( $unitObject->getPropertyByIndex(0))[0]['description']);



                $this->extractPhotos($unit, $unitObject);

                if(property_exists($unit->ServiceList, 'Service'))
                {
                    
                    foreach ($unit->ServiceList->Service as $service) {
                        if($service->ServiceType == 'SpecialOffer')
                        {
                            $specialOffer = $this->factory->makeSpecialOffer();
                            $specialOffer->setId($service->ServiceID)
                                ->setName($service->ServiceName)
                                ->setType($service->ServiceType);

                            $unitObject->addSpecialOffer($specialOffer);
                        }
                        else
                        {
                            $serviceObject = $this->factory->makeService();
                            $serviceObject->setId($service->ServiceID)->setName($service->ServiceName)->setType($service->ServiceType);
                            $unitObject->addService($serviceObject);
                        }
                    }
                }

                if(property_exists($unit->SpecialOfferList, 'SpecialOffer'))
                {
                    foreach ($unit->SpecialOfferList->SpecialOffer as $special_offer) {
                        $specialOffer = $this->factory->makeSpecialOffer();
                        $specialOffer->setId($special_offer->ServiceID)
                        ->setName($special_offer->ServiceName)
                        ->setType($special_offer->ServiceType);

                        $unitObject->addSpecialOffer($specialOffer);
                    }
                }


                $accommodation->addUnit($unitObject);

            }

        }






        if($accommodation->getType())
        {
            $accommodation->setRating($accommodation->getProperty("number_of_stars", $this->makeKey($accommodation->getType())));
        }

        $accommodation->removePropertyByKey('hotel');


        return $accommodation;
    }


    public function getDetails($id)
    {
        $this->setClient(app('api'));
        $args = $this->getArguments(['ObjectID' => $id]);
        $this->setPayload($this->DetailedDescription($this->getArguments($args)));
        return $this->makeAccommodation($this->payload->AccommodationObject);
    }



}