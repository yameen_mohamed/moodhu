<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/30/16
 * Time: 8:42 PM
 */

namespace Moodhu\Repositories;

use Illuminate\Support\Collection;

class TourRepository extends Repository
{
    protected $default_arguments = [
        "InPriceType" => "Total",
        "OutParameterList" =>  [
            [
                "ResponseDetail" => "ObjectPhotos",
                "NumberOfResults" => "100"
            ],
            [
                "ResponseDetail" => "ObjectDetailedAttributes",
                "NumberOfResults" => "10"
            ],
            [
                "ResponseDetail" => "UnitDescription",
                "NumberOfResults" => "1"
            ],
            [
                "ResponseDetail" => "CalculatedPriceInfo",
                "NumberOfResults" => "1"
            ],
            [
                "ResponseDetail" => "UnitPhotos",
                "NumberOfResults" => "100"
            ]
            ],
        ];

    protected $payload;

    protected $data;

    /**
     * @param $args
     * @return array|Collection
     */
    protected function getData($args)
    {
        if(is_null($this->data))
        {
            $this->setClient(app('api'));

            $defaults = [
                'ignorePriceAndAvailability' => true
            ];

            $args = array_merge($args, $defaults);


            $this->payload =  $this->PackageSearchResults( $this->getArguments($args));

            return $this->process()->data;
        }

        return $this->data;

    }


    public function getDetails($id)
    {
        $this->setClient(app('api'));

        $args = $this->getArguments(['PackageTourID' => $id]);

        $this->payload =  $this->PackageDetailedDescription( $this->getArguments($args));

//        dd($this->payload->GetPackageDetailedDescriptionResult->PackageTour);

        return $this->extractTour($this->payload->PackageTour);

    }


    public function getTours($args)
    {
        return $this->getData($args);
    }

    private function process()
    {
        if(isset($this->payload))
        {

            $this->data = new Collection();
            if(property_exists($this->payload->PackageTourList,'PackageTour'))
            {
                foreach ($this->payload->PackageTourList->PackageTour as $tourObject) {

                    $this->data[] = $this->extractTour($tourObject);
                }
            }
        }
        return $this;
    }

    /**
     * @param $tourObject
     * @return \Moodhu\Entities\Tour
     */
    private function extractTour($tourObject)
    {
        $tour = $this->factory->makeTour();
        $tour->setName($tourObject->Name);
        $tour->setId($tourObject->ObjectID);
        if(property_exists($tourObject, 'Description')) {
            $tour->setDescription($tourObject->Description);
        }
        if(property_exists($tourObject, 'ShortDescription')){
            $tour->setShortDescription($tourObject->ShortDescription);
        }

        $tour->setType($tour->makeKey($tourObject->ObjectType->ObjectTypeName));

        if ($attributes = $tourObject->AttributeGroupList) {
            if (count($attributes->AttributeGroup)) {
                if ($attributes->AttributeGroup) {
                    foreach ($attributes->AttributeGroup as $group) {
                        foreach ($group->AttributeList->Attribute as $attribute) {
                            $tour->addProperty(
                                $tour->makeKey($attribute->AttributeName),
                                $attribute->AttributeOriginalValue
                            );
                        }
                    }
                }
            }
        }

        if (property_exists($tourObject, 'CategoryList')) {
            if (property_exists($tourObject->CategoryList, 'Category')) {
                foreach ($tourObject->CategoryList->Category as $category) {
                    $tour->addCategory(
                        $this->factory->makeCategory()->setName($category->CategoryName)
                    );
                }
            }

        }

        $this->extractPhotos($tourObject, $tour);
        return $tour;
    }

}