<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/1/16
 * Time: 12:39 AM
 */

namespace Moodhu\Repositories;


use Illuminate\Support\Collection;
use Moodhu\Factories\Factory;

class SpecialOfferRepository extends Repository
{
    protected $arguments;
    protected $payload;
    protected $default_arguments = [
        'ignorePriceAndAvailability' => true,
        "OutParameterList" =>  [
            [
                "ResponseDetail" => "ObjectPhotos",
                "NumberOfResults" => "100"
            ],
            [
                "ResponseDetail" => "ObjectDetailedAttributes",
                "NumberOfResults" => "10"
            ],
            [
                "ResponseDetail" => "UnitDescription",
                "NumberOfResults" => "1"
            ],
            [
                "ResponseDetail" => "CalculatedPriceInfo",
                "NumberOfResults" => "1"
            ],
            [
                "ResponseDetail" => "UnitPhotos",
                "NumberOfResults" => "100"
            ]
        ],
    ];
    /**
     * @var
     */
    protected $accommodationRepository;

    /**
     * @var
     */
    protected $data;

    /**
     * AccommodationRepository constructor.
     * @param Factory $factory
     * @param AccommodationRepository $accommodationRepository
     */
    public function __construct(Factory $factory, AccommodationRepository $accommodationRepository)
    {
        $this->factory = $factory;
        $this->accommodationRepository = $accommodationRepository;
    }

    public function getSpecialOffers($args)
    {
        return $this->GetData($args);
    }

    /**
     * @param $args
     * @return array|Collection
     */
    protected function GetData($args)
    {
        if(is_null($this->data))
        {
            $this->setClient(app('api'));

            $args = $this->getArguments($args);

            $this->payload =  $this->SpecialOffers($this->getArguments($args));

            return $this->process()->data;
        }
        return $this->data;
    }

    private function process()
    {
        if(property_exists($this->payload, 'SpecialOfferList'))
        {
            if(property_exists($this->payload->SpecialOfferList, 'SpecialOffer')) {

                $specialOffers = new Collection();

                foreach ($this->payload->SpecialOfferList->SpecialOffer as $specialOfferObject) {
                    $specialOffer = $this->factory->makeSpecialOffer();
                    $specialOffer->setName($specialOfferObject->ServiceName);
                    $specialOffer->setId($specialOfferObject->ServiceID);
                    $specialOffer->setType($specialOfferObject->ServiceType);
                    $specialOffer->setAccommodation(
                        $this->accommodationRepository->makeAccommodation($specialOfferObject->AccommodationObject)
                    );
                    $specialOffers[] = $specialOffer;
                }

                $this->data = $specialOffers;
            }

        }
        return $this;
    }



}