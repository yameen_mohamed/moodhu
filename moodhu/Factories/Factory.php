<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 12:48 AM
 */

namespace Moodhu\Factories;


use Illuminate\Support\Facades\Config;
use Moodhu\Entities\Accommodation;
use Moodhu\Entities\Category;
use Moodhu\Entities\Country;
use Moodhu\Entities\Destination;
use Moodhu\Entities\Filter;
use Moodhu\Entities\Location;
use Moodhu\Entities\Photo;
use Moodhu\Entities\Region;
use Moodhu\Entities\Service;
use Moodhu\Entities\SpecialOffer;
use Moodhu\Entities\Tour;
use Moodhu\Entities\Unit;
use SoapClient;
use SoapHeader;

class Factory
{

    public function getClient()
    {
        return app('api');
    }

    public function makeAccommodation()
    {
        return new Accommodation();
    }

    public function makeUnit()
    {
        return new Unit();
    }

    public function makeService(){
        return new Service();
    }

    public function makeLocation()
    {
        return new Location();
    }

    public function makeCountry()
    {
        return new Country();
    }

    public function makeRegion()
    {
        return new Region();
    }

    public function makeDestination()
    {
        return new Destination();
    }

    public function makeTour()
    {
        return new Tour();
    }

    public function makeCategory()
    {
        return new Category();
    }

    public function makeFilter()
    {
        return new Filter();
    }

    public function makeSpecialOffer()
    {
        return new SpecialOffer();
    }

    public function makePhoto()
    {
        return new Photo();
    }

}