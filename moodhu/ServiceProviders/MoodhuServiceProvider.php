<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 1:20 AM
 */

namespace Moodhu\ServiceProviders;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use SoapClient;
use SoapHeader;

class MoodhuServiceProvider extends ServiceProvider
{


    /**
     * @var string for readable config
     */
    protected $namespace = 'moodhu.';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->loadConfig();

        $authHeader = array(
            'Username' => Config::get('moodhu.soap_user_name'),
            'Password' => Config::get('moodhu.soap_password')
        );
        $header = new SoapHeader('http://tempuri.org/', 'AuthHeader', $authHeader, false);

        $apiWebService = new SoapClient(Config::get('moodhu.soap_wsdl_url'), array(
            "connection_timeout" => 120,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
        $apiWebService->__setSoapHeaders($header);

        app()->singleton('api', function() use ($apiWebService)
        {
            return $apiWebService;
        });
    }

    private function loadConfig()
    {
        if(file_exists(__DIR__.'/../Config/config.php'))
        {
            foreach(require_once(__DIR__.'/../Config/config.php') as $key => $value){
                app('config')->set($this->namespace . $key , $value);
            }
        }
    }

}