<?php namespace Moodhu\Interfaces;

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 12:32 AM
 */
interface iDetails
{
    public function getID();
    public function getName();
    public function getDescription();
    public function getType();
    public function getRating();
    public function getProperties();
    public function getPhotos();

}