<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 4/7/17
 * Time: 1:30 PM
 */

namespace Moodhu\Entities;


class Customer extends BaseEntity
{
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $mobile_number;

    /**
     * @var
     */
    protected $country_id;

    /**
     * @var
     */
    protected $email;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobileNumber()
    {
        return $this->mobile_number;
    }

    /**
     * @param mixed $mobile_number
     * @return Customer
     */
    public function setMobileNumber($mobile_number)
    {
        $this->mobile_number = $mobile_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * @param mixed $country_id
     * @return Customer
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }










}