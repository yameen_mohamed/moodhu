<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/24/16
 * Time: 2:41 AM
 */

namespace Moodhu\Entities;


class Country extends BaseEntity
{
    /**
     * @var
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

}