<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/7/16
 * Time: 3:21 AM
 */

namespace Moodhu\Entities;


class Unit extends BaseEntity
{
    /**
     * @var
     */
    protected $description;
    /**
     * @var
     */
    protected $type;
    /**
     * @var
     */
    protected $photos;

    protected $image;
    /**
     * @var
     */
    protected $services;
    /**
     * @var
     */
    protected $special_offers = [];


    protected $bookingUrl;

    /**
     * @return mixed
     */
    public function getBookingUrl()
    {
        return $this->bookingUrl;
    }

    /**
     * @param mixed $bookingUrl
     * @return Unit
     */
    public function setBookingUrl($bookingUrl)
    {
        $this->bookingUrl = $bookingUrl;
        return $this;
    }






    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Unit
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Unit
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    public function addPhoto($image)
    {
        $this->photos[] = $image;
        $this->image = $this->getRandomImage();
        return $this;
    }

    public function hasPhotos()
    {
        return count($this->photos) > 0;
    }
    /**
     * @param mixed $photos
     * @return Unit
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    public function addService(Service $service)
    {
        $this->services[] = $service;
        return $service;
    }

    /**
     * @param mixed $services
     * @return Unit
     */
    public function setServices($services)
    {
        $this->services = $services;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialOffers()
    {
        return $this->special_offers;
    }

    public function addSpecialOffer(SpecialOffer $offer)
    {
        $this->special_offers[] = $offer;
        return $this;
    }
    /**
     * @param mixed $special_offers
     * @return Unit
     */
    public function setSpecialOffers($special_offers)
    {
        $this->special_offers = $special_offers;
        return $this;
    }


    public function getFacilities()
    {
        $props = array_values($this->getProperties())[0];
        unset($props['description']);
        unset($props['capacity']);
        return implode(', ', array_map(function($arg) { return str_replace('_',' ',title_case($arg)); }, array_keys($props)));
    }




    public function toJson($options = 0)
    {
        if(count($this->photos))
        {
            foreach ($this->photos as $index => $photo) {
                $this->photos[$index] = $photo->getAttributes();
            }

            $this->image = $this->image->getAttributes();
        }

        if(count($this->services))
        {
            foreach ($this->services as $index => $service) {
                $this->services[$index] = $service->getAttributes();
            }
        }

        return parent::toJson($options);
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }


}