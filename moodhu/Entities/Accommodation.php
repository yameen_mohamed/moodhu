<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 12:37 AM
 */

namespace Moodhu\Entities;


use Moodhu\Interfaces\iDetails;

class Accommodation extends BaseEntity implements iDetails
{

    /**
     * @var
     */
    protected $id;
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $description;


    /**
     * @var Destination
     */
    protected $destination;

    /**
     * @var
     */
    protected $short_description = "";
    /**
     * @var
     */
    protected $type;
    /**
     * @var
     */
    protected $rating;

    /**
     * @var
     */
    protected $photos = [];

    /**
     * @var
     */
    protected $units = [];

    /**
     * @var
     */
    protected $image;

    /**
     * @var array
     */
    protected $policies = [];

    protected $has_special_offers = false;

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * @param $name
     * @return $this
     */
    public function removePolicy($name)
    {
        unset($this->policies[$name]);
        return $this;
    }

    /**
     * @param $name
     * @param $text
     * @return $this
     */
    public function addPolicy($name, $text)
    {
        $this->policies[$name] = $text;
        return $this;

    }

    /**
     * @param $name
     * @param $text
     * @return mixed
     */
    public function getPolicy($name, $text)
    {
        return $this->policies[$name];
    }

    /**
     * @return array
     */
    public function getPolicies()
    {
        return $this->policies;
    }

    /**
     * @param array $policies
     * @return Accommodation
     */
    public function setPolicies($policies)
    {
        $this->policies = $policies;
        return $this;
    }

    public function hasSpecialOffers()
    {
        foreach ($this->getUnits() as $unit) {
            if($unit->hasSpecialOffers())
            {
                return true;
            }
        }

        return false;
    }




    /**
     * @param mixed $id
     * @return Accommodation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        if(request()->has('raw') && request()->get('raw') == 'false')
        {
            $this->description = strip_tags($description);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * @param mixed $short_description
     * @return Accommodation
     */
    public function setShortDescription($short_description)
    {
        $this->short_description = $short_description;
        return $this;
    }



    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @param mixed $properties
     * @return BaseEntity|void
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @param $photo
     * @return $this
     */
    public function addPhoto($photo)
    {
        $this->photos[] = $photo;
        $this->image = $this->getRandomPhoto();
        return $this;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
    }

    public function getSpecialOffers()
    {
        $special_offers = [];
        foreach ($this->getUnits() as $unit) {
            $special_offers = array_merge($special_offers, $unit->getSpecialOffers());
        }

        return $special_offers;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return mixed
     */
    public function getProperties($key=null)
    {
        if(isset($key)) {
            if(array_key_exists($key, $this->properties))
                return $this->properties[$key];
            else
                return [];
        }
        return $this->properties;
    }

    public function getMeals()
    {
        return $this->getProperties('meals');
    }

    public function getMealsString()
    {
        $meals = array_keys($this->getProperties('meals'));
        return implode(', ', 
            array_map(
                function($arg){
                    return str_replace('_',' ',title_case($arg));
                }, ($meals)));


        
        return ;
    }

    public function getDistanceFromString()
    {
        $distance = $this->getProperties('distances_from');
        return implode(', ', 
            array_map(
                function($arg, $v){
                    // $v = $meals[$arg];
                    return str_replace('_',' ',title_case($arg))." ({$v}m)"; 
                }, array_keys($distance), array_values($distance)));


        
        return ;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param Unit $unit
     * @return $this
     */
    public function addUnit(Unit $unit)
    {
        $this->units[] = $unit;
        return $this;
    }

    /**
     * @param mixed $units
     * @return Accommodation
     */
    public function setUnits($units)
    {
        $this->units = $units;
        return $this;
    }

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param Destination $destination
     * @return Accommodation
     */
    public function setDestination(Destination $destination)
    {
        $this->destination = $destination;
        return $this;
    }




    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        
        $this->has_special_offers = $this->hasSpecialOffers();

        if(count($this->photos))
        {
            foreach ($this->photos as $index => $photo) {
                $this->photos[$index] = $photo->getAttributes();
            }
            $this->image = $this->image->getAttributes();
        }

        if(count($this->units))
        {
            foreach ($this->units as $index => $unit) {
                $this->units[$index] = $unit->getAttributes();
            }
        }

        if(isset($this->location))
            $this->location = $this->location->getAttributes();

        if(isset($this->destination)){
            $this->destination = $this->destination->getAttributes();
        }

        return parent::toJson($options);
    }



}