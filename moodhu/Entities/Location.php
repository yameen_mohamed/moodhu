<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/13/16
 * Time: 12:55 AM
 */

namespace Moodhu\Entities;


class Location extends BaseEntity
{
    protected $longitude;
    protected $latitude;

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     * @return Location
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     * @return Location
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }


}