<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/6/16
 * Time: 11:38 PM
 */

namespace Moodhu\Entities;


class Filter extends BaseEntity
{
    /**
     * @var
     */
    protected $id;
    /**
     * @var
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Filter
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Filter
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }



}