<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 2:13 AM
 */

namespace Moodhu\Entities;


class Photo extends BaseEntity
{
    protected $imageUrl;
    protected $thumbnailUrl;

    /**
     * @param mixed $imageUrl
     * @return Photo
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @param mixed $thumbnailUrl
     * @return Photo
     */
    public function setThumbnailUrl($thumbnailUrl)
    {
        $this->thumbnailUrl = $thumbnailUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @return mixed
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnailUrl;
    }

}