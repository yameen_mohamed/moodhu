<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/30/16
 * Time: 8:38 PM
 */

namespace Moodhu\Entities;


use Moodhu\Interfaces\iDetails;

class Tour extends BaseEntity implements iDetails
{
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $description;

    protected $short_description;
    /**
     * @var
     */
    protected $type;

    /**
     * @var
     */
    protected $rating;
    /**
     * @var
     */
    protected $photos;

    protected $categories;

    protected $image;

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }




    /**
     * @var
     */
    protected $id;

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getType()
    {
        // TODO: Implement getType() method.
    }

    public function getRating()
    {
        // TODO: Implement getRating() method.
    }


    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param mixed $id
     * @return Tour
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @param mixed $name
     * @return Tour
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param mixed $description
     * @return Tour
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param mixed $type
     * @return Tour
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param mixed $rating
     * @return Tour
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @param mixed $properties
     * @return Tour
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    public function addPhoto($image)
    {

        $this->photos[] = $image;
        $this->image = $this->getRandomPhoto();
        return $this;
    }

    /**
     * @param mixed $photos
     * @return Tour
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
        return $this;
    }


    public function getID()
    {
        return $this->id;
    }

    public function toJson($options = 0)
    {
        if(count($this->photos))
        {
            foreach ($this->photos as $index => $photo) {
                $this->photos[$index] = $photo->getAttributes();
            }

            $this->image = $this->image->getAttributes();
        }

        if(count($this->categories))
        {
            foreach ($this->categories as $index => $category) {
                $this->categories[$index] = $category->getAttributes();
            }
        }

        return parent::toJson($options);
    }

    /**
     * @param mixed $short_description
     * @return Tour
     */
    public function setShortDescription($short_description)
    {
        $this->short_description = $short_description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    function addCategory($category)
    {
        $this->categories[] = $category;
    }
    /**
     * @param mixed $categories
     * @return Tour
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }


}