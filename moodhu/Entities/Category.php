<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/6/16
 * Time: 9:39 PM
 */

namespace Moodhu\Entities;


class Category extends BaseEntity
{
    protected $name;

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

}