<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 11/29/16
 * Time: 1:55 AM
 */

namespace Moodhu\Entities;


use Illuminate\Contracts\Support\Jsonable;

class BaseEntity implements Jsonable
{
    protected $id;

    /**
     * @var
     */
    protected $properties = [];

    protected $location;

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param mixed $properties
     * @return BaseEntity
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return BaseEntity
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }


    /**
     * @param $key
     * @param string $group
     * @return bool
     */
    public function hasProperty($key, $group = '')
    {
        $data = $this->properties;


        if (array_key_exists($group, $this->properties))
            $data = $this->properties[$group];

        return array_key_exists($key, $data);
    }

    /**
     * @param $key
     * @param string $group
     * @return bool
     */
    public function getProperty($key, $group = '')
    {

        if ($this->hasProperty($key, $group)) {
            if (isset($group))
                return $this->properties[$group][$key];
            return $this->properties[$key];
        }
        return false;
    }

    public function removePropertyByKey($key)
    {
        if($this->hasProperty($key))
            unset($this->properties[$key]);
        return $this;
    }

    public function getPropertyByIndex($index)
    {
        if(count($this->properties) > $index)
        {
            return array_slice($this->properties, $index, 1);
        }
    }

    /**
     * @param $key
     * @param $value
     * @param string $group
     */
    public function addProperty($key, $value, $group = '')
    {
        if(isset($group))
        {
            if( !array_key_exists($group, $this->properties))
                $this->properties[$group] = [];
            $this->properties[$group][$key] = $value;
        }
        else
            $this->properties[$key] = $value;
    }

    public function getAttributes()
    {
        $props = get_object_vars($this);



//        if(get_class($this) == 'Moodhu\Entities\Destination')
//            dd($props,"fuck");

        foreach ($props as $index => $prop) {
            if(is_array($prop))
            {
                foreach ($prop as $key => $attribute) {


                    if(is_object($attribute) && method_exists($attribute, 'getAttributes'))
                    {


                        $prop[$key] = $attribute->getAttributes();

                    }

                }

                $props[$index] = $prop;

            }
            elseif(is_object($prop) && method_exists($prop, 'getAttributes'))
            {


                $props[$index] = $prop->getAttributes();

            }
        }

        return array_filter($props);
    }


    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->getAttributes());
    }

    public function makeKey($key)
    {
        return studly_case($key);
    }

    function __call($name, $arguments)
    {
        if(substr($name,0,3) == 'has')
        {
            $method = "get".substr($name, 3);
            return count($this->$method()) > 0;
        }
    }


    public function getRandomPhoto()
    {
        if(property_exists($this, 'photos'))
            return $this->photos[rand(0, count($this->photos)-1)];
        return null;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function fill($data = [])
    {
        foreach ($data as $field => $val) {
            if(property_exists($this, $field))
                $this->$field = $val;
        }

    }
}