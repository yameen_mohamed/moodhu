<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/7/16
 * Time: 3:43 AM
 */

namespace Moodhu\Entities;


/**
 * Class Service
 * @package Moodhu\Entities
 */
class Service extends BaseEntity
{
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Service
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }



}