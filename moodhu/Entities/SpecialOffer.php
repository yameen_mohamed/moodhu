<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 12/1/16
 * Time: 12:37 AM
 */

namespace Moodhu\Entities;


use Moodhu\Interfaces\iDetails;

class SpecialOffer extends BaseEntity implements iDetails
{
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $type;
    /**
     * Accommodation
     */
    protected $accommodation;

    /**
     * @var
     */
    protected $id;

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getType()
    {
        return $this->type;
    }

    public function getRating()
    {
        // TODO: Implement getRating() method.
    }

    public function getProperties()
    {
        // TODO: Implement getProperties() method.
    }

    public function getPhotos()
    {
        // TODO: Implement getPhotos() method.
    }

    /**
     * @return mixed
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @param mixed $id
     * @return SpecialOffer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }



    /**
     * @param mixed $name
     * @return SpecialOffer
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param mixed $type
     * @return SpecialOffer
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param mixed $accommodation
     * @return SpecialOffer
     */
    public function setAccommodation($accommodation)
    {
        $this->accommodation = $accommodation;
        return $this;
    }

    public function toJson($options = 0)
    {
        $this->accommodation = $this->accommodation->getAttributes();

        return parent::toJson($options);
    }


    public function getID()
    {
        return $this->id;
    }
}