<?php

/// *required* to use the date.timezone setting or the date_default_timezone_set() function for new DateTime()
date_default_timezone_set('UTC');

// Used for clearing wsdl cache - uncomment this line, make the API request, then comment it back. 
// The cache will be recreated with the new WSDL file. Later, repeat the process with the line containing 86400 seconds
// to rollback to the old value of the cache duration.
//ini_set('soap.wsdl_cache_ttl', '1'); 
// ini_set('soap.wsdl_cache_ttl', '86400'); 


session_start();
///generate cartID which is unique per session
if (empty($_SESSION['iTravelCartID'])) {
    $_SESSION['iTravelCartID'] = uniqid();
}

$iTravelSoapClient = null;

include_once(__DIR__ . "/Helper/AddResourcesToXML.php");
include_once(__DIR__ . "/Helper/XMLSerializer.php");
include_once(__DIR__ . "/iTravelGeneralSettings.php");

/// initialize the API
function InitAPI() {
    global $iTravelSoapClient;
    if (!isset($iTravelSoapClient)) {
        //define username and password
        $authHeader = array('Username' => iTravelGeneralSettings::$iTravelAPIUsername, 'Password' => iTravelGeneralSettings::$iTravelAPIPassword);
        $header = new SoapHeader('http://tempuri.org/', 'AuthHeader', $authHeader, false);
        $apiWebService = new SoapClient(iTravelGeneralSettings::$iTravelAPIURL, array(
																				"connection_timeout" => 120, 
                                                                                'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
                                                                                'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
        $apiWebService->__setSoapHeaders($header);
        $iTravelSoapClient = $apiWebService;
    }
    return $iTravelSoapClient;
}

function isJson($string) {
	if(!empty($string)){
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	return false;
}

function GetParameterFromGetOrPost($queryString, $defaultValue, $includeCookie = false) {
    $inputParameters = file_get_contents('php://input');
	$queryStringParameters = null;
	//if string is json, decode it with json
	if(isJson($inputParameters))
	{
		$queryStringParameters = json_decode($inputParameters, true);
	}
	else
	{
		parse_str($inputParameters, $queryStringParameters);
	}
    $returnValue = $defaultValue;

    if (isset($_GET[$queryString]) && (!empty($_GET[$queryString]) || $_GET[$queryString] == '0')) {
        $returnValue = $_GET[$queryString];
    } else if (isset($_POST[$queryString]) && (!empty($_POST[$queryString]) || $_POST[$queryString] == '0')) {
        $returnValue = $_POST[$queryString];
    }
    ///look into php:input (when webservice calls)
    else if (!empty($queryStringParameters) && isset($queryStringParameters[$queryString]) && (!empty($queryStringParameters[$queryString]) || $queryStringParameters[$queryString] == '0')) {
        $returnValue = $queryStringParameters[$queryString];
    } else if ($includeCookie && isset($_COOKIE[$queryString]) && (!empty($_COOKIE[$queryString]) || $_COOKIE[$queryString] == '0')) {
        $returnValue = $_COOKIE[$queryString];
    }

    if (!is_array($returnValue) && strtolower($returnValue) == 'false')
        $returnValue = false;

    return $returnValue;
}

function GetParameterFromGetOrPostInArray($queryString, $defaultValue) {
    $returnArray = array();
    $parameter = GetParameterFromGetOrPost($queryString, $defaultValue);
    if ($parameter != null && !empty($parameter))
        $returnArray = explode(',', $parameter);

    return $returnArray;
}

/// method used to load and transform xml file from xslt file, and then to echo the result to the page
function TransformResult($iTravelAPIResponse, $xsltParameters, $xsltStyleSheetPath, $iTravelAPIResultClassName, $iTravelAPIClassName) {
    $iTravelAPIResponseXML = '<?xml version="1.0"?>' . XMLSerializer::generateXmlFromArray($iTravelAPIResponse, "root");
    $iTravelAPIResponseXML = str_replace($iTravelAPIResultClassName, $iTravelAPIClassName, $iTravelAPIResponseXML);

    $translator = new ResourceManager(realpath($_SERVER['DOCUMENT_ROOT'] . iTravelGeneralSettings::$iTravelResourcesFolderPath));
    $resourcesXML = $translator->ResourcesToXml();
    ///read all query string and put it as parameter in xslt
    $queryStringXML = '<QueryString>';
    foreach ($_GET as $key => $value) {
        $queryStringXML .= '<' . $key . '>' . urlencode($value) . '</' . $key . '>';
    }
    $queryStringXML .= '</QueryString>';
    $iTravelAPIResponseXML = str_replace('</' . $iTravelAPIClassName . '>', $resourcesXML . $queryStringXML . '</' . $iTravelAPIClassName . '>', $iTravelAPIResponseXML);

    if (!empty($_GET['debug']) && $_GET['debug'] == '1') {
        echo htmlentities($iTravelAPIResponseXML);
    }

    $xp = new XsltProcessor();
    $xsl = new DomDocument;
	$filePath = $_SERVER['DOCUMENT_ROOT'] . $xsltStyleSheetPath;
	$filePathReal = realpath($filePath);
	/// check if the XSLT full path exists and die if if does not exist
	if (!$filePathReal)
		die('Cannot find XSLT file: ' . $filePath);
    $xsl->load($filePathReal);
    $xp->importStylesheet($xsl);
    $xml = new DomDocument;
    $xml->loadXML($iTravelAPIResponseXML);
    $xp->setParameter("", $xsltParameters);

    if ($searchBoxHtml = $xp->transformToXML($xml)) {
        $cleanedHtml = str_replace("<?xml version=\"1.0\"?>", "", $searchBoxHtml);
        return $cleanedHtml;
    } else {
        $xsltTransformError = libxml_get_last_error();
        return 'Error while processing XSLT: ' . $xsltTransformError->message;
    }
}

/// method used to load and transform xml file from xslt file, and then to echo the result to the page
function TransformAndEchoResult($iTravelAPIResponse, $xsltParameters, $xsltStyleSheetPath, $iTravelAPIResultClassName, $iTravelAPIClassName) {
    echo TransformResult($iTravelAPIResponse, $xsltParameters, $xsltStyleSheetPath, $iTravelAPIResultClassName, $iTravelAPIClassName);
}

function ticks_to_date($ticks) {
    return new DateTime(floor(($ticks - 621355968000000000) / 10000000));
}

function GeneratAttributeFilterList($additionalFilters, $currentFilterList) {
    if (!empty($additionalFilters)) {
        foreach ($additionalFilters as $additionalObjectFilter) {
			if(empty($additionalObjectFilter)){
				continue;
			}
            $filterArray = explode('_', $additionalObjectFilter);
            $comparisonType = 'Equals';
            switch ($filterArray[1]) {
                case '2':
                    $comparisonType = 'GreaterOrEqualThan';
                    break;
                case '3':
                    $comparisonType = 'LessOrEqualThan';
                    break;
                case '4':
                    $comparisonType = 'Between';
                    break;
                case '5':
                    $comparisonType = 'Like';
                    break;
                case '1':
                default:
                    $comparisonType = 'Equals';
                    break;
            }

            $filter = array(
                'AttributeID' => intval($filterArray[0]),
                'AttributeValue' => $filterArray[2],
                'ComparisonType' => $comparisonType
            );

            if (count($filterArray) > 3) {
                $filter['AttributeValue2'] = $filterArray[3];
            }

            array_push($currentFilterList, $filter);
        }
    }

    return $currentFilterList;
}

function GetSortParameterArray($sortBy, $sortOrder){
	$sortParameterList = array();
	$sortByParameter = '';
	switch (strtolower($sortBy))
	{
		case 'price':
			$sortByParameter = 'Price';
			break;
		case 'name':
			$sortByParameter = 'Name';
			break;
		case 'stars':
			$sortByParameter = 'Stars';
			break;
	}
	if($sortByParameter != ''){
		$sortOrderParameter = 'Ascending';
		if ($sortOrder == '0' || $sortOrder == 'Descending') {
			$sortOrderParameter = 'Descending';
		}
		array_push($sortParameterList, array('SortBy' => $sortByParameter, 'SortOrder' => $sortOrderParameter));
	}

	return $sortParameterList;
}

class GetAPISettings {
    #region Properties
    public $languageID;
    public $currencyID;
    public $searchSuppliers;
    public $xsltPath;
    public $searchResultsPage;
    public $showDestinationAsDropDownList;

    public $ReadBasicFromCookie;
    public $WriteBasicToCookie;
    public $ReadBasicFromQueryString;
    public $ReadAdvancedFromQueryString;
    #endregion

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);
        $this->searchSuppliers = GetParameterFromGetOrPost('searchSuppliers', '');
        $this->showDestinationAsDropDownList = true;
    }

    public function GetAPISettings()
    {    
        $getApiSettingsParameters = array(
            'LanguageID' => $this->languageID,
            'CurrencyID' => $this->currencyID,
            'SearchSuppliers' => $this->searchSuppliers
        );

        /// Call the API
        $apiSettings = InitAPI()->GetApiSettings(array('getApiSettingsParameters' => $getApiSettingsParameters));
		return $apiSettings;
	}
	
    public function GetAPISettingsHTML()
    {    
		$apiSettings = $this->GetAPISettings();
		
        ///xslt parameters
        $xsltParameters = array(
            'languageID' => $this->languageID,
            'currencyID' => $this->currencyID,
            'SearchResultsURL' => $this->searchResultsPage,
            'ShowDestinationAsDropDownList' => $this->showDestinationAsDropDownList,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL
        );
        return TransformResult($apiSettings, $xsltParameters, $this->xsltPath, "GetApiSettingsResult", "ApiSettings");
    }

    public function EchoAPISettings() {
        echo $this->GetAPISettingsHTML();
    }
	public function GetJSON(){
		$apiSettings = $this->GetAPISettings();

		$apiSettings->GetApiSettingsResult->ReadBasicFromCookie = $this->ReadBasicFromCookie;
		$apiSettings->GetApiSettingsResult->WriteBasicToCookie = $this->WriteBasicToCookie;
		$apiSettings->GetApiSettingsResult->ReadBasicFromQueryString = $this->ReadBasicFromQueryString;
		$apiSettings->GetApiSettingsResult->ReadAdvancedFromQueryString = $this->ReadAdvancedFromQueryString;
		$apiSettings->GetApiSettingsResult->SearchResultsPage = $this->searchResultsPage;

		return json_encode((array)$apiSettings);
	}
}

function formatResponse($item)
{
    
    $hotel = [
        'id'    => $item->ObjectID,
        'name' => $item->Name,
        'type' => $item->ObjectType->ObjectTypeName,
        'description' => $item->Description,
    ];

    foreach ($item->AttributeGroupList->AttributeGroup as $group) {
        $groupName = snake_case(studly_case($group->GroupName));
        foreach ($group->AttributeList->Attribute as $attr) {
            $hotel[$groupName][ snake_case(studly_case($attr->AttributeName)) ] = $attr->AttributeOriginalValue;
        }
    }

    foreach ($item->UnitList->AccommodationUnit as $key => $accom) {
        // if(!property_exists($accom->Type, 'UnitTypeName'))
        // {
        //     dd($accom);
        // }
        $accom_key = snake_case(studly_case($accom->Type->UnitTypeID));

        foreach ($accom->AttributeGroupList->AttributeGroup as $group) {
            $groupName = snake_case(studly_case($group->GroupName));
            foreach ($group->AttributeList->Attribute as $attr) {
                $hotel[$accom_key][$groupName][ snake_case(studly_case($attr->AttributeName)) ] = $attr->AttributeOriginalValue;
                
            }

        }

        if(property_exists($accom->PhotoList, 'Photo'))
        {
            foreach ($accom->PhotoList->Photo as $Photo) {
                $hotel[$accom_key]['photos'][] = [
                    'image' =>  $Photo->PhotoUrl,
                    'thumbnail' =>  $Photo->ThumbnailUrl
                ];
            }
        }


        $hotel[$accom_key]['services'] = $accom->ServiceList->Service;
        if(property_exists($accom, 'CalculatedPriceInfo'))
            $hotel[$accom_key]['pricing'] = $accom->CalculatedPriceInfo;
        $hotel[$accom_key]['booking_url'] = $accom->BookingAddress;

        
        
    }
    if(property_exists($item, 'CategoryList') && property_exists($item->CategoryList, 'Category')){
        foreach ($item->CategoryList->Category as $category) {
            $hotel['categories'][] = $category->CategoryName;
        }
    }

    if(property_exists($item->PhotoList, 'Photo'))
    {
        foreach ($item->PhotoList->Photo as $Photo) {
            $hotel['photos'][] = [
                    'image' =>  $Photo->PhotoUrl,
                    'thumbnail' =>  $Photo->ThumbnailUrl
                ];
        }
    }
    return $hotel;
}


function formatTour($_tour)
{
    $tour = [];

    $tour['name'] = $_tour->Name;

}


class SpecialOffers {
    #region Properties
    public $languageID;
    public $currencyID;
    public $searchSuppliers;
    public $xsltPath;
    public $accommodationSearchResults;
    public $searchTabID;
    public $categoryID;
    public $countryID;
    public $regionID;
    public $destinationIDList;
    public $destinationName;
    public $objectTypeID;
    public $persons;
    public $children;
    public $childrenAges;
    public $onlyOnSpecialOffer;
    public $objectTypeGroup;
    public $currentPage;
    public $ignorePriceAndAvailability;
    public $inPriceType;
    public $searchResultsXSLT;
    public $unitFilters;
    public $objectFilters;
    public $personsFilter;
    public $outParameterList;
    public $calculatedInfo;
    public $description;
    public $group;
    public $objectPhoto;
    public $toString, $to;
    public $fromString, $from;
    public $accommodationDetailedDescription;
    public $bookingForm;
    public $objectTypeGroupID;
    public $globalDestinationID;
    public $thumbnailWidth;
    public $thumbnailHeight;
    public $sortParameterList;
    public $pageSize = 10;
    public $doNotCountMandatoryServices;
    public $searchResultsResponse;
    public $categoryIntersectionID;
    public $customerID;
    public $priceFrom = null;
    public $priceTo = null;
    public $unitCategoryIDList;
    public $toDisplaySpecialOffers;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion
    
    public function __construct($defaultSortParameter = '', $defaultSortOrder = '') {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->accommodationDetailedDescription = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageAccommodationDetailedDescriptionPath, $this->languageID);
        $this->accommodationSearchResults = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageAccommodationSearchResultsPath, $this->languageID);
        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);

        $this->thumbnailWidth = 144;
        $this->thumbnailHeight = 77;
        /// Parse the parameters from query string
        $this->searchTabID = GetParameterFromGetOrPost('searchTabID', 0);
        $this->categoryID = GetParameterFromGetOrPostInArray('categoryID', 0);
        $this->destinationIDList = GetParameterFromGetOrPostInArray('destinationID', null);
        if (empty($this->destinationIDList)) {
            $this->regionID = GetParameterFromGetOrPost('regionID', 0);
            if ($this->regionID == 0) {
                $this->countryID = GetParameterFromGetOrPost('countryID', 0);
            }
        }
        $this->destinationName = GetParameterFromGetOrPost('destinationName', '');
        $this->objectTypeID = GetParameterFromGetOrPost('objectTypeID', 0);
        $this->persons = GetParameterFromGetOrPost('persons', 1);
        $this->children = GetParameterFromGetOrPost('children', 0);
        $this->childrenAges = GetParameterFromGetOrPost('childrenAges', '');
        $this->globalDestinationID = GetParameterFromGetOrPost('globalDestinationID', 0);
        $this->toDisplaySpecialOffers = GetParameterFromGetOrPost('toDisplaySpecialOffers', false);

        $this->from = new DateTime();
        $this->fromString = GetParameterFromGetOrPost('from', '');
        if (!empty($this->fromString))
            $this->from->setTimestamp($this->fromString / 1000);
        else
            $this->from = null;

        $this->to = new DateTime();
        $this->toString = GetParameterFromGetOrPost('to', '');
        // var_dump($this->to->setTimestamp($this->toString / 1000));die();
        if (!empty($this->toString))
            $this->to->setTimestamp($this->toString / 1000);
        else
            $this->to = null;

        $this->onlyOnSpecialOffer = GetParameterFromGetOrPost('onlyOnSpecialOffer', 'false');
        $this->objectTypeGroup = GetParameterFromGetOrPostInArray('objectTypeGroupID', 0);
        $this->categoryIntersectionID = GetParameterFromGetOrPostInArray('categoryIntersectionID', 0);
        $this->unitCategoryIDList = GetParameterFromGetOrPostInArray("unitCategoryIDList", 0);
        $this->currentPage = GetParameterFromGetOrPost('currentPage', 0);
        $this->objectTypeGroupID = GetParameterFromGetOrPost('objectTypeGroupID', 0);
        $this->currentPage = GetParameterFromGetOrPost('currentPage', 0);
        $this->ignorePriceAndAvailability = GetParameterFromGetOrPost('ignorePriceAndAvailability', false);
        $this->inPriceType = GetParameterFromGetOrPost('priceType', 'Total');
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);

        $this->objectPhoto = array(
            'ResponseDetail' => 'ObjectPhotos',
            'NumberOfResults' => '100'
        );
        $unitPhoto = array(
            'ResponseDetail' => 'UnitPhotos',
            'NumberOfResults' => '100'
        );
        $this->group = array(
            'ResponseDetail' => 'ObjectDetailedAttributes',
            'NumberOfResults' => '10'
        );

        $this->description = array(
            'ResponseDetail' => 'UnitDescription',
            'NumberOfResults' => '1'
        );

        if (!$this->ignorePriceAndAvailability) {

            $this->calculatedInfo = array(
                'ResponseDetail' => 'CalculatedPriceInfo',
                'NumberOfResults' => '1'
            );

            $this->outParameterList = array(
                '0' => $this->objectPhoto,
                '1' => $this->group,
                '2' => $this->description,
                '3' => $this->calculatedInfo,
                '4' => $unitPhoto
            );
        } else {
            $this->outParameterList = array(
                '0' => $this->objectPhoto,
                '1' => $this->group,
                '2' => $this->description,
                '3' => $unitPhoto
            );
        }

        $this->personsFilter = array(
            'AttributeID' => 120,
            'AttributeValue' => $this->persons,
            'ComparisonType' => 'GreaterOrEqualThan'
        );

        $this->unitFilters = array(
            '0' => $this->personsFilter
        );

        $additionalObjectFilters = GetParameterFromGetOrPostInArray('objectAttributeFilters', null);
        $this->objectFilters = array();
        $this->objectFilters = GeneratAttributeFilterList($additionalObjectFilters, $this->objectFilters);

        $additionalUnitFilters = GetParameterFromGetOrPostInArray('unitAttributeFilters', null);
        $this->unitFilters = GeneratAttributeFilterList($additionalUnitFilters, $this->unitFilters);

        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTAccommodationSearchResultsPath;
        $this->sortParameterList = array();
        $sortBy = GetParameterFromGetOrPost('sortBy', '');      
        if ($sortBy != '') {
            $sortOrder = GetParameterFromGetOrPost('sortOrder', '');
            $this->sortParameterList = GetSortParameterArray($sortBy, $sortOrder);          
        }
        else if ($defaultSortParameter != '') {
            $this->sortParameterList = GetSortParameterArray($defaultSortParameter, $defaultSortOrder);
        }
        ///add sort by priority
        array_push($this->sortParameterList, array('SortBy' => 'Priority', 'SortOrder' => 'Descending'));

        $this->pageSize = GetParameterFromGetOrPost('pageSize', $this->pageSize);
        $this->doNotCountMandatoryServices = GetParameterFromGetOrPost('doNotCountMandatoryServices', null, true);

        $this->priceFrom = GetParameterFromGetOrPost('priceFrom', '0');
        $this->priceTo = GetParameterFromGetOrPost('priceTo', '0');
    }

    public function get() {        
        $fromParameter = null;
        if ($this->from != null)
            $fromParameter = $this->from->format(DATE_ATOM);
        $toParameter = null;
        if ($this->to != null)
            $toParameter = $this->to->format(DATE_ATOM);       
        $regionIDList = null;
        if (!empty($this->regionID)) {
            $regionIDList = explode(',', $this->regionID);
        }
        $countryIDList = null;
        if (!empty($this->countryID)) {
            $countryIDList = explode(',', $this->countryID);
        }
        $globalDestinationID = null;
        if (!empty($this->globalDestinationID)) {
            $globalDestinationID = explode(',', $this->globalDestinationID);
        }

        $getSearchResultsParameters = array(
            'StartDate' => $fromParameter,
            'EndDate' => $toParameter,
            'DestinationIDList' => $this->destinationIDList,
            'RegionIDList' => $regionIDList,
            'CountryIDList' => $countryIDList,
            'UnitCategoryIDList' => $this->unitCategoryIDList,
            'DestinationName' => $this->destinationName,
            'ObjectTypeIDList' => array('0' => $this->objectTypeID),
            'ObjectTypeGroupIDList' => $this->objectTypeGroup,
            'CategoryIDListUnion' => $this->categoryID,
            'PageSize' => $this->pageSize,
            'CurrentPage' => $this->currentPage,
            'CurrencyID' => $this->currencyID,
            'LanguageID' => $this->languageID,
            'IgnorePriceAndAvailability' => $this->ignorePriceAndAvailability,
            'OnlyOnSpecialOffer' => $this->onlyOnSpecialOffer,
            'InPriceType' => $this->inPriceType,
            'OutParameterList' => $this->outParameterList,
            'UnitAttributeFilterList' => $this->unitFilters,
            'ObjectAttributeFilterList' => $this->objectFilters,
            'ThumbnailWidth' => $this->thumbnailWidth,
            'ThumbnailHeight' => $this->thumbnailHeight,
            'DestinationCodes' => $globalDestinationID,
            'SortParameterList' => $this->sortParameterList,
            'DoNotCountMandatoryServices' => $this->doNotCountMandatoryServices,
            'CategoryIDListIntersection' => $this->categoryIntersectionID,
            'CustomerID' => $this->customerID,
            'PriceFrom' => $this->priceFrom,
            'PriceTo' => $this->priceTo,
            'ToDisplaySpecialOffers' => $this->toDisplaySpecialOffers,
            'UserID' => $this->UserID
        );


        dd($getSearchResultsParameters);
        
        /// Call the API
        $searchResultsResponse = InitAPI()->GetSpecialOffers(array('getSpecialOffersParameters' => $getSearchResultsParameters));       
        if(!empty($searchResultsResponse->GetSearchResultsResult->AccommodationObjectList->AccommodationObject)){           
            foreach ($searchResultsResponse->GetSearchResultsResult->AccommodationObjectList->AccommodationObject as $accommodationObject) {
                if (isset($accommodationObject->ObjectCode)) {
                    $accommodationObject->ObjectCode = urlencode($accommodationObject->ObjectCode);
                }
            }
        }
        return $searchResultsResponse;
    }
    public function GetSearchResultsHTML($parametersArray = array()) {        
        $searchResultsResponse = $this->GetSearchResults();

        $parameters = array(
            'countryIDParameter' => $this->countryID,
            'regionIDParameter' => $this->regionID,
            'destinationIDParameter' => $this->destinationID,
            'fromParameter' => $this->fromString,
            'toParameter' => $this->toString,
            'numberOfStarsParameter' => 0,
            'personsParameter' => $this->persons,
            'childrenParameter' => $this->children,
            'childrenAgesParameter' => $this->childrenAges,
            'objectTypeIDParameter' => $this->objectTypeID,
            'objectTypeGroupID' => $this->objectTypeGroupID,
            'categoryIDParameter' => $this->categoryID,
            'ignorePriceAndAvailabilityParam' => $this->ignorePriceAndAvailability,
            'onlyOnSpecialOfferParameter' => $this->onlyOnSpecialOffer,
            'urlPrefixParameter' => '',
            'destinationName' => $this->destinationName,
            'priceFromParameter' => $this->priceFrom,
            'priceToParameter' => $this->priceTo,
            'priceTypeParameter' => $this->inPriceType,
            'postaviDirektanLink' => false,
            'BookingLinkInNewWindow' => false,
            'OpenInParent' => false,
            'detailsURL' => $this->accommodationDetailedDescription,
            'SearchPage' => $this->accommodationSearchResults,
            'bookingAddressDisplayURL' => $this->bookingForm,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'globalDestinationID' => $this->globalDestinationID,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL
        );
        
        if(!empty($parametersArray)) {
            foreach($parametersArray as $key => $value){
                $parameters[$key] = $value;
            }
        }

        return TransformResult($searchResultsResponse, $parameters, $this->xsltPath, 'GetSearchResultsResult', 'SearchResults');
    }

    public function json(){
        $searchResultsResponse = $this->get();
        dd($searchResultsResponse);
        $offers = [];
        foreach ($searchResultsResponse->GetSpecialOffersResult->SpecialOfferList->SpecialOffer as $offer) {
            $accom = $offer->AccommodationObject;
            $offer->AccommodationObject = formatResponse($accom);
            $offers[] = $offer;
        }
        return $offers;
    }
    public function EchoSearchResults($parametersArray = array()) {
        echo $this->GetSearchResultsHTML($parametersArray);
    }
}
/// class used for getting Accommodation search results
class GetSearchResults {
    #region Properties
    public $languageID;
    public $currencyID;
    public $searchSuppliers;
    public $xsltPath;
    public $accommodationSearchResults;
    public $searchTabID;
    public $categoryID;
    public $countryID;
    public $regionID;
    public $destinationIDList;
    public $destinationName;
    public $objectTypeID;
    public $persons;
    public $children;
    public $childrenAges;
    public $onlyOnSpecialOffer;
    public $objectTypeGroup;
    public $currentPage;
    public $ignorePriceAndAvailability;
    public $inPriceType;
    public $searchResultsXSLT;
    public $unitFilters;
    public $objectFilters;
    public $personsFilter;
    public $outParameterList;
    public $calculatedInfo;
    public $description;
    public $group;
    public $objectPhoto;
    public $toString, $to;
    public $fromString, $from;
    public $accommodationDetailedDescription;
    public $bookingForm;
    public $objectTypeGroupID;
    public $globalDestinationID;
    public $thumbnailWidth;
    public $thumbnailHeight;
    public $sortParameterList;
    public $pageSize = 10;
    public $doNotCountMandatoryServices;
    public $searchResultsResponse;
    public $categoryIntersectionID;
    public $customerID;
    public $priceFrom = null;
    public $priceTo = null;
    public $unitCategoryIDList;
    public $toDisplaySpecialOffers;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion
	
    public function __construct($defaultSortParameter = '', $defaultSortOrder = '') {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->accommodationDetailedDescription = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageAccommodationDetailedDescriptionPath, $this->languageID);
        $this->accommodationSearchResults = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageAccommodationSearchResultsPath, $this->languageID);
        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);

        $this->thumbnailWidth = 144;
        $this->thumbnailHeight = 77;
        /// Parse the parameters from query string
        $this->searchTabID = GetParameterFromGetOrPost('searchTabID', 0);
        $this->categoryID = GetParameterFromGetOrPostInArray('categoryID', 0);
        $this->destinationIDList = GetParameterFromGetOrPostInArray('destinationID', null);
        if (empty($this->destinationIDList)) {
            $this->regionID = GetParameterFromGetOrPost('regionID', 0);
            if ($this->regionID == 0) {
                $this->countryID = GetParameterFromGetOrPost('countryID', 0);
            }
        }
        $this->destinationName = GetParameterFromGetOrPost('destinationName', '');
        $this->objectTypeID = GetParameterFromGetOrPost('objectTypeID', 0);
        $this->persons = GetParameterFromGetOrPost('persons', 1);
        $this->children = GetParameterFromGetOrPost('children', 0);
        $this->childrenAges = GetParameterFromGetOrPost('childrenAges', '');
        $this->globalDestinationID = GetParameterFromGetOrPost('globalDestinationID', 0);
        $this->toDisplaySpecialOffers = GetParameterFromGetOrPost('toDisplaySpecialOffers', false);

        $this->from = new DateTime();
        $this->fromString = GetParameterFromGetOrPost('from', '');
        if (!empty($this->fromString))
            $this->from->setTimestamp($this->fromString / 1000);
        else
            $this->from = null;

        $this->to = new DateTime();
        $this->toString = GetParameterFromGetOrPost('to', '');
        // var_dump($this->to->setTimestamp($this->toString / 1000));die();
        if (!empty($this->toString))
            $this->to->setTimestamp($this->toString / 1000);
        else
            $this->to = null;

        $this->onlyOnSpecialOffer = GetParameterFromGetOrPost('onlyOnSpecialOffer', 'false');
        $this->objectTypeGroup = GetParameterFromGetOrPostInArray('objectTypeGroupID', 0);
        $this->categoryIntersectionID = GetParameterFromGetOrPostInArray('categoryIntersectionID', 0);
        $this->unitCategoryIDList = GetParameterFromGetOrPostInArray("unitCategoryIDList", 0);
        $this->currentPage = GetParameterFromGetOrPost('currentPage', 0);
        $this->objectTypeGroupID = GetParameterFromGetOrPost('objectTypeGroupID', 0);
        $this->currentPage = GetParameterFromGetOrPost('currentPage', 0);
        $this->ignorePriceAndAvailability = GetParameterFromGetOrPost('ignorePriceAndAvailability', false);
        $this->inPriceType = GetParameterFromGetOrPost('priceType', 'Total');
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);

        $this->objectPhoto = array(
            'ResponseDetail' => 'ObjectPhotos',
            'NumberOfResults' => '100'
        );
        $unitPhoto = array(
            'ResponseDetail' => 'UnitPhotos',
            'NumberOfResults' => '100'
        );
        $this->group = array(
            'ResponseDetail' => 'ObjectDetailedAttributes',
            'NumberOfResults' => '10'
        );

        $this->description = array(
            'ResponseDetail' => 'UnitDescription',
            'NumberOfResults' => '1'
        );

        if (!$this->ignorePriceAndAvailability) {

            $this->calculatedInfo = array(
                'ResponseDetail' => 'CalculatedPriceInfo',
                'NumberOfResults' => '1'
            );

            $this->outParameterList = array(
                '0' => $this->objectPhoto,
                '1' => $this->group,
                '2' => $this->description,
                '3' => $this->calculatedInfo,
                '4' => $unitPhoto
            );
        } else {
            $this->outParameterList = array(
                '0' => $this->objectPhoto,
                '1' => $this->group,
                '2' => $this->description,
                '3' => $unitPhoto
            );
        }

        $this->personsFilter = array(
            'AttributeID' => 120,
            'AttributeValue' => $this->persons,
            'ComparisonType' => 'GreaterOrEqualThan'
        );

        $this->unitFilters = array(
            '0' => $this->personsFilter
        );

        $additionalObjectFilters = GetParameterFromGetOrPostInArray('objectAttributeFilters', null);
        $this->objectFilters = array();
        $this->objectFilters = GeneratAttributeFilterList($additionalObjectFilters, $this->objectFilters);

        $additionalUnitFilters = GetParameterFromGetOrPostInArray('unitAttributeFilters', null);
        $this->unitFilters = GeneratAttributeFilterList($additionalUnitFilters, $this->unitFilters);

        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTAccommodationSearchResultsPath;
        $this->sortParameterList = array();
		$sortBy = GetParameterFromGetOrPost('sortBy', '');		
        if ($sortBy != '') {
			$sortOrder = GetParameterFromGetOrPost('sortOrder', '');
			$this->sortParameterList = GetSortParameterArray($sortBy, $sortOrder);			
        }
		else if ($defaultSortParameter != '') {
			$this->sortParameterList = GetSortParameterArray($defaultSortParameter, $defaultSortOrder);
        }
        ///add sort by priority
        array_push($this->sortParameterList, array('SortBy' => 'Priority', 'SortOrder' => 'Descending'));

        $this->pageSize = GetParameterFromGetOrPost('pageSize', $this->pageSize);
        $this->doNotCountMandatoryServices = GetParameterFromGetOrPost('doNotCountMandatoryServices', null, true);

        $this->priceFrom = GetParameterFromGetOrPost('priceFrom', '0');
        $this->priceTo = GetParameterFromGetOrPost('priceTo', '0');
    }

    public function GetSearchResults() {		
        $fromParameter = null;
        if ($this->from != null)
            $fromParameter = $this->from->format(DATE_ATOM);
        $toParameter = null;
        if ($this->to != null)
            $toParameter = $this->to->format(DATE_ATOM);       
        $regionIDList = null;
        if (!empty($this->regionID)) {
            $regionIDList = explode(',', $this->regionID);
        }
        $countryIDList = null;
        if (!empty($this->countryID)) {
            $countryIDList = explode(',', $this->countryID);
        }
        $globalDestinationID = null;
        if (!empty($this->globalDestinationID)) {
            $globalDestinationID = explode(',', $this->globalDestinationID);
        }

        $getSearchResultsParameters = array(
//            'StartDate' => $fromParameter,
//            'EndDate' => $toParameter,
//            'DestinationIDList' => $this->destinationIDList,
//            'RegionIDList' => $regionIDList,
//            'CountryIDList' => $countryIDList,
//            'UnitCategoryIDList' => $this->unitCategoryIDList,
//            'DestinationName' => $this->destinationName,
//            'ObjectTypeIDList' => array('0' => $this->objectTypeID),
//            'ObjectTypeGroupIDList' => $this->objectTypeGroup,
//            'CategoryIDListUnion' => $this->categoryID,
//            'PageSize' => $this->pageSize,
//            'CurrentPage' => $this->currentPage,
//            'CurrencyID' => $this->currencyID,
//            'LanguageID' => $this->languageID,
//            'IgnorePriceAndAvailability' => $this->ignorePriceAndAvailability,
//            'OnlyOnSpecialOffer' => $this->onlyOnSpecialOffer,
            'InPriceType' => $this->inPriceType,
//            'OutParameterList' => $this->outParameterList,
//            'UnitAttributeFilterList' => $this->unitFilters,
//            'ObjectAttributeFilterList' => $this->objectFilters,
//            'ThumbnailWidth' => $this->thumbnailWidth,
//            'ThumbnailHeight' => $this->thumbnailHeight,
//            'DestinationCodes' => $globalDestinationID,
//            'SortParameterList' => $this->sortParameterList,
//            'DoNotCountMandatoryServices' => $this->doNotCountMandatoryServices,
//            'CategoryIDListIntersection' => $this->categoryIntersectionID,
//            'CustomerID' => $this->customerID,
//            'PriceFrom' => $this->priceFrom,
//            'PriceTo' => $this->priceTo,
//			'ToDisplaySpecialOffers' => $this->toDisplaySpecialOffers,
//            'UserID' => $this->UserID
        );

//        dd($getSearchResultsParameters);
        
        /// Call the API
        $searchResultsResponse = InitAPI()->GetSearchResults(array('getSearchResultsParameters' => $getSearchResultsParameters));		
		if(!empty($searchResultsResponse->GetSearchResultsResult->AccommodationObjectList->AccommodationObject)){			
			foreach ($searchResultsResponse->GetSearchResultsResult->AccommodationObjectList->AccommodationObject as $accommodationObject) {
				if (isset($accommodationObject->ObjectCode)) {
					$accommodationObject->ObjectCode = urlencode($accommodationObject->ObjectCode);
				}
			}
		}
		return $searchResultsResponse;
	}
    public function GetSearchResultsHTML($parametersArray = array()) {        
		$searchResultsResponse = $this->GetSearchResults();

		$parameters = array(
            'countryIDParameter' => $this->countryID,
            'regionIDParameter' => $this->regionID,
            'destinationIDParameter' => $this->destinationID,
            'fromParameter' => $this->fromString,
            'toParameter' => $this->toString,
            'numberOfStarsParameter' => 0,
            'personsParameter' => $this->persons,
            'childrenParameter' => $this->children,
            'childrenAgesParameter' => $this->childrenAges,
            'objectTypeIDParameter' => $this->objectTypeID,
            'objectTypeGroupID' => $this->objectTypeGroupID,
            'categoryIDParameter' => $this->categoryID,
            'ignorePriceAndAvailabilityParam' => $this->ignorePriceAndAvailability,
            'onlyOnSpecialOfferParameter' => $this->onlyOnSpecialOffer,
            'urlPrefixParameter' => '',
            'destinationName' => $this->destinationName,
            'priceFromParameter' => $this->priceFrom,
            'priceToParameter' => $this->priceTo,
            'priceTypeParameter' => $this->inPriceType,
            'postaviDirektanLink' => false,
            'BookingLinkInNewWindow' => false,
            'OpenInParent' => false,
            'detailsURL' => $this->accommodationDetailedDescription,
            'SearchPage' => $this->accommodationSearchResults,
            'bookingAddressDisplayURL' => $this->bookingForm,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'globalDestinationID' => $this->globalDestinationID,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL
        );
        
        if(!empty($parametersArray)) {
            foreach($parametersArray as $key => $value){
                $parameters[$key] = $value;
            }
        }

        return TransformResult($searchResultsResponse, $parameters, $this->xsltPath, 'GetSearchResultsResult', 'SearchResults');
    }

	public function GetJSON(){
		$searchResultsResponse = $this->GetSearchResults();
		if (!empty($this->children) || $this->children == "0") {						
			if($this->children != "0")
			{
				$searchResultsResponse->GetSearchResultsResult->ChildrenAges = array_map('intval',  explode(',', $this->childrenAges));
			}
			$searchResultsResponse->GetSearchResultsResult->Children = $this->children;
		}
		$searchResultsResponse->GetSearchResultsResult->SortParameterList = $this->sortParameterList;
        $searchResultsResponse->GetSearchResultsResult->IgnorePriceAndAvailability = $this->ignorePriceAndAvailability;
		$searchResultsResponse->GetSearchResultsResult->RequestedDestinationIDList= $this->destinationIDList;		
		$searchResultsResponse->GetSearchResultsResult->DetailedDescriptionPage = $this->accommodationDetailedDescription;		
		$searchResultsResponse->GetSearchResultsResult->BookingFormPath = $this->bookingForm;		
        $accoms = [
            'length' => $searchResultsResponse->GetSearchResultsResult->TotalNumberOfResults,
            'pages' => floor($searchResultsResponse->GetSearchResultsResult->TotalNumberOfResults/$searchResultsResponse->GetSearchResultsResult->PageSize)
        ];
        foreach ($searchResultsResponse->GetSearchResultsResult->AccommodationObjectList->AccommodationObject as $accommodation) {
            $accoms['data'][] = formatResponse($accommodation);
        }
		return $accoms;
	}
    public function EchoSearchResults($parametersArray = array()) {
        echo $this->GetSearchResultsHTML($parametersArray);
    }
}

/// class used for getting Tour search results
class GetTourSearchResults {

    #region Properties
    public $currentPage;
    public $objectIDList = array();
    public $currencyID;
    public $categoryValue = "";
    public $personsValue = "";
    public $childrenValue = "";
    public $childrenAges;
    public $categoryID;
    public $priceType;
    public $priceFrom = null;
    public $priceTo = null;
    public $onlyOnSpecialOffer;
    public $objectTypeIDList = array();
    public $objectTypeGroupIDList = array();
    public $fromString, $toString;
    public $from, $to;
    public $countryID;
    public $regionID;
    public $globalDestinationID;
    public $destinationIDList;
    public $objectTypeID;
    public $objectTypeGroup;
    public $persons;
    public $children;
    public $numberOfStars;
    public $languageID;
    public $sortParameterList;
    public $sortParameter = "";
    public $outParameterList;
    public $outParameter = "";
    public $pageSize = 10;
    public $destinationName = '';
    public $airport;
    public $minNumberOfNights = 0, $maxNumberOfNights = 0;
    public $urlPrefix = "";
    public $marketIDList = array();
    public $marketIDs = 0;
    public $isRelativeUrl = false;
    public $customerID;
    public $splitResultsByHotel = false;
    public $ignoreCapacity = false;
    public $xsltPath;
    public $tourDetailedDescription;
    public $tourSearchResults;
    public $marketID;
    public $thumbnailWidth;
    public $thumbnailHeight;
    public $unitFilters;
    public $objectFilters;
    public $bookingForm;
    public $unitCategoryIDList;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion
    
    public function __construct($defaultSortParameter = '', $defaultSortOrder = '') {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->tourDetailedDescription =iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageTourDetailedDescriptionPath, $this->languageID);
        $this->tourSearchResults =iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageTourSearchResultsPath, $this->languageID);
        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);

        $this->thumbnailWidth = 152;
        $this->thumbnailHeight = 82;
        $this->currentPage = GetParameterFromGetOrPost('currentPage', 1);
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);
        $this->countryID = GetParameterFromGetOrPost('countryID', 0);
        $this->regionID = GetParameterFromGetOrPost('regionID', 0);
        $this->destinationIDList = GetParameterFromGetOrPostInArray('destinationID', null);
        $this->objectTypeID = GetParameterFromGetOrPost('objectTypeID', 0);
        $this->objectTypeGroup = explode(',', GetParameterFromGetOrPost('objectTypeGroupID', ''));
        $this->persons = GetParameterFromGetOrPost('persons', 0);
        $this->categoryID = GetParameterFromGetOrPostInArray('categoryID', 0);
        $this->childrenAges = GetParameterFromGetOrPost('childrenAges', '');
        $this->numberOfStars = GetParameterFromGetOrPost('numberOfStars', 0);
        $this->children = GetParameterFromGetOrPost('children', 0);
        $this->priceType = GetParameterFromGetOrPost('priceType', 'PerPerson');
        $this->onlyOnSpecialOffer = GetParameterFromGetOrPost('onlyOnSpecialOffer', false);
        $this->splitResultsByHotel = GetParameterFromGetOrPost('splitResultsByHotel', false);
        $this->destinationName = GetParameterFromGetOrPost('destinationName', '');
        $this->airport = GetParameterFromGetOrPost('airport', '');
        $this->objectTypeGroupID = GetParameterFromGetOrPost('objectTypeGroupID', 0);
        $this->globalDestinationID = GetParameterFromGetOrPost('globalDestinationID', 0);

        $this->from = new DateTime();
        $this->fromString = GetParameterFromGetOrPost('from', '');
        if (!empty($this->fromString))
            $this->from->setTimestamp($this->fromString / 1000);
        else
            $this->from = null;

        $this->to = new DateTime();
        $this->toString = GetParameterFromGetOrPost('to', '');
        if (!empty($this->toString))
            $this->to->setTimestamp($this->toString / 1000);
        else
            $this->to = null;


        $this->customerID = GetParameterFromGetOrPost('customerID', 0);

        $this->objectPhoto = array(
            'ResponseDetail' => 'ObjectPhotos',
            'NumberOfResults' => '100'
        );
        $unitPhoto = array(
            'ResponseDetail' => 'UnitPhotos',
            'NumberOfResults' => '100'
        );

        $this->group = array(
            'ResponseDetail' => 'ObjectDetailedAttributes',
            'NumberOfResults' => '10'
        );

        $this->description = array(
            'ResponseDetail' => 'UnitDescription',
            'NumberOfResults' => '1'
        );

        $this->outParameterList = array(
            '0' => $this->objectPhoto,
            '1' => $this->group,
            '2' => $this->description,
            '3' => $unitPhoto
        );
        if ($this->persons != 0) {
            $this->personsFilter = array(
                'AttributeID' => 120,
                'AttributeValue' => $this->persons,
                'ComparisonType' => 'GreaterOrEqualThan'
            );

            $this->unitFilters = array(
                '0' => $this->personsFilter
            );
        }
        $additionalObjectFilters = GetParameterFromGetOrPostInArray('objectAttributeFilters', null);
        $this->objectFilters = array();
        $this->objectFilters = GeneratAttributeFilterList($additionalObjectFilters, $this->objectFilters);

        $additionalUnitFilters = GetParameterFromGetOrPostInArray('unitAttributeFilters', null);
        $this->unitFilters = GeneratAttributeFilterList($additionalUnitFilters, $this->unitFilters);
        $this->unitCategoryIDList = GetParameterFromGetOrPostInArray("unitCategoryIDList", 0);


        $this->sortParameterList = array();
		$sortBy = GetParameterFromGetOrPost('sortBy', '');		
        if ($sortBy != '') {
			$sortOrder = GetParameterFromGetOrPost('sortOrder', '');
			$this->sortParameterList = GetSortParameterArray($sortBy, $sortOrder);			
        }
		else if ($defaultSortParameter != '') {
			$this->sortParameterList = GetSortParameterArray($defaultSortParameter, $defaultSortOrder);
        }
        ///add sort by priority
        array_push($this->sortParameterList, array('SortBy' => 'Priority', 'SortOrder' => 'Descending'));

        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTTourSearchResultsPath;

        $this->pageSize = GetParameterFromGetOrPost('pageSize', $this->pageSize);
    }

    public function GetSearchResults() {
        $fromParameter = null;
        if ($this->from != null)
            $fromParameter = $this->from->format(DATE_ATOM);
        $toParameter = null;
        if ($this->to != null)
            $toParameter = $this->to->format(DATE_ATOM);

        $regionIDList = null;
        if (!empty($this->regionID)) {
            $regionIDList = explode(',', $this->regionID);
        }
        $countryIDList = null;
        if (!empty($this->countryID)) {
            $countryIDList = explode(',', $this->countryID);
        }
        $childrenAgesList = null;
        if (!empty($this->childrenAges)) {
            $childrenAgesList = explode(',', $this->childrenAges);
        }
        
		$getPackageSearchResultsParameters = array(
            'StartDate' => $fromParameter,
            'EndDate' => $toParameter,
            'DestinationIDList' => $this->destinationIDList,
            'RegionIDList' => $regionIDList,
            'CountryIDList' => $countryIDList,
            'UnitCategoryIDList' => $this->unitCategoryIDList,
            'CategoryIDListUnion' => $this->categoryID,
            'CategoryIDListIntersection' => array(),
            'PriceFrom' => $this->priceFrom,
            'PriceTo' => $this->priceTo,
            'InPriceType' => $this->priceType,
            'SortParameterList' => $this->sortParameterList,
            'PageSize' => $this->pageSize,
            'CurrentPage' => $this->currentPage,
            'ObjectAttributeFilterList' => $this->objectFilters,
            'UnitAttributeFilterList' => $this->unitFilters,
            'ThumbnailWidth' => $this->thumbnailWidth,
            'ThumbnailHeight' => $this->thumbnailHeight,
            'CurrencyID' => $this->currencyID,
            'OutParameterList' => $this->outParameterList,
            'LanguageID' => $this->languageID,
            'OnlyOnSpecialOffer' => $this->onlyOnSpecialOffer,
            'ChildrenAgeList' => $childrenAgesList,
            'IsRelativeUrl' => $this->isRelativeUrl,
            'CustomerID' => $this->customerID,
            'SplitResultsByHotel' => $this->splitResultsByHotel,
            'IgnoreCapacity' => $this->ignoreCapacity,
            'MarketIDList' => array(),
            'DestinationName' => $this->destinationName,
            'Airport' => $this->airport,
            'MinNumberOfNights' => $this->minNumberOfNights,
            'MaxNumberOfNights' => $this->maxNumberOfNights,
            'DestinationCodes' => explode(',', $this->globalDestinationID),
            'UserID' => $this->UserID
        );

//        dd($getPackageSearchResultsParameters);

        $searchResults = InitAPI()->GetPackageSearchResults(array('getPackageSearchResultsParameters' => $getPackageSearchResultsParameters));

//        dd($searchResults);

		return $searchResults;		
    }

	public function GetSearchResultsHTML() {
		$searchResultsResponse = $this->GetSearchResults();

		$parameters = array(
            'urlPrefixParameter' => '',
            'fromParameter' => $this->fromString,
            'toParameter' => $this->toString,
            'countryIDParameter' => $this->countryID,
            'regionIDParameter' => $this->regionID,
            'destinationIDParameter' => implode(',', $this->destinationIDList),
            'objectTypeIDParameter' => $this->objectTypeID,
            'objectTypeGroupIDParameter' => implode(',', $this->objectTypeGroup),
            'personsParameter' => $this->persons,
            'childrenParameter' => $this->children,
            'childrenAgesParameter' => '',
            'numberOfStarsParameter' => $this->numberOfStars,
            'categoryIDParameter' => implode(',', $this->categoryID),
            'priceTypeParameter' => $this->priceType,
            'onlyOnSpecialOfferParameter' => $this->onlyOnSpecialOffer,
            'splitResultsByHotel' => $this->splitResultsByHotel,
            'objectTypeGroupID' => implode(',', $this->objectTypeGroup),
            'affiliateID' => '',
            'detailsURL' => $this->tourDetailedDescription,
            'SearchPage' => $this->tourSearchResults,
            'destinationName' => $this->destinationName,
            'airport' => $this->airport,
            'packageTourDuration' => $this->minNumberOfNights,
            'ClientWebAddress' => '',
            'bookingAddressDisplayURL' => $this->bookingForm,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL
        );
        return TransformResult($searchResultsResponse, $parameters, $this->xsltPath, 'GetPackageSearchResultsResult', 'PackageSearchResults');
	}

	public function GetJSON(){
		$searchResultsResponse = $this->GetSearchResults();
		if (!empty($this->children) || $this->children == "0") {						
			if($this->children != "0")
			{
				$searchResultsResponse->GetPackageSearchResultsResult->ChildrenAges = array_map('intval',  explode(',', $this->childrenAges));
			}
			$searchResultsResponse->GetPackageSearchResultsResult->Children = $this->children;
		}
		$searchResultsResponse->GetPackageSearchResultsResult->SortParameterList = $this->sortParameterList;
        $searchResultsResponse->GetPackageSearchResultsResult->PriceType = $this->priceType;
        $searchResultsResponse->GetPackageSearchResultsResult->RequestedDestinationIDList = $this->destinationIDList;
        $searchResultsResponse->GetPackageSearchResultsResult->DetailedDescriptionPage = $this->tourDetailedDescription;
		$searchResultsResponse->GetPackageSearchResultsResult->BookingFormPath = $this->bookingForm;		
		$searchResultsResponse->GetPackageSearchResultsResult->IgnoreCapacity = $this->ignoreCapacity;

        // $tours = [];
        // foreach ($searchResultsResponse->GetPackageSearchResultsResult->PackageTourList as $tour) {
        //     $tours[] = ($tour);
        // }
		return (array)$searchResultsResponse->GetPackageSearchResultsResult->PackageTourList->PackageTour;
	}

	public function EchoSearchResults() {
        echo $this->GetSearchResultsHTML();
    }
}

/// class used for getting Accommodation detailed description
class GetDetailedDescription {
    #region Properties
    public $ignorePriceAndAvailability;
    public $ticksFrom1970;
    public $from;
    public $fromString;
    public $to;
    public $toString;
    public $persons;
    public $objectID;
    public $languageID;
    public $currencyID;
    public $objectURL;
    public $xsltPath;
    public $bookingForm;
    public $objectCode;
    public $cartID;
    public $childrenAges;
    public $doNotCountMandatoryServices;
    public $customerID;
    public $thumbnailWidth;
    public $thumbnailHeight;
	public $detailedDescriptionPage;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->detailedDescriptionPage = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageAccommodationDetailedDescriptionPath, $this->languageID);
        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);
        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTAccommodationDetailedDescriptionPath;

        $this->ignorePriceAndAvailability = GetParameterFromGetOrPost('ignorePriceAndAvailability', false);

        $this->from = new DateTime();
        $this->fromString = GetParameterFromGetOrPost('from', 0, true);
        if (!empty($this->fromString))
            $this->from->setTimestamp($this->fromString / 1000);
        else
            $this->from = null;

        $this->to = new DateTime();
        $this->toString = GetParameterFromGetOrPost('to', 0, true);
        if (!empty($this->toString))
            $this->to->setTimestamp($this->toString / 1000);
        else
            $this->to = null;

        $this->persons = GetParameterFromGetOrPost('persons', 1, true);
        $this->objectID = GetParameterFromGetOrPost('objectID', 0);
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);
        $this->objectURL = GetParameterFromGetOrPost('objectURL', null);
        $this->objectCode = GetParameterFromGetOrPost('objectCode', null);
        $this->cartID = $_SESSION['iTravelCartID'];
        $this->childrenAges = GetParameterFromGetOrPost('childrenAges', '', true);
        $this->doNotCountMandatoryServices = GetParameterFromGetOrPost('doNotCountMandatoryServices', null, true);
        $this->thumbnailWidth = 181;
        $this->thumbnailHeight = 132;
    }

    public function GetDetailedDescription($id) {
        $fromParameter = null;
        if ($this->from != null)
            $fromParameter = $this->from->format(DATE_ATOM);
        $toParameter = null;
        if ($this->to != null)
            $toParameter = $this->to->format(DATE_ATOM);
        $childrenAgesParam = array();
        if (!empty($this->childrenAges) || $this->childrenAges == "0") {
            $childrenAgesParam = explode(',', $this->childrenAges);
        }

        if ($this->objectURL != null and isset($this->objectURL)) {
            $getDetailedDescriptionParameters = array(
                'StartDate' => $fromParameter,
                'EndDate' => $toParameter,
                'NumberOfPersons' => $this->persons,
                'CurrencyID' => $this->currencyID,
                'ObjectURL' => $this->objectURL,
                'ObjectCode' => $this->objectCode,
                'ChildrenAgeList' => $childrenAgesParam,
                'DoNotCountMandatoryServices' => $this->doNotCountMandatoryServices,
                'CustomerID' => $this->customerID,
				'ThumbnailWidth' => $this->thumbnailWidth,
				'ThumbnailHeight' => $this->thumbnailHeight,
                'UserID' => $this->UserID
            );
        } else {
            $getDetailedDescriptionParameters = array(
                'ObjectID' => $id,
                'StartDate' => $fromParameter,
                'EndDate' => $toParameter,
                'NumberOfPersons' => $this->persons,
                'LanguageID' => $this->languageID,
                'CurrencyID' => $this->currencyID,
                'ObjectCode' => $this->objectCode,
                'ObjectURL' => null,
                'ChildrenAgeList' => $childrenAgesParam,
                'DoNotCountMandatoryServices' => $this->doNotCountMandatoryServices,
                'CustomerID' => $this->customerID,
				'ThumbnailWidth' => $this->thumbnailWidth,
				'ThumbnailHeight' => $this->thumbnailHeight,
                'UserID' => $this->UserID
            );
        }

        dd($getDetailedDescriptionParameters);


        $accommodationObjectDetails = InitAPI()->GetDetailedDescription(array('getDetailedDescriptionParameters' => $getDetailedDescriptionParameters));

		return $accommodationObjectDetails;
	}

	public function GetJSON(){
		$accommodationObjectDetails = $this->GetDetailedDescription();
        if (!empty($this->childrenAges) || $this->childrenAges == "0") {			
			$accommodationObjectDetails->GetDetailedDescriptionResult->ChildrenAges = array_map('intval',  explode(',', $this->childrenAges));
		}
		$accommodationObjectDetails->GetDetailedDescriptionResult->CustomerID = $this->customerID;
		$accommodationObjectDetails->GetDetailedDescriptionResult->DetailedDescriptionPage = $this->detailedDescriptionPage;
		$accommodationObjectDetails->GetDetailedDescriptionResult->BookingFormPath = $this->bookingForm;
		return json_encode((array)$accommodationObjectDetails);
	}

	public function GetDetailedDescriptionHTML() {		
		$accommodationObjectDetails = $this->GetDetailedDescription();
		$childrenAgesParam = array();
        if (!empty($this->childrenAges) || $this->childrenAges == "0") {
            $childrenAgesParam = explode(',', $this->childrenAges);
        }

        $parameters = array(
            'languageID' => $this->languageID,
            'currencyID' => $this->currencyID,
            'unitActivityStatus' => '0,1,2',
            'bookingAddressDisplayURL' => $this->bookingForm,
            'cartIDParameter' => $this->cartID,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL,
            'childrenAgesParam' => $this->childrenAges,
            'childrenParam' => count($childrenAgesParam)
        );

        return TransformResult($accommodationObjectDetails, $parameters, $this->xsltPath, 'GetDetailedDescriptionResult', 'AccommodationObjectDetails');
    }

    public function EchoGetDetailedDescription() {
        echo $this->GetDetailedDescriptionHTML();
    }

}

class Date {
    public $StartDate;
    public $EndDate;
    public $NumberOfDays;
}

/// class used for getting Tour detailed description
class GetTourDetailedDescription {

    #region Properties
    public $objectID;
    public $objectIDList;
    public $period;
    public $packageTourID;
    public $currencyID;
    public $languageID;
    public $packageTourCode;
    public $numberOfDays = 0;
    public $persons = 0;
    public $numberOfPersons = 0;
    public $customerID = 0;
    public $marketID = 0;
    public $EncryptedAffiliateID = 0;
    public $isRelativeURL = false;
    public $packageDetailsXSLTStyleSheet;
    public $bookingForm;
    public $xsltPath;
    public $from, $to, $fromString, $toString;
    public $periods;
    public $objectURL;
    public $thumbnailWidth;
    public $thumbnailHeight;
    public $photoWidth;
    public $photoHeight;
	public $detailedDescriptionPage;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
		$this->detailedDescriptionPage = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageTourDetailedDescriptionPath, $this->languageID);
        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTTourDetailedDescriptionPath;
        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);

        $this->thumbnailWidth = 191;
        $this->thumbnailHeight = 142;

        $this->objectID = GetParameterFromGetOrPost('objectID', 0);
        $this->objectIDList = GetParameterFromGetOrPost('objectIDList', 0);
        $this->period = GetParameterFromGetOrPost('period', null, true);
        $this->packageTourID = GetParameterFromGetOrPost('packageTourID', 0);
        $this->packageTourCode = GetParameterFromGetOrPost('packageTourCode', "");
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);
        $this->numberOfDays = GetParameterFromGetOrPost('numberOfDays', 1);
        $this->persons = GetParameterFromGetOrPost('persons', 0, true);
        $this->objectURL = GetParameterFromGetOrPost('objectURL', '');
    }

	public function GetTourDetailedDescription() {
        if (!empty($this->period)) {
            $periodStartDate = new DateTime($this->period);
            $periodEndDate = new DateTime($this->period);
            $periodEndDate->add(new DateInterval('P' . ($this->numberOfDays - 1) . 'D'));

            $this->periods = array();

            $datePeriod = (object) array('Date' => (object) array('StartDate' => $periodStartDate->format('Y-m-d'), 'EndDate' => $periodEndDate->format('Y-m-d'), 'NumberOfDays' => $this->numberOfDays));
            array_push($this->periods, $datePeriod->Date);
        } else {
            $this->periods = array();
        }

        $objectIDListParameter = array();
        if (!empty($this->objectIDList)) {
            $objectIDListParameter = explode(',', $this->objectIDList);
        } else if (!empty($this->objectID)) {
            $objectIDListParameter = array('0' => $this->objectID);
        }
        $getPackageDetailedDescriptionParameters = array(
            'NumberOfPersons' => $this->persons,
            'PackageTourID' => $this->packageTourID,
            'ObjectIDList' => $objectIDListParameter,
            'Periods' => $this->periods,
            'ThumbnailWidth' => $this->thumbnailWidth,
            'ThumbnailHeight' => $this->thumbnailHeight,
            'PhotoWidth' => $this->photoWidth,
            'PhotoHeight' => $this->photoHeight,
            'CurrencyID' => $this->currencyID,
            'LanguageID' => $this->languageID,
            'ListUnitActivityStatus' => '0,1,2',
            'ObjectURL' => $this->objectURL,
            'PackageTourCode' => $this->packageTourCode,
            'IsRelativeURL' => $this->isRelativeURL,
            'CustomerID' => $this->customerID,
            'MarketID' => $this->marketID,
            'UserID' => $this->UserID
        );

        $requestParameter = array('getPackageDetailedDescriptionParameters' => $getPackageDetailedDescriptionParameters);
        $packageTourDetails = InitAPI()->GetPackageDetailedDescription($requestParameter);

		return $packageTourDetails;
	}

	public function GetJSON(){
        $packageTourDetails = $this->GetTourDetailedDescription();
		$packageTourDetails->GetPackageDetailedDescriptionResult->CustomerID = $this->customerID;
		$packageTourDetails->GetPackageDetailedDescriptionResult->DetailedDescriptionPage = $this->detailedDescriptionPage;
		$packageTourDetails->GetPackageDetailedDescriptionResult->BookingFormPath = $this->bookingForm;
		return json_encode((array)$packageTourDetails);
	}

    public function GetTourDetailedDescriptionHTML() {        
        $packageTourDetails = $this->GetTourDetailedDescription();

        $objectIDParameter = 0;
        if (is_array($packageTourDetails->GetPackageDetailedDescriptionResult->PackageTour->PackageUnitList->PackageUnit)) {
            foreach ($packageTourDetails->GetPackageDetailedDescriptionResult->PackageTour->PackageUnitList->PackageUnit as $packageUnit) {
                $accommodationObjectFound = false;
                if (is_array($packageUnit->PackagePeriodList->PackagePeriod)) {
                    foreach ($packageUnit->PackagePeriodList->PackagePeriod as $packagePeriod) {
                        if ($packagePeriod->Visible == true && $packageUnit->AccommodationUnitID != 0) {
                            if (is_array($packageTourDetails->GetPackageDetailedDescriptionResult->AccommodationObjectList->AccommodationObject)) {
                                foreach ($packageTourDetails->GetPackageDetailedDescriptionResult->AccommodationObjectList->AccommodationObject as $accommodationObject) {
                                    if (is_array($accommodationObject->UnitList->AccommodationUnit)) {
                                        foreach ($accommodationObject->UnitList->AccommodationUnit as $accommodationUnit) {
                                            if ($accommodationUnit->UnitID == $packageUnit->AccommodationUnitID) {
                                                $objectIDParameter = $accommodationObject->ObjectID;
                                                $accommodationObjectFound = true;
                                                break;
                                            }
                                        }
                                    } else {
                                        if ($accommodationObject->UnitList->AccommodationUnit->UnitID == $packageUnit->AccommodationUnitID) {
                                            $objectIDParameter = $accommodationObject->ObjectID;
                                            $accommodationObjectFound = true;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                $accommodationObject = $packageTourDetails->GetPackageDetailedDescriptionResult->AccommodationObjectList->AccommodationObject;
                                if (is_array($accommodationObject->UnitList->AccommodationUnit)) {
                                    foreach ($accommodationObject->UnitList->AccommodationUnit as $accommodationUnit) {
                                        if ($accommodationUnit->UnitID == $packageUnit->AccommodationUnitID) {
                                            $objectIDParameter = $accommodationObject->ObjectID;
                                            $accommodationObjectFound = true;
                                            break;
                                        }
                                    }
                                } else {
                                    if ($accommodationObject->UnitList->AccommodationUnit->UnitID == $packageUnit->AccommodationUnitID) {
                                        $objectIDParameter = $accommodationObject->ObjectID;
                                        $accommodationObjectFound = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if ($accommodationObjectFound)
                    break;
            }
        }

        ///re-format period string
        if (!empty($this->period)) {
            try {
                $tempDate = new DateTime($this->period);
                $this->period = $tempDate->format('d.m.Y');
            }
            catch (Exception $e) {
                
            }
        }

        $parameters = array(
            'periodParameter' => $this->period,
            'numberOfDaysParameter' => $this->numberOfDays,
            'affiliateIDParameter' => $this->EncryptedAffiliateID,
            'languageID' => $this->languageID,
            'currencyID' => $this->currencyID,
            'bookingAddressDisplayURL' => $this->bookingForm,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL,
            'objectIDParameter' => $objectIDParameter
        );

        return TransformResult($packageTourDetails, $parameters, $this->xsltPath, 'GetPackageDetailedDescriptionResult', 'PackageTourDetails');
    }

    public function EchoTourDetailedDescription() {
        echo $this->GetTourDetailedDescriptionHTML();
    }

}

/// class used for getting Transportation search results
class GetTransportationSearchResults {

    #region Properties
    public $currentPage;
    public $dateFrom, $dateTo;
    public $languageID;
    public $currencyID;
    public $categoryID;
    public $personsValue;
    public $childrenValue;
    public $childrenAges;
    public $onlyOnSpecialOffer;
    public $isReturnTransfer = false;
    public $ignorePriceAndAvailability = false;
    public $from, $fromString;
    public $to, $toString;
    public $pickupDestinationIDList;
    public $pickupRegionID;
    public $pickupCountryID;
    public $pickupDestinationLevel;
    public $dropoffDestinationIDList;
    public $dropoffDestinationLevel;
    public $objectTypeID;
    public $objectTypeGroupID;
    public $persons;
    public $children;
    public $numberOfStars;
    public $priceType;
    public $toDisplaySpecialOffers;
    public $categoryIntersectionID;
    public $globalDestinationID;
    public $searchSupplier;
    public $destinationID;
    public $countryID;
    public $isRelativeURL = false;
    public $transportationDetailedDescription;
    public $transportationSearchResults;
    public $openInParent = false;
    public $urlPrefix = "/";
    public $objectFilters;
    public $personsFilter;
    public $unitFilters;
    public $outParameterList;
    public $sortParameterList;
    public $person = '';
    public $priceFrom = null;
    public $priceTo = null;
    public $bookingForm;
    public $xsltPath;
    public $EncryptedAffiliateID;
    public $regionID;
    public $thumbnailWidth;
    public $thumbnailHeight;
    public $dropoffRegionID;
    public $dropoffCountryID;
    public $pageSize = 10;
    public $unitCategoryIDList;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion
    
    public function __construct($defaultSortParameter = '', $defaultSortOrder = '') {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);
        $this->transportationSearchResults = iTravelGeneralSettings::$iTravelPageTransportationSearchResultsPath;
        $this->transportationDetailedDescription = iTravelGeneralSettings::$iTravelPageTransportationDetailedDescriptionPath;

        $this->thumbnailWidth = 152;
        $this->thumbnailHeight = 82;

        $this->from = new DateTime();
        $this->fromString = GetParameterFromGetOrPost('from', '');
        if (!empty($this->fromString))
            $this->from->setTimestamp($this->fromString / 1000);
        else
            $this->from = null;

        $this->to = new DateTime();
        $this->toString = GetParameterFromGetOrPost('to', '');
        if (!empty($this->toString))
            $this->to->setTimestamp($this->toString / 1000);
        else
            $this->to = null;

        $this->ignorePriceAndAvailability = GetParameterFromGetOrPost('ignorePriceAndAvailability', false);
        $this->currentPage = GetParameterFromGetOrPost('currentPage', 1);
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);
        $this->pickupDestinationIDList = GetParameterFromGetOrPostInArray('pickupDestinationID', null);
        $this->pickupRegionID = GetParameterFromGetOrPost('pickupRegionID', 0);
        $this->pickupCountryID = GetParameterFromGetOrPost('pickupCountryID', 0);
        $this->dropoffDestinationIDList = GetParameterFromGetOrPostInArray('dropoffDestinationID', null);
        $this->dropoffRegionID = GetParameterFromGetOrPost('dropoffRegionID', 0);
        $this->dropoffCountryID = GetParameterFromGetOrPost('dropoffCountryID', 0);
        $this->pickupDestinationLevel = GetParameterFromGetOrPost('pickupDestinationLevel', 0);
        $this->dropoffDestinationLevel = GetParameterFromGetOrPost('dropoffDestinationLevel', 0);
        $this->objectTypeID = GetParameterFromGetOrPost('objectTypeID', 0);
        $this->objectTypeGroupID = GetParameterFromGetOrPost('objectTypeGroupID', 0);
        $this->persons = GetParameterFromGetOrPost('persons', 1);
        $this->children = GetParameterFromGetOrPost('children', 0);
        $this->childrenAges = GetParameterFromGetOrPostInArray('childrenAges', null);

        $this->numberOfStars = GetParameterFromGetOrPost('numberOfStars', 0);
        $this->categoryID = GetParameterFromGetOrPostInArray('categoryID', 0);

        $this->onlyOnSpecialOffer = GetParameterFromGetOrPost('onlyOnSpecialOffer', false);
        $this->isReturnTransfer = GetParameterFromGetOrPost('isReturnTransfer', false);

        $this->priceType = GetParameterFromGetOrPost('priceType', 'Total');
        $this->toDisplaySpecialOffers = GetParameterFromGetOrPost('toDisplaySpecialOffers', false);
        $this->categoryIntersectionID = GetParameterFromGetOrPost('categoryIntersectionID', 0);
        $this->unitCategoryIDList = GetParameterFromGetOrPostInArray("unitCategoryIDList", 0);
        $this->globalDestinationID = GetParameterFromGetOrPost('globalDestinationID', 0);
        $this->searchSupplier = GetParameterFromGetOrPost('searchSupplier', 0);
        $this->destinationID = GetParameterFromGetOrPost('destinationID', 0);
        $this->regionID = GetParameterFromGetOrPost('regionID', 0);
        $this->countryID = GetParameterFromGetOrPost('countryID', 0);

        $this->objectPhoto = array(
            'ResponseDetail' => 'ObjectPhotos',
            'NumberOfResults' => '100'
        );

        $unitPhoto = array(
            'ResponseDetail' => 'UnitPhotos',
            'NumberOfResults' => '100'
        );
        $this->group = array(
            'ResponseDetail' => 'ObjectDetailedAttributes',
            'NumberOfResults' => '10'
        );

        $this->description = array(
            'ResponseDetail' => 'UnitDescription',
            'NumberOfResults' => '1'
        );

        if (!$this->ignorePriceAndAvailability) {

            $this->calculatedInfo = array(
                'ResponseDetail' => 'CalculatedPriceInfo',
                'NumberOfResults' => '1'
            );

            $this->outParameterList = array(
                '0' => $this->objectPhoto,
                '1' => $this->group,
                '2' => $this->description,
                '3' => $this->calculatedInfo,
                '4' => $unitPhoto
            );
        } else {
            $this->outParameterList = array(
                '0' => $this->objectPhoto,
                '1' => $this->group,
                '2' => $this->description,
                '3' => $unitPhoto
            );
        }

        $this->personsFilter = array(
            'AttributeID' => 120,
            'AttributeValue' => $this->persons,
            'ComparisonType' => 'GreaterOrEqualThan'
        );

        $this->unitFilters = array(
            '0' => $this->personsFilter
        );

        $additionalObjectFilters = GetParameterFromGetOrPostInArray('objectAttributeFilters', null);
        $this->objectFilters = array();
        $this->objectFilters = GeneratAttributeFilterList($additionalObjectFilters, $this->objectFilters);

        $additionalUnitFilters = GetParameterFromGetOrPostInArray('unitAttributeFilters', null);
        $this->unitFilters = GeneratAttributeFilterList($additionalUnitFilters, $this->unitFilters);


        $this->sortParameterList = array();
		$sortBy = GetParameterFromGetOrPost('sortBy', '');		
		if ($sortBy != '') {
			$sortOrder = GetParameterFromGetOrPost('sortOrder', '');
			$this->sortParameterList = GetSortParameterArray($sortBy, $sortOrder);			
        }
		else if ($defaultSortParameter != '') {
			$this->sortParameterList = GetSortParameterArray($defaultSortParameter, $defaultSortOrder);
        }		
        ///add sort by priority
        array_push($this->sortParameterList, array('SortBy' => 'Priority', 'SortOrder' => 'Descending'));
        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTTransportationSearchResultsPath;

        $this->pageSize = GetParameterFromGetOrPost('pageSize', $this->pageSize);
    }

    public function GetTransportationSearchResults() {
        $fromParameter = null;
        if ($this->from != null)
            $fromParameter = $this->from->format(DATE_ATOM);
        $toParameter = null;
        if ($this->to != null)
            $toParameter = $this->to->format(DATE_ATOM);

        $pickupRegionIDList = null;
        if (!empty($this->pickupRegionID)) {
            $pickupRegionIDList = explode(',', $this->pickupRegionID);
        }
        $pickupCountryIDList = null;
        if (!empty($this->pickupCountryID)) {
            $pickupCountryIDList = explode(',', $this->pickupCountryID);
        }
        $dropoffRegionIDList = null;
        if (!empty($this->dropoffRegionID)) {
            $dropoffRegionIDList = explode(',', $this->dropoffRegionID);
        }
        $dropoffCountryIDList = null;
        if (!empty($this->dropoffCountryID)) {
            $dropoffCountryIDList = explode(',', $this->dropoffCountryID);
        }

        $getTransportationSearchResultsParameters = array(
            'StartDate' => $fromParameter,
            'EndDate' => $toParameter,
            'PickupDestinationIDList' => $this->pickupDestinationIDList,
            'PickupRegionIDList' => $pickupRegionIDList,
            'PickupCountryIDList' => $pickupCountryIDList,
            'DropoffDestinationIDList' => $this->dropoffDestinationIDList,
            'DropoffRegionIDList' => $dropoffRegionIDList,
            'DropoffCountryIDList' => $dropoffCountryIDList,
            'ObjectTypeIDList' => array("0" => $this->objectTypeID),
            'ObjectTypeGroupIDList' => explode(',', $this->objectTypeGroupID),
            'CategoryIDListUnion' => $this->categoryID,
            'PriceFrom' => $this->priceFrom,
            'PriceTo' => $this->priceTo,
            'InPriceType' => $this->priceType,
            'SortParameterList' => $this->sortParameterList,
            'PageSize' => $this->pageSize,
            'CurrentPage' => $this->currentPage,
            'ObjectAttributeFilterList' => $this->objectFilters,
            'UnitAttributeFilterList' => $this->unitFilters,
            'ThumbnailWidth' => $this->thumbnailWidth,
            'ThumbnailHeight' => $this->thumbnailHeight,
            'CurrencyID' => $this->currencyID,
            'OutParameterList' => $this->outParameterList,
            'LanguageID' => $this->languageID,
            'IgnorePriceAndAvailability' => $this->ignorePriceAndAvailability,
            'UnitTypeIDList' => array(),
            'UnitCategoryIDList' => $this->unitCategoryIDList,
            'CategoryIDListIntersection' => array(),
            'OnlyOnSpecialOffer' => false,
            'ChildrenAgeList' => $this->childrenAges,
            'IsRelativeUrl' => $this->isRelativeURL,
            'CustomerID' => 0,
            'AffiliateID' => 0,
            'DestinationCodes' => explode(',', $this->globalDestinationID),
            'SearchSupplierList' => array(),
            'UserID' => $this->UserID
        );       

        $transportationSearchResults = InitAPI()->GetTransportationSearchResults(array('getTransportationSearchResultsParameters' => $getTransportationSearchResultsParameters));
		return $transportationSearchResults;
    }
	
	public function GetJSON(){
        $transportationSearchResults = $this->GetTransportationSearchResults();
		
		$transportationSearchResults->GetTransportationSearchResultsResult->SortParameterList = $this->sortParameterList;
        $transportationSearchResults->GetTransportationSearchResultsResult->IgnorePriceAndAvailability = $this->ignorePriceAndAvailability;
        $transportationSearchResults->GetTransportationSearchResultsResult->PriceType = $this->priceType;	
		$transportationSearchResults->GetTransportationSearchResultsResult->RequestedPickupDestinationIDList= $this->pickupDestinationIDList;		
		$transportationSearchResults->GetTransportationSearchResultsResult->RequestedDropoffDestinationIDList= $this->dropoffDestinationIDList;
		$transportationSearchResults->GetTransportationSearchResultsResult->DetailedDescriptionPage = $this->transportationDetailedDescription;
		$transportationSearchResults->GetTransportationSearchResultsResult->BookingFormPath = $this->bookingForm;
		return json_encode((array)$transportationSearchResults);
	}

    public function GetTransportationSearchResultsHTML() {
        $transportationSearchResults = $this->GetTransportationSearchResults();
		
		$parameters = array(
		   'urlPrefixParameter' => $this->urlPrefix,
		   'OpenInParent' => $this->openInParent,
		   'ignorePriceAndAvailabilityParam' => $this->ignorePriceAndAvailability,
		   'fromParameter' => $this->fromString,
		   'toParameter' => $this->toString,
		   'pickupDestinationLevelParameter' => 0,
		   'pickupDestinationIDParameter' => $this->pickupDestinationID,
		   'pickupRegionIDParameter' => $this->pickupRegionID,
		   'pickupCountryIDParameter' => $this->pickupCountryID,
		   'dropoffDestinationLevelParameter' => 0,
		   'dropoffDestinationIDParameter' => 0,
           'objectTypeIDParameter' => $this->objectTypeID,
           'objectTypeGroupID' => $this->objectTypeGroupID,
		   'numberOfStarsParameter' => 0,
		   'childrenAgesParameter' => 0,
		   'childrenParameter' => 0,
		   'personsParameter' => 0,
		   'categoryIDParameter' => 0,
		   'onlyOnSpecialOfferParameter' => false,
		   'isReturnTransfer' => $this->isReturnTransfer,
		   'affiliateID' => $this->EncryptedAffiliateID,
		   'priceTypeParameter' => $this->priceType,
		   'bookingAddressDisplayURL' => $this->bookingForm,
		   'detailsURL' => $this->transportationDetailedDescription,
		   'SearchPage' => $this->transportationSearchResults,
		   'postaviDirektanLink' => false,
		   'BookingLinkInNewWindow' => false,
		   'globalDestinationID' => $this->globalDestinationID,
		   'searchSupplier' => 0,
		   'ClientWebAddress' => '',
		   'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
		   'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
		   'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
		   'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL
	   );

        return TransformResult($transportationSearchResults, $parameters, $this->xsltPath, 'GetTransportationSearchResultsResult', 'TransportationSearchResults');
	}

	function EchoTransportationSearchResults()
	{
		echo $this->GetTransportationSearchResultsHTML();
	}
}

/// class used for getting Transportation detailed description
class GetTransportationDetailedDescription {

    #region Properties
    public $ignorePriceAndAvailability = false;
    public $from, $fromString;
    public $to, $toString;
    public $persons;
    public $children;
    public $childrenAges;
    public $objectID;
    public $objectCode;
    public $provider;
    public $searchName = '';
    public $searchDestination;
    public $startDate;
    public $endDate;
    public $languageID;
    public $currencyID;
    public $tab;
    public $unitActivityStatus;
    public $objectURL;
    public $priceType;
    public $pickUpDestinationID;
    public $dropOffDestinationID;
    public $marketID = 0;
    public $customerID = 0;
    public $cartID = 0;
    public $xsltPath;
    public $bookingForm;
    public $EncryptedAffiliateID = 0;
    public $bookingAdresa = ''; 
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;
    #endregion

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);

        $this->bookingForm = iTravelGeneralSettings::PagePath(iTravelGeneralSettings::$iTravelPageBookingFormPath, $this->languageID);

        $this->ignorePriceAndAvailability = GetParameterFromGetOrPost('ignorePriceAndAvailability', false);

        $this->from = new DateTime();
        $this->fromString = GetParameterFromGetOrPost('from', 0, true);
        if (!empty($this->fromString))
            $this->from->setTimestamp($this->fromString / 1000);
        else
            $this->from = null;

        $this->to = new DateTime();
        $this->toString = GetParameterFromGetOrPost('to', 0, true);
        if (!empty($this->toString))
            $this->to->setTimestamp($this->toString / 1000);
        else
            $this->to = null;

        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);
        $this->persons = GetParameterFromGetOrPost('persons', 1, true);
        $this->childrenAges = GetParameterFromGetOrPost('childrenAges', '', true);
        $this->objectID = GetParameterFromGetOrPost('objectID', 0);
        $this->objectCode = GetParameterFromGetOrPost('objectCode', null);
        $this->provider = GetParameterFromGetOrPost('provider', 0);
        $this->searchName = GetParameterFromGetOrPost('searchName', '');
        $this->searchDestination = GetParameterFromGetOrPost('searchDestination', 0);
        $this->tab = GetParameterFromGetOrPost('tab', 0);
        $this->objectURL = GetParameterFromGetOrPost('objectURL', '');
        $this->priceType = GetParameterFromGetOrPost('priceType', 'Total');
        $this->pickUpDestinationID = GetParameterFromGetOrPost('pickUpDestinationID', 0);
        $this->dropOffDestinationID = GetParameterFromGetOrPost('dropOffDestinationID', 0);
        $this->unitActivityStatus = GetParameterFromGetOrPost('unitActivityStatus', 0);

        $this->xsltPath = iTravelGeneralSettings::$iTravelXSLTTransportationDetailedDescriptionPath;
    }

    public function GetTransportationDetailedDescriptionHTML() {
        $fromParameter = null;
        if ($this->from != null)
            $fromParameter = $this->from->format(DATE_ATOM);
        $toParameter = null;
        if ($this->to != null)
            $toParameter = $this->to->format(DATE_ATOM);
        $childrenAgesParam = array();
        if (!empty($this->childrenAges) || $this->childrenAges == "0") {
            $childrenAgesParam = explode(',', $this->childrenAges);
        }

        $getTransportationDetailedDescriptionParameters = array(
            'StartDate' => $fromParameter,
            'EndDate' => $toParameter,
            'NumberOfPersons' => $this->persons,
            'ObjectID' => $this->objectID,
            'ThumbnailWidth' => 191,
            'ThumbnailHeight' => 142,
            'CurrencyID' => $this->currencyID,
            'LanguageID' => $this->languageID,
            'IgnorePriceAndAvailability' => $this->ignorePriceAndAvailability,
            'InPriceType' => $this->priceType,
            'ListUnitActivityStatus' => "Active",
            'ObjectURL' => $this->objectURL,
            'ObjectCode' => $this->objectCode,
            'ChildrenAgeList' => $childrenAgesParam,
            'CustomerID' => $this->customerID,
            'MarketID' => $this->marketID,
            'AffiliateID' => $this->EncryptedAffiliateID,
            'UserID' => $this->UserID
        );

        $parameters = array(
            'bookingAddressDisplayURL' => $this->bookingForm,
            'unitActivityStatus' => 0,
            'affiliateIDParameter' => $this->EncryptedAffiliateID,
            'postaviDirektanLink' => false,
            'BookingLinkInNewWindow' => false,
            'childrenParam' => count($childrenAgesParam),
            'childrenAgesParam' => $this->childrenAges,
            'showChildrenAgesParam' => false,
            'detailsAddressDisplayURL' => iTravelGeneralSettings::$iTravelPageTransportationDetailedDescriptionPath,
            'objectsIDWithoutUnit' => 0,
            'cartIDParameter' => $this->cartID,
            'marketIDParameter' => $this->marketID,
            'customerIDParameter' => $this->customerID,
            'objectCode' => $this->objectCode,
            'SelectedPickUpDestinationIDGlobal' => 0,
            'SelectedDropOffDestinationIDGlobal' => 0,
            'imagesFolderPath' => iTravelGeneralSettings::$iTravelImagesPath,
            'proxyPath' => iTravelGeneralSettings::$iTravelProxyPath,
            'scriptsFolderPath' => iTravelGeneralSettings::$iTravelScriptsPath,
            'contactFormTemplateURL' => iTravelGeneralSettings::$iTraveContactFormTemplateURL
        );

        $transportationDetailedDescription = InitAPI()->GetTransportationDetailedDescription(array('getTransportationDetailedDescriptionParameters' => $getTransportationDetailedDescriptionParameters));
        return TransformResult($transportationDetailedDescription, $parameters, $this->xsltPath, 'GetTransportationDetailedDescriptionResult', 'TransportationDetails');
    }

    function EchoTransportationDetailedDescription() {
        echo $this->GetTransportationDetailedDescriptionHTML();
    }

}

class GetSearchFields {

    public $languageID;
    public $objectTypeIDList;
    public $objectTypeGroupIDList;
    public $categoryIDList;
    public $countryID;
    public $regionID;

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->currencyID = GetParameterFromGetOrPost('currencyID', iTravelGeneralSettings::$iTravelDefaultCurrencyID, true);

		$objectTypeIDList = GetParameterFromGetOrPost('objectTypeIDList', '');
		if(is_array($objectTypeIDList)){
            $this->objectTypeIDList = $objectTypeIDList;
		}
        else if(isset($objectTypeIDList)) {
            $this->objectTypeIDList = explode(',', $objectTypeIDList);
		}

		$objectTypeGroupIDList = GetParameterFromGetOrPost('objectTypeGroupIDList', '');
		if(is_array($objectTypeGroupIDList)){
            $this->objectTypeGroupIDList = $objectTypeGroupIDList;
		}
        else if(isset($objectTypeGroupIDList)) {
            $this->objectTypeGroupIDList = explode(',', $objectTypeGroupIDList);
		}
        if (isset($_POST['categoryIDList'])) {
            $this->categoryIDList = explode(',', $_POST['categoryIDList']);
        }
        $this->countryID = GetParameterFromGetOrPost('countryID', 0);
        $this->regionID = GetParameterFromGetOrPost('regionID', 0);
    }

	public function GetSearchFields(){
		/// Create the object for calling the API
        $getSearchFieldsParameter = array(
            'LanguageID' => $this->languageID,
            'ObjectTypeIDList' => $this->objectTypeIDList,
            'ObjectTypeGroupIDList' => $this->objectTypeGroupIDList,
            'CategoryIDList' => $this->categoryIDList,
            'CountryID' => $this->countryID,
            'RegionID' => $this->regionID
        );

        /// Call the API
        $getSearchFieldsResult = InitAPI()->GetSearchFields(array('getSearchFieldsParameters' => $getSearchFieldsParameter));

		return $getSearchFieldsResult;
	}

    public function EchoSearchFieldsXML() {
		$searchFields = $this->GetSearchFields();      
        $iTravelAPIResponseXML = '<?xml version="1.0"?>' . XMLSerializer::generateXmlFromArray($searchFields, "root");
        echo $iTravelAPIResponseXML;
    }

	public function GetJSON(){
		$searchFields = $this->GetSearchFields();
		return json_encode((array)$searchFields);
	}
}

class GetAllDestinations {

    public $languageID;
    public $transferDestinations;
    public $SearchQuery;

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID);
        $this->transferDestinations = GetParameterFromGetOrPost('transfers', null);
    }

    public function GetAllDestinations() {
        /// Create the object for calling the API
        $getAllDestinationsParameter = array(
            'LanguageID' => $this->languageID,
            'TransferDestinations' => $this->transferDestinations,
            'SearchQuery' => $this->SearchQuery
        );
        /// Call the API
        $getAllDestinationsResponse = InitAPI()->GetAllDestinations(array('getAllDestinationsParameter' => $getAllDestinationsParameter));
        return $getAllDestinationsResponse;
    }
}

class GetDestinations {

    public $languageID;
    public $categoryID;
    public $countryID;
    public $objectTypeGroupID;
    public $objectTypeID;
    public $regionID;
    public $seasonID;

    public function __construct() {
        $this->languageID = GetParameterFromGetOrPost('languageID', iTravelGeneralSettings::$iTravelDefaultLanguageID, true);
    }

    public function GetDestinations() {
        /// Create the object for calling the API
        $getDestinationsParameters = array(
            'LanguageID' => $this->languageID,
            'CategoryID' => $this->categoryID,
            'CountryID' => $this->countryID,
            'ObjectTypeGroupID' => $this->objectTypeGroupID,
            'ObjectTypeID' => $this->objectTypeID,
            'RegionID' => $this->regionID,
            'SeasonID' => $this->seasonID
        );

        /// Call the API
        $getAllDestinationsResponse = InitAPI()->GetDestinations(array('getDestinationsParameters' => $getDestinationsParameters));
        return $getAllDestinationsResponse;
    }
}

/// object holder for the Booking Form
class BookingForm {
    public $bookingAddress;

    public function __construct() {
        $this->bookingAddress = GetParameterFromGetOrPost('bookingAddress', '');
    }

    public function EchoBookingForm() {
        $finalBookingAddress = $this->bookingAddress;
        if (!empty($_GET['cart'])) {
            $finalBookingAddress = $finalBookingAddress . "&cartID=" . $_SESSION['iTravelCartID'];
        }
        echo "<iframe width=\"900\" height=\"2000\" src=\"" . $finalBookingAddress . "\" frameborder=\"0\" allowtransparency=\"1\" style=\"margin:0; padding:0; border:none;\" ></iframe>";
    }
}

class GetAllSeoData {

    public function __construct() {        
    }

    public function GetAllSeoData() {
        $seoDataParameters = array();
        $getAllSeoDataResults = InitAPI()->GetAllSeoData(array('seoDataParameters' => $seoDataParameters));

        return $getAllSeoDataResults;
    }
}

class Customer {

    #region Properties
    public $CustomerID;
    public $IsCustomer;
    public $IsSupplier;
    public $IsPartner;
    public $LanguageID;
    public $UniqueIndentificationNumber;
    public $CustomerType;
    public $Adress;
    public $City;
    public $ZipCode;
    public $TelephoneNumber1;
    public $TelephoneNumber2;
    public $MobilePhoneNumber;
    public $Fax;
    public $Email;
    public $PersonName;
    public $PersonSurname;
    public $CompanyName;
    public $PassportNumber;
    public $taxPayerType = 1;
    public $BirthDate;
    public $BirthPlace;
    public $CountryID;
    public $CitizenshipID;
    public $Sex;
    public $ContractType;
    public $UserList;
    public $OtherSystemID;
    public $listCustomField;
    /*
     * Customer's default currency.
     */
    public $CurrencyID;
    /*
     * Flag indicating whether the B2B partner is able to search and book in other currencies.
     */
    public $HasLockedCurrencyForB2B;

    #endregion

    public function __construct() {
        
    }

}

class UserCredentials {

    public $Email;
    public $Password;

    public function __construct() {
        
    }

    public function CheckUserCredentials() {
        $checkUserCredentialsRequest = array('Email' => $this->Email,
            'Password' => $this->Password);
        $checkUserCredentialsResponse = InitAPI()->CheckUserCredentials(array('checkUserCredentialsRequest' => $checkUserCredentialsRequest));

        return $checkUserCredentialsResponse;
    }

}

class CustomerInsert {

    public $Customer;	
	
    public function __construct() {
		///this formatting must be in 2 lines because in php 5.3.24 one line creates error
		$currentTime = new DateTime();
		$currentTimeFormated = $currentTime->format(DATE_ATOM);
		
        $this->Customer = array(
            'TaxPayerType' => 1,
            'CustomerID' => 0,
            'IsCustomer' => true,
            'IsSupplier' => false,
            'IsPartner' => false,
            'CustomerType' => 0,
            'ContractType' => 1,
            'CreatedDate' => $currentTimeFormated,
            'BirthDate' => $currentTimeFormated
        );
    }

    public function InsertCustomer() {
        $customerInsertParameters = array('Customer' => $this->Customer);
        return (array('customerInsertParameters' => $customerInsertParameters));
        $customerInsertResponse = InitAPI()->CustomerInsert(array('customerInsertParameters' => $customerInsertParameters));

        return $customerInsertResponse;
    }

}

class GetAllReservations {

    public $LanguageID;
    public $CustomerID;
    public $ReservationCreationDateFrom;
    public $ReservationCreationDateTo;
    public $PageSize;
    public $CurrentPage;
    public $FetchDocuments;
	public $SortParameters;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;

    public function __construct() {
        
    }

    public function GetAllReservations() {
		$getAllReservationsParameters = array('LanguageID' => $this->LanguageID,
			'CustomerID' => $this->CustomerID,
			'ReservationCreationDateFrom' => $this->ReservationCreationDateFrom,
			'ReservationCreationDateTo' => $this->ReservationCreationDateTo,
			'PageSize' => $this->PageSize,
			'CurrentPage' => $this->CurrentPage,
			'FetchDocuments' => $this->FetchDocuments,
			'SortParameters' => $this->SortParameters,
            'UserID' => $this->UserID);

        $getAllReservationsReponse = InitAPI()->GetAllReservations(array('request' => $getAllReservationsParameters));

        return $getAllReservationsReponse;
    }

}

class ChangeReservationStatus {

    public $ReservationID;
    public $StatusID;

    public function __construct() {
        
    }

    public function ChangeReservationStatus() {
        $changeReservationsStatusParameters = array('ReservationID' => $this->ReservationID,
            'StatusID' => $this->StatusID);

        $changeReservationStatusResponse = InitAPI()->ChangeReservationStatus(array('request' => $changeReservationsStatusParameters));

        return $changeReservationStatusResponse;
    }

}

class GetReservation {

    public $LanguageID;
    public $ReservationID;
    /*
     * ID of the "logged in" user. Used on custom B2B portals to correctly generated the booking link.
     */
    public $UserID;

    public function __construct() {
        
    }

    public function GetReservation() {
        $getReservationParameters = array('LanguageID' => $this->LanguageID,
            'ReservationID' => $this->ReservationID,
            'UserID' => $this->UserID);

        $getReservationResponse = InitAPI()->GetReservation(array('getReservationParameters' => $getReservationParameters));

        return $getReservationResponse;
    }

}

class GetAllTransactions {

    public $CustomerID;
    public $PageSize;
    public $CurrentPage;
	public $SortParameters;
	public $LanguageID;
	
    public function __construct() {
        
    }

    public function GetAllTransactions() {
		$getAllTransactionsParameters = array('CustomerID' => $this->CustomerID,
			'LanguageID' => $this->LanguageID,
			'PageSize' => $this->PageSize,
			'CurrentPage' => $this->CurrentPage,
			'SortParameters' => $this->SortParameters	
			);

        $getAllTransactionsReponse = InitAPI()->GetAllTransactions(array('request' => $getAllTransactionsParameters));

        return $getAllTransactionsReponse;
    }

}

class GetReservationCanNotStartDates{
	public $UnitID;
	public $StartDate;
	public $EndDate;
	
	public function __construct(){
	}
	
	public function GetReservationCanNotStartDates(){
		$GetReservationCanNotStartDatesRequest = array(
			'UnitID' => $this->UnitID,
			'StartDate' => $this->StartDate,
			'EndDate' => $this->EndDate
		);
		$GetReservationCanNotStartDatesResponse = InitAPI()->GetReservationCanNotStartDates(array('request' => $GetReservationCanNotStartDatesRequest));
		return $GetReservationCanNotStartDatesResponse;
	}
}

class GetReservationCanNotEndDates{
	public $UnitID;
	public $StartDate;
	public $EndDate;
	
	public function __construct(){
	}
	
	public function GetReservationCanNotEndDates(){
		$GetReservationCanNotEndDatesRequest = array(
			'UnitID' => $this->UnitID,
			'StartDate' => $this->StartDate,
			'EndDate' => $this->EndDate
			);
		
		$GetReservationCanNotEndDatesResponse = InitAPI()->GetReservationCanNotEndDates(array('request' => $GetReservationCanNotEndDatesRequest));
		
        return $GetReservationCanNotEndDatesResponse;
	}
}

class GetPassengersOnReservation{
    public $ReservationUniqueID;
    public $LanguageID;
    public function  __construct(){}
    
    public function GetPassengersOnReservation(){
		$GetPassengersOnReservationRequest = array(
            'ReservationUniqueID' => $this->ReservationUniqueID,
            'LanguageID' => $this->LanguageID
            );
        $GetPassengersOnReservationResponse = InitAPI()->GetPassengersOnReservation(array('request' => $GetPassengersOnReservationRequest));
        
        return $GetPassengersOnReservationResponse;
        
    }
}

class PassengerUpdate{
    public $Passenger;
    
    public function __construct(){}
    
    public function PassengerUpdate(){
        $PassengerUpdateRequest = array(
            'Passenger' => $this->Passenger
            );
        $PassengerUpdateResponse = InitAPI()->PassengerUpdate(array('request' => $PassengerUpdateRequest));
        
        return $PassengerUpdateResponse;
    }
}

class GetPassengerEditFormDTO{
    public $PassengerID;
    public $Name;
    public $Surname;
    public $Email;
    public $DateOfBirth;
    public $MobilePhone;
    public $Birthplace;
    public $PassportNumber;
    public $Address;
    public $Town;
    public $ZipCode;
    //DTO
    public $ListCustomFieldData; // Collection of type CustomField
    
    public function __construct(){}
}

class CustomField{
    public $CustomFieldID;
    public $CustomFieldName;
    public $CustomFieldType;
    //DTOs
    public $CustomFieldValue; // Property of type CustomFiledValue
    public $listCustomFieldValue; // Collection of type CustomFieldValue
    public $AvailableValuesList; // Collection of collections of type CustomFieldValue (Double list)
    
    public function __construct(){}
    
}

class CustomFieldValue{
	public $LanguageID;
    public $Value;
    public $ValueID;
    
    
    public function __construct(){}
}

class CreateReservationRequest{
    public $CurrencyID;
    public $Customer;
    public $PaymentMethod ;
    public $AffiliateID;
    public $MarketID;
    public $LanguageID;
    //DTOs
    public $ReservationCustomFields; // Collection of type CustomFieldReservationProcess
    public $ReservationItemsParametersList; // Collection of type ReservationItemParameters
    public $AdHocReservationItemsParametersList; // Collection of type AdHocReservationItemParameters
	public function  __construct(){
    }
}

class CustomerReservationProcess{
	public $Email;
    public $Name;
    public $MiddleName;
    public $Surname;
    public $CompanyName;
    public $PersonalID;
    public $Gender;
    public $BirthDate;
    public $Birthplace;
    public $CountryID;
    public $CitizenshipCountryID;
    public $Address;
    public $City;
    public $ZIPCode;
    public $PassportNumber;
    public $MobilePhone;
    public $Telephone;
    public $Telefax;
    public $CustomerType;
    public $LanguageID;
    //DTO
    public $CustomFields; // Collection of type CustomFieldReservationProcess
    
    public function  __construct(){
    }
}

class CustomFieldReservationProcess{
    public $ID;
    public $Value;
    
    public function  __construct(){
    }
}

class PaymentMethod{
    public $PaymentMethod;
    public $PaymentMethodName;
    
    public function  __construct(){
    }
}

class ReservationItemParameters{
    public $ReservationItemOrder;
    public $UnitID;
    public $UnitGDSCode;
    public $StartDate;
    public $EndDate;
    public $CuponCode;
    //DTOs
    public $PassengerList; // Collection of type ReservationProcessPassenger
    public $SelectedServices; // Collection of type ReservationProcessService
    
    public function  __construct(){
    }
}

class ReservationProcessPassenger{
    public $ID;
    public $Name;
    public $Surname;
    public $DateOfBirth;
    //DTO
    public $SelectedServices; // Collection of type ReservationProcessService
    
    public function  __construct(){
    }
}

class ReservationProcessService{
    public $ServiceID;
    public $Amount;
    public $AdHocPrice;
    
    public function  __construct(){
    }
}

class AdHocReservationItemParameters{
	public $ReservationItemOrder;
	public $StartDate;
    public $EndDate;
    //DTOs
    public $PassengerList; // Collection of type ReservationProcessPassenger
    public $SelectedServices; // Collection of type ReservationProcessService
    
    public function __construct(){
    }
}

class CreateReservation{
	public $CreateReservationRequest;
    
    public function __construct(){
    }
    
    public function CreateReservation()
    {
        $CreateReservationResponse = InitAPI()->CreateReservation(array('createReservationRequest' => $this->CreateReservationRequest));
    }
}

/// Class used for getting all services.
class GetAllServices {
    public $LanguageID;
    
    public function __construct() { }
    
    public function GetAllServices(){
        $GetAllServicesRequest = array(
            'LanguageID' => $this->LanguageID
            );
        $GetAllServicesResponse = InitAPI()->GetAllServices(array('getAllServicesParameters' => $GetAllServicesRequest));
        
        return $GetAllServicesResponse;
    }
}