<?php

/*
  Author: Lemax d.o.o.
  Version: 1.0
  Author URI: http://www.itravelsoftware.com
 */
/// static class for general settings
class iTravelGeneralSettings
{    
	public static $iTravelAPIURL = "http://demotest.itravelsoftware.com/itravel/api/webservice/itravelapi_3_0.asmx?WSDL";
	public static $iTravelAPIUsername = "DemoAPIUser";
	public static $iTravelAPIPassword = "DemoAPIPassword";

//	 public static $iTravelAPIURL = "http://moodhu.itravelsoftware.com/itravel/API/WebService/iTravelAPI_3_0.asmx?WSDL";
//	 public static $iTravelAPIUsername = "APIUsermoodhu";
//	 public static $iTravelAPIPassword = "272Kr50RId";
	  
	public static $ActiveLanguagesList = array(
		0 => "en",
		1 => "hr"
	);

    //define folder paths
	public static $iTravelResourcesFolderPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/Resources";
    //define default parameters
    public static $iTravelDefaultLanguageID = "en";
    public static $iTravelDefaultCurrencyID = 978;
	#region paths to pages
	public static $iTravelPageBookingFormPath = array(
		"en" => "/booking-form/",
		"hr" => "/booking-form/"
	);
	#region accommodation paths
    public static $iTravelPageAccommodationSearchResultsPath = array(
		"en" => "/accommodation-results/",
		"hr" => "/accommodation-results/"
	);
    public static $iTravelPageAccommodationDetailedDescriptionPath = array(
		"en" => "/accommodation-details/",
		"hr" => "/accommodation-details/"
	);
	#endregion

	#region activity paths
	public static $iTravelPageActivitySearchResultsPath = array(
	   "en" => "/activity-results/",
	   "hr" => "/activity-results/"
   );
    public static $iTravelPageActivityDetailedDescriptionPath = array();
	#endregion

	#region tours paths
    public static $iTravelPageTourSearchResultsPath = array(
		"en" => "/tour-results/",
		"hr" => "/tour-results/"
	);
    public static $iTravelPageTourDetailedDescriptionPath = array(
		"en" => "/tour-details/",
		"hr" => "/tour-details/"
	);
	#endregion

	#region transfer paths
	public static $iTravelPageTransferSearchResultsPath = array(
	   "en" => "/transfer-results/",
	   "hr" => "/transfer-results/"
   );
    public static $iTravelPageTransferDetailedDescriptionPath = array();
	#endregion

	#region rent a car paths
	public static $iTravelPageRentACarSearchResultsPath = array(
	   "en" => "/rent-a-car-results/",
	   "hr" => "/rent-a-car-results/"
   );
    public static $iTravelPageRentACarDetailedDescriptionPath = array();
	#endregion

    #region transportation paths
    public static $iTravelPageTransportationSearchFormPath = "/SamplePages/TransportationSearch.php";
    public static $iTravelPageTransportationSearchResultsPath = "/SamplePages/TransportationSearchResults.php";
    public static $iTravelPageTransportationDetailedDescriptionPath = "/SamplePages/TransportationDetails.php";
	#endregion
	#endregion

    //define paths to default xslt stylesheets
    //accomodation
    public static $iTravelXSLTAccommodationSearchFormPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/AccommodationSearchControl.xslt";
    public static $iTravelXSLTAccommodationSearchResultsPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/AccommodationSearchResults/AccommodationSearchResults.xslt";
    public static $iTravelXSLTAccommodationDetailedDescriptionPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/AccommodationDetails.xslt";
    public static $iTravelXSLTAccommodationDetailsReservationsTabPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/AccommodationDetailsReservationsTab.xslt";
    //tours
    public static $iTravelXSLTTourSearchFormPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/PackageTourSearchControl.xslt";
    public static $iTravelXSLTTourSearchResultsPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/PackageTourSearchResults.xslt";
    public static $iTravelXSLTTourDetailedDescriptionPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/PackageTourDetails.xslt";
    public static $iTravelXSLTTourDetailsAccommodationUnitListPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/PackageTourDetailsAccommodationUnitList.xslt";
    //transportation
    public static $iTravelXSLTTransportationSearchFormPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/TransportationSearchControl.xslt";
    public static $iTravelXSLTTransportationSearchResultsPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/TransportationSearchResults.xslt";
    public static $iTravelXSLTTransportationDetailedDescriptionPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/TransportationDetails.xslt";
    public static $iTravelXSLTTransportationDetailsReservationsTabPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/XSLStyleSheets/TransportationDetailsReservationsTab.xslt";

    //static files paths
    public static $iTravelScriptsPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/Script";
    public static $iTravelImagesPath = "/wp-content/plugins/iTravelPlugin/XSLTControls/Css/Images";
    public static $iTravelProxyPath = "/wp-content/plugins/iTravelPlugin/PHPAPIIntegration/ProxyWebService.php?functionName=";
    public static $iTraveContactFormTemplateURL = "/wp-content/plugins/iTravelPlugin/XSLTControls";

	/*
	 * Returns path on requested language
	 */
	static function PagePath($pathArray, $languageID)
	{
		//if path is not array, there is no multi language version
		if(!is_array($pathArray))
			return $pathArray;

		//if it is array, try to find the translation on requested language, if it not exists, return first value
		if(!empty($pathArray[$languageID]))
			return $pathArray[$languageID];

		return reset($pathArray);
	}
}