<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/all.css" rel="stylesheet">

    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    
    <style type="text/css">
        .h1000
        {
            height: calc(100vh - 60px);
        }
        #frmEnquire
        {
            height: auto;
            min-height: 600px;
        }
        img.activator{
            width: 100%;
        }
        .navbar-fixd {
            box-shadow: 0 0 140px #37a1d8;
        }
        .panel-body i {
            font-size: 15px;
            margin: -2px;
            vertical-align: middle;
        }

        .unitSlider
        {
            margin-top: 0 !important;
        }

        i
        {
            vertical-align: middle;
        }
        td, th {
            padding: 4px !important;
        }

        .modal-header {
            padding: 10px 10px 0;
            border-bottom: 1px solid #ccc;
        }
        a[data-slide-index] img {
            width: 30px;
            height: 30px;
            border: 0;
            margin: 0;
            padding: 1px;
        }
    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        <!-- CSS  -->
    </head>
    <body ng-app='app'>
        <main>
            @include('partials.header')
            @yield('content')
        
        </main>
        @include('partials.footer')




                <!-- Scripts -->
        <script src="/js/all.js"></script>
        <script type="text/javascript">
            (function($){
                $(function(){
                    $('.button-collapse').sideNav();
                    $('.parallax').parallax();
                    $('.slider').slider({full_width: true});

                }); // end of document ready
                $(function() {
                    $('a[href*="#"]:not([href="#"])').click(function() {
                    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                        if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                        }
                    }
                    });
                });
            })(jQuery); // end of jQuery name space
        </script>

        @yield('scripts')

    </body>
    </html>

