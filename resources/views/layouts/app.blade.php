<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Moodhu Holidays</title>

	<!-- Styles -->
	<link href="/css/all.css" rel="stylesheet">
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
	<style type="text/css">
		.h1000
		{
			height: calc(100vh - 60px);
		}

		@media only screen and (max-width: 601px)
		{
			.carousel .carousel-item img
			{
				height: 100%;
				width: auto;
			}
		}
		.carousel .carousel-item img
		{
			height: 100%;
			min-width: 100%;
		}
		img.activator{
			width: 100%;
		}
	</style>
	<!-- Scripts -->
	<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
			]); ?>
		</script>
		<!-- CSS  -->
		
	</head>
	<body ng-app='app'>
		<main>
			@include('partials.header')
			@yield('content')
		</main>

		@include('partials.footer')


		<!-- Scripts -->
		<script src="/js/all.js"></script>
		<script type="text/javascript">


			app.controller('bookingController', function($scope, $http, $location, $filter)
			{
				$scope.destinations = [];
                $scope.loading = false;
                $scope.filter_loading = true;

				$scope.minDate = true;

				$scope.getMinDate = function()
				{
					console.log('Date changed', $filter('date')($scope.booking.from_date, 'yyyy-MM-dd'));
					return $filter('date')($scope.booking.from_date, 'yyyy-MM-dd');
				}

				$scope.change = function(selected)
				{
					$scope.toDate = selected.obj;
				}

				$scope.filters = {};

				$scope.booking = {
                    destinations: [],
                    currentPage: 1,
                    children_age_groups: []
                };


                $scope.$watch('booking.number_of_children', function()
                {
					$scope.booking.children_age_groups = [];
					for (var i = $scope.booking.number_of_children - 1; i >= 0; i--) {
						$scope.booking.children_age_groups.push({age: null});
					}
                });

                $scope.toggle = function (id) {
                    if ($scope.booking.destinations.indexOf(id) === -1) {
                        $scope.booking.destinations.push(id);
                    } else {
                        $scope.booking.destinations.splice($scope.booking.destinations.indexOf(id), 1);
                    }
                };

    //             $scope.load = function(page)
    //             {
    //                 $scope.booking.current_page = page;
    //                 $scope.search();
    //             }

				// $http.get('/api/filters').success(function($filters)
				// {
    //                 $scope.filter_loading = false;
    //                 $scope.filters = $filters;
				// });

				

				$scope.submit = function()
				{
					$('form').submit();
					$scope.loading = true;

					// $http.post('/api/customize', $scope.booking).success(function(data)
					// {
					// 	$scope.destinations = data;
					// 	$scope.loading = false;

					// }).error(function()
					// {
					// 	$scope.loading = false;
					// });
					$location.path(jsonToQueryString($scope.booking));


//
				}

			});

			app.controller('specialOffer', function($scope, $http){
				$scope.special_offers = [];
				$scope.loading = true;

				$http.get('/api/special-offers?pageSize=9').success(function($data)
				{	
					$scope.special_offers = $data;
				});
			}).controller('tours', function($scope, $http){
				$scope.tours = [];
				$scope.loading = true;

				$http.get('/api/tours?pageSize=9').success(function($data)
				{	
					$scope.tours = $data;
				});
			}).controller('accommodations', function($scope, $http, $location){
				$scope.accommodations = [];
				$scope.loading = true;
				$scope.page = $location.search().current_page || 1;
				$scope.page_size = $location.search().page_size || 18;
				$scope.loading = false;


				$scope.loadPage = function($page)
				{
					$scope.loading = true;
					$http.get('/api/accommodations?page-size='+$scope.page_size+'&current-page='+ $page + '&raw=false').success(function($data)
					{
						$scope.loading = false;

						$scope.accommodations = $data;
						$scope.total = $data.total;
						$scope.lastPage = $data.lastPage;

						$location.search('current_page', $page);

					}).error(function()
					{
						$scope.loading = false;

					});

				};

				$scope.loadPage($scope.page);
			});


			(function($){
				$(function(){
					$('.button-collapse').sideNav();
					$('.parallax').parallax();
					$('.carousel.carousel-slider').carousel({full_width: true});

					$('.carousel').carousel({
					    padding: 200    
					});
					autoplay()   
					function autoplay() {
					    $('.carousel').carousel('next');
					    setTimeout(autoplay, 4500);
					}

				}); // end of document ready
				$(function() {
					$('a[href*="#"]:not([href="#"])').click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						if (target.length) {
						$('html, body').animate({
							scrollTop: target.offset().top
						}, 1000);
						return false;
						}
					}
					});
				});
			})(jQuery);
		</script>

		@yield('scripts')

	</body>
	</html>

