@extends('layouts.detail')

@section('content')
    <style>
        .error {
            color: red;
            border-left: 4px solid red;
            padding: 10px;
        }
    </style>
    <div class="container below_icon_container">
        <h3>Like and win!</h3>


        <p>
            @if (Session::has('exists'))
                <h4 class="error">{!! session('exists') !!}</h4>
            @endif
        </p>
        <form id="best-deals-form" action="/like_and_win" method="post" ng-submit='submit()'>
            <!-- <h5 class=" lighten-2">Find the best accommodation deals</h5> -->
            <div class="">
                <!-- <div class="row  col s12 m6 l3"> -->
                <div class="input-field col s12 m6 l2">
                    <i class="material-icons prefix">today</i>
                    <input required type="text" name='name' class="icon-prefix validate" value="{{old('name')}}">
                    <label class="" for="from_date">Name</label>
                </div>
                <div class="input-field col s12 m6 l2">
                    <i class="material-icons prefix">today</i>
                    <input required id="to_date" pick-date type="email" name='email' class="icon-prefix "  value="{{old('email')}}">
                    <label class="" for="to_date">Email</label>
                </div>
                <div class="input-field col s12 m6 l2">
                    <i class="material-icons prefix">today</i>
                    <input required id="to_date" pick-date type="text" name='mobile_number' class="icon-prefix "  value="{{old('mobile_number')}}">
                    <label class="" for="to_date">Mobile</label>
                </div>
                <div class="input-field col s12 m6 l2">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha" data-sitekey="6Ldb-xsUAAAAAEgeFi64qqW3VPD7v3rs75i6IpXI"></div>

                    @if (Session::has('error'))
                        <span class="error">{!! session('error') !!}</span>
                    @endif
                </div>
                <div class="input-field col s12 m6 l2">
                    <input type="submit" class="btn">

                </div>

                {{ csrf_field()  }}

            </div>
        </form>

    </div>
    <br>
@stop