@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Latest Pages</div>

                <div class="panel-body">
                    <ul class="list-group">
                    @foreach(\App\Page::latest()->take(5)->get() as $page)
                      <li class="list-group-item">
                        <span class="badge">{{$page->id}}</span>
                            {{$page->title}}
                      </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
