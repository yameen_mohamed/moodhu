@extends('layouts.admin')

@section('content')
<div class="container">
	<h1><img width="200" src="/images/exclamation.png"> Your account needs activation.</h1>
	<p>
	<form method="post">		
		<button class="btn">
			<i class="glyphicon glyphicon-play"></i>	Remind the admin
		</button>
		</a>
	</form>
</div>
@endsection
