@extends('layouts.detail')

@section('content')
<div class="container below_icon_container">
	<h3>Find the best accommodation deals</h3>
	<div class="row">
		<div class="col l3">
			<div class="row">
			      	<form class="col s12" action="/customized">
						@foreach($filters as $key=>$value)
							@if(is_array($value))
								@foreach($value as $k=>$v)
									<input type="hidden" name="{{$key}}[{{$k}}]" value="{{$v}}">
								@endforeach
							@else
								<input type="hidden" name="{{$key}}" value="{{$value}}">
							@endif
						@endforeach
	      				<div class="row">
					        <div class="card-panel orange lighten-4">
					        	<h5 class="orange-text">Search</h5>
					        	<div>
						        	<div class="input-field">
										<input name="name" id="place" type="text" class="validate" value="{{isset($name) ? $name : ''}}">
										<label for="place">Place</label>
									</div>
									<div class="input-field">
										<input name="to_date" id="destination_name" type="text" class="validate" value="{{isset($to_date) ? $to_date : ''}}">
										<label for="destination_name">Check-in</label>
									</div>
									<div class="input-field">
										<input name="from_date" id="destination_name" type="text" class="validate" value="{{isset($from_date) ? $from_date : ''}}">
										<label for="destination_name">Check-out</label>
									</div>
									<div class="input-field">
										<input name="number_of_adults" id="destination_name" type="text" class="validate" value="{{isset($number_of_adults) ? $number_of_adults : ''}}">
										<label for="destination_name">No of adults</label>
									</div>

									<div class="input-field">
										<input name="number_of_children" id="destination_name" type="text" class="validate" value="{{isset($number_of_children) ? $number_of_children : ''}}">
										<label for="destination_name">No of children</label>
									</div>

									<div class="divider"></div>

									<div class="input-field">
										<button class="btn">Submit</button>
									</div>
					        	</div>
					        </div>
			      		</div>

			      		<div class="row">
					        <div class="card-panel blue lighten-4">
					        	<h5 class="orange-text">Filter</h5>
					        	<h6>Regions</h6>
					        	<div>
									<p>
										<input class="filled-in" type="checkbox" id="test5" />
										<label class="blue-text text-darken-2" for="test5">Central Province</label>
									</p>
									<p>
										<input class="filled-in" type="checkbox" id="test6" />
										<label class="blue-text text-darken-2" for="test6">North Province</label>
									</p>

									<div class="divider"></div>

									<div class="input-field">
										<button class="btn">Filter</button>
									</div>
					        	</div>
					        </div>
			      		</div>
			      	</form>
			</div>
		</div>
		<div class="col l9">

			<div class="row listing">

				<div class="col">
				@foreach($data['data'] as $plan)
					<div class="card horizontal hoverable">
						<div class="card-image">
						@if($plan->hasPhotos())
							<img height="100%" src="{{$plan->getRandomPhoto()->getImageUrl()}}">
						@else
							<img src="/images/image-not-available.png">
						@endif
						</div>
						<div class="card-stacked">
							<div class="pad-10 card-content">
								<h5 class="no-margin">{{$plan->getName()}} </h5>
								<div class="divider"></div>
								<h6 class="region">
									<i style="font-size: 10px" class="material-icons">location_on</i>
									{{$plan->getDestination()->getName()}} / {{$plan->getDestination()->getRegion()->getName()}}
								</h6>
								@if($plan->getRating())
								<h4 class="no-margin">
									@for($i=0;$i<$plan->getRating();$i++)
										<i class="orange-text material-icons">star</i>
									@endfor
								</h4>
								@endif
								@if(count($plan->getSpecialOffers()))
								<span class="red-text" style="text-transform: uppercase;">Available Special Offers:</span>
								@foreach($plan->getSpecialOffers() as $index => $special_offer)
									@if($index < 3)
									<p> <i style="vertical-align: middle" class="material-icons">stars</i> {{$special_offer->getName()}}</p>
									@endif
								@endforeach
									@if($index > 2)
										<small class="red-text"><i style="vertical-align: middle;font-size: 15px" class="material-icons">thumb_up</i> More offers</small>
									@endif
								@endif


								


								{{--<div class="chip">--}}
							    	{{--<img src="/images/logo_x.png" alt="Contact Person">--}}
								    {{--Beach--}}
								 {{--</div>--}}
							</div>
							<div class="card-action">
							@if($plan->getMealsString())
								<div class="red-text" style="font-size:10px;text-transform: uppercase;margin-left: -20px">Meals:
									<div class="black-text" style="font-size: 10px;display: inline;">
									 
										{{ $plan->getMealsString() }}
									</div>
								</div>
								
							@endif

							@if($plan->getDistanceFromString())
								<div class="red-text" style="font-size:10px;text-transform: uppercase;margin-left: -20px">Distances:
									<div class="black-text" style="font-size: 10px;display: inline;">
									 
										{{ $plan->getDistanceFromString() }}
									</div>
								</div>
								
							@endif

							
								<a target="_blank" class="right btn-floating" href="/accommodation/{{$plan->getId().'?'.$_SERVER['QUERY_STRING'] }}">
								<i class="material-icons">explore</i>
								</a>
							</div>
						</div>
					</div>
				@endforeach

				@if(isset($data['last_page']) && $data['last_page'] > 0)
				<div class="row center">
					<ul class="pagination">
						<form action="/customized">
							@foreach($filters as $name=>$value)
								@if(is_array($value))
									@foreach($value as $k=>$v)
										<input type="hidden" name="{{$name}}[{{$k}}]" value="{{$v}}">
									@endforeach
								@else
								<input type="hidden" name="{{$name}}" value="{{$value}}">
								@endif
							@endforeach
							<li class="waves-effect">
								<button @if(array_key_exists('current_page', $filters) && $filters['current_page'] <= 1) disabled @endif
										name="current_page"
										@if(array_key_exists('current_page', $filters))
										value="{{$filters['current_page']-1}}"
										@endif
										>
									<i class="material-icons">chevron_left</i>
								</button>
							</li>
								@for($i=1;$i<=$data['last_page'];$i++)
									<li class="@if(array_key_exists('current_page', $filters) && $filters['current_page'] == $i)  active @endif">
										{{--<a name="current_page" value="{{$i}}" href="#">--}}

										<button name="current_page" value="{{$i}}" type="submit">{{$i}}</button>
										{{--</a>--}}
									</li>
								@endfor
								<li class="waves-effect">
									<button @if(array_key_exists('current_page', $filters)&&$filters['current_page'] >= $data['last_page']) disabled
											@endif
											name="current_page"
											@if(array_key_exists('current_page', $filters))
											value="{{$filters['current_page']+1}}"
											@endif
											>
										<i class="material-icons">chevron_right</i></button>
								</li>
						</form>
					</ul>
				</div>
					@endif

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
	@if(session('message'))
	<script>
		$(function()
		{
			Materialize.toast("{{session('message')}}", 4000);
		});

	</script>
	@endif
@stop