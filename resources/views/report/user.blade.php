@extends('layouts.app')
@section('content')
	<br>
	<div class="">
		<form class="form-horizontal">
			<div class="form-inline">

				<div class="input-group">
					<div class="input-group-addon">From</div>
					<input class="form-control datepicker" name="from" placeholder="From" 
						value="{{isset($from) ? $from : ''}}"></input>
				</div>
				<div class="input-group">
					<div class="input-group-addon">To</div>
					<input class="form-control datepicker" name="to" placeholder="To" 
						value="{{isset($to) ? $to : ''}}"></input>
				</div>
				<div class="input-group">
					<select class="selectpicker form-control" name="user" >
						<option value=""> -- User --</option>
						@foreach(App\User::all() as $userObj)
						<option @if(isset($user) && $userObj->id == $user) selected=selected @endif value="{{$userObj->id}}">{{$userObj->name}}</option>
						@endforeach
					</select>
				</div>
				<button class="btn btn-info pull-right"><i class="glyphicon glyphicon-search"></i></button>
			</div>	
		</form>
		<hr>
	</div>
	<table class="table">  	<thead><tr>
	 		<th>Name</th>
	 		<th>Email</th>
	 		<th title="Number of correction requests">No of CR</th>
	 	</tr>
	 	
		</thead>
	 	<tbody>
	 	@foreach($models as $model)
	 	<tr>
	 		<td>{{$model->name}}</td>
	 		<td>{{$model->email}}</td>
	 		<td>{{$model->corrections->count()}}</td>
	 	</tr>
	 	@endforeach	</tbody>
	</table>
@stop
