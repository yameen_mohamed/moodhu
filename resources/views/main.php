
	<div class="slider" style="height: 810px;">
		<div class="carousel carousel-slider" data-indicators="true">
		    <a class="carousel-item" href="#"><img src="/images/original-1.jpg"></a>
		    <a class="carousel-item" href="#"><img src="/images/original-2.jpg"></a>
		    <a class="carousel-item" href="#"><img src="/images/original-3.jpg"></a>
		    <a class="carousel-item" href="#"><img src="/images/original-4.jpg"></a>
		  </div>
		<div class="container" id='search' ng-controller='bookingController' style="position: absolute;top: 460px;left:0; right:0;">
			<div class="section">

				<div class="">
					<div class="row yellow lighten-4 card-panel search-toolbar">
						<!-- <div class="progress" ng-if="filter_loading">
                            <div class="indeterminate"></div>
                        </div> -->
						<form id="best-deals-form" action="/customized" method="get" ng-submit='submit()' target="_blank">
							<!-- <h5 class=" lighten-2">Find the best accommodation deals</h5> -->
							<div class="">
								<!-- <div class="row  col s12 m6 l3"> -->
									<div class="input-field col s12 m6 l2">
										<i class="material-icons prefix">today</i>
										<input required on-change="change(date)" min-date="{{minDate}}" format="yyyy-mm-dd" id="from_date" pick-date type="text" name='from_date' ng-model='booking.from_date' class="icon-prefix validate">
										<label class="" for="from_date">Check-in</label>
									</div>
									<div class="input-field col s12 m6 l2">
										<i class="material-icons prefix">today</i>
										<input ng-disabled="!booking.from_date" required min-date="{{getMinDate()}}" id="to_date" pick-date type="text" name='to_date' ng-model='booking.to_date' class="icon-prefix  validate">
										<label class="" for="to_date">Check-out</label>
									</div>
								<div class="input-field col s12 m6 l2">
									<i class="material-icons prefix">content_copy</i>
									<input required id="number_of_adults" type="text" name='number_of_rooms' ng-model='booking.number_of_rooms' class="icon-prefix  validate">
									<label class="" for="number_of_rooms">No of Rooms</label>
								</div>
								<!-- </div> -->
								<div class="input-field col s12 m6 l2">
									<i class="material-icons prefix">perm_identity</i>
									<input required id="number_of_adults" type="text" name='number_of_adults' ng-model='booking.number_of_adults' class="icon-prefix  validate">
									<label class="" for="number_of_adults">No of Adults</label>
								</div>
								<div class="col s12 m6 l2">

									<div class="input-field">
										<!-- <i class="material-icons ">user_group</i> -->
										<i class="material-icons prefix">child_care</i>
										<input id="number_of_ad" type="text" name='number_of_children' ng-model='booking.number_of_children' class=" validate">
										<label class="" for="number_of_ad">No of Children</label>
									</div>

									<div class="row">

										<div ng-repeat='i in booking.children_age_groups'  class="input-field col">
											<input required id="number_of_ad" type="text" name='children_age_groups[{{$index}}]' ng-model='i.age' class=" validate">
											<label class="" for="number_of_ad">Age of child #{{$index+1}}</label>
										</div>
									</div>
								</div>
							<!-- </div> -->
							<!-- <div class="row"> -->
								<div class="input-field col s6 m1 l1">
									<button type="submit" class="btn-floating right waves-effect waves-light btn">
										<i class="material-icons right">search</i>
									</button>
								</div>
							</div>
							<!-- <div class="row">

								<div class="col s6 m3 l4">
									<h3>Category</h3>
									<p><input type="text" ng-model="filterCat" placeholder="Search Categories"></p>
									<p ng-repeat="cat in filters.categories | filter:filterCat">
										<input name="group1" value="{{cat.id}}" type="radio" id="cat_{{cat.id}}" ng-model="booking.category_id"/>
										<label for="cat_{{cat.id}}">{{ cat.name }}</label>
									</p>
								</div>
								<div class="col s6 m3 l4">
									<h3>Destinations</h3>
									<p><input type="text" ng-model="filterDest" placeholder="Search Destinations"></p>
									<p ng-repeat="dest in filters.destinations | filter: filterDest">
										<input type="checkbox" id="dest_{{dest.id}}" ng-click="toggle(dest.id)"/>
										<label for="dest_{{dest.id}}">{{ dest.name }}</label>
									</p>
								</div>
								<div class="col s6 m3 l4">
									<h3>Regions</h3>
									<p><input type="text" ng-model="filterRegion" placeholder="Search Regions"></p>

									<p ng-repeat="region in filters.regions | filter:filterRegion">
										<input name="group2" value="{{region.id}}" type="radio" id="region_{{region.id}}" ng-model="booking.region_id"/>
										<label for="region_{{region.id}}">{{ region.name }}</label>
									</p>
								</div>
							</div> -->
						</form>
					</div>
				</div>
			</div>

		</div>
		<div style="clear: both"></div>

	</div>



<!--	<div class="container">-->
<!--		<div class="section">-->
<!---->
<!--			<div class="row">-->
<!--				<div class="col s12 m4">-->
<!--					<div class="icon-block">-->
<!--						<img src="/images/special-offers.jpg" alt="Image is not available!">-->
<!--						<h2 class="center brown-text"><i class="material-icons">card_travel</i></h2>-->
<!--						<h5 class="center">Special Offers</h5>-->
<!---->
<!---->
<!--					</div>-->
<!--					<div class="center">-->
<!--						<a class="btn" href="#special-offers">Explore</a>-->
<!--					</div>-->
<!--				</div>-->
<!---->
<!--				<div class="col s12 m4">-->
<!--					<div class="icon-block">-->
<!--						<img src="/images/tours.jpg" alt="Image is not available!">-->
<!--						<h2 class="center brown-text"><i class="material-icons">group</i></h2>-->
<!--						<h5 class="center">Tours</h5>-->
<!---->
<!--						<div class="center">-->
<!--							<a href="#tours" class="btn light-blue">Explore</a>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!---->
<!--				<div class="col s12 m4">-->
<!--					<div class="icon-block">-->
<!--						<img src="/images/customized-holidays.jpg" alt="Image is not available!">-->
<!--						<h2 class="center brown-text"><i class="material-icons">settings</i></h2>-->
<!--						<h5 class="center">Customize Holidays</h5>-->
<!---->
<!--						<div class="center">-->
<!--							<a href="#search" class="btn">Explore</a>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			-->
<!---->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="teal lighten-4" id="special-offers">-->
<!--		<div class="container" ng-controller='specialOffer'>-->
<!--			<div class="section">-->
<!--				<h2 class="center white-text">Special Offers</h2>-->
<!--				<div class="row">-->
<!--					<div class="col s12 m4 l4" ng-repeat='special_offer in special_offers'>-->
<!--						<div class="card sticky-action hoverable">-->
<!--							<div class="card-image waves-effect waves-block waves-light">-->
<!--								<img ng-if="special_offer.accommodation.image" class="activator" src="{{special_offer.accommodation.image.thumbnailUrl}}">-->
<!--								<img ng-if="!special_offer.accommodation.image" src="/images/image-not-available.png" alt="Image is not available!">-->
<!--							</div>-->
<!--							<div class="card-content">-->
<!--								<span class="card-title activator grey-text text-darken-4 truncate">{{special_offer.accommodation.name}}-->
<!--								</span>-->
<!--							</div>-->
<!--							<div class="card-reveal">-->
<!--								<span class="card-title grey-text text-darken-4">{{special_offer.AccommodationObject.name}}<i class="material-icons right">close</i></span>-->
<!--								<p>Here is some more information about this product that is only revealed once clicked on.</p>-->
<!--								<ul>-->
<!--									<li ng-repeat='(prop, info) in special_offer.AccommodationObject.hotel'>-->
<!--										<strong>-->
<!--											{{prop}}-->
<!--										</strong>-->
<!--										<br>-->
<!--										<span>{{info}}</span>-->
<!--									</li>-->
<!--								</ul>-->
<!--							</div>-->
<!---->
<!--							<div class="card-action">-->
<!--								<a target="_blank" href="/tour/{{special_offer.accommodation.id}}" class="btn"><i class="material-icons">more</i></a>-->
<!--								<span class="right">{{special_offer.AccommodationObject.double_room.pricing.Currency.CurrencyShortName}} {{special_offer.AccommodationObject.double_room.pricing.CalculatedPrice}}</span>-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->

<!--	<div class="teal lighten-4" id="tours">-->
<!--		<div class="container" ng-controller='tours'>-->
<!--			<div class="section">-->
<!--				<h2 class="center white-text">Tours</h2>-->
<!--				<div class="row">-->
<!--					<div class="col s12 m4 l4" ng-repeat='tour in tours'>-->
<!--						<div class="card sticky-action hoverable">-->
<!--							<div class="card-image waves-effect waves-block waves-light">-->
<!--								<img ng-if="tour.image" class="activator" src="{{tour.image.thumbnailUrl}}">-->
<!--								<img ng-if="!tour.image" src="/images/image-not-available.png" alt="">-->
<!--							</div>-->
<!--							<div class="card-content">-->
<!--								<span class="card-title activator grey-text text-darken-4 truncate">{{tour.name}}-->
<!--								</span>-->
<!--							</div>-->
<!--							<div class="card-reveal">-->
<!--								<span class="card-title grey-text text-darken-4">{{tour.name}}<i class="material-icons right">close</i></span>-->
<!--								<p>Here is some more information about this product that is only revealed once clicked on.</p>-->
<!--								<ul>-->
<!--									<li ng-repeat='(prop, info) in tour.hotel'>-->
<!--										<strong>-->
<!--											{{prop}}-->
<!--										</strong>-->
<!--										<br>-->
<!--										<span>{{info}}</span>-->
<!--									</li>-->
<!--								</ul>-->
<!--							</div>-->
<!---->
<!--							<div class="card-action">-->
<!--								<a target="_blank" href="/tour/{{tour.id}}" class="btn"><i class="material-icons">more</i></a>-->
<!--								<span class="right">{{tour.AccommodationObject.double_room.pricing.Currency.CurrencyShortName}} {{tour.AccommodationObject.double_room.pricing.CalculatedPrice}}</span>-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
	<div class="" id='accommodations'>
		<div class="container" ng-controller='accommodations' ng-cloak>
			<div class="section">
				<h2 class="center ">Accommodations</h2>
				<div class="row">
					<div class="col s12 m6 l4" ng-repeat='accommodation in accommodations.data'>
						<div class="card sticky-action hoverable">
							<div class="card-image waves-effect waves-block waves-light">
								<span ng-if='accommodation.has_special_offers' class='special_offer'>
									Special Offer
								</span>

								<img class="activator" src="{{accommodation.image.thumbnailUrl}}">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4 truncate">
								{{accommodation.name}}
									<p ng-if="accommodation.rating">
										<i ng-repeat='i in accommodation.rating | range' class="orange-text material-icons">star</i>
									</p>

								</span>
								<small>
									<i style="vertical-align: middle" class="material-icons">location_on</i> Male' / Central
								</small>

							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">{{accommodation.name}}<i class="material-icons right">close</i></span>
								<p>{{ accommodation.description }}</p>
								<ul>
									<li ng-repeat='(prop, info) in accommodation.hotel'>
										<strong>
											{{prop}}
										</strong>
										<br>
										<span>{{info}}</span>
									</li>
								</ul>
							</div>

							<div class="card-action">
								<a target="_blank" href="/accommodation/{{accommodation.id}}" class="btn"><i class="material-icons">explore</i></a>
								<span class="right">{{accommodation.double_room.pricing.Currency.CurrencyShortName}} {{accommodation.double_room.pricing.CalculatedPrice}}</span>
							</div>
						</div>
					</div>
				</div>
				<div class=" center">
					<div ng-if="loading" class="preloader-wrapper small active">
						<div class="spinner-layer spinner-green-only">
							<div class="circle-clipper left">
								<div class="circle"></div>
							</div><div class="gap-patch">
								<div class="circle"></div>
							</div><div class="circle-clipper right">
								<div class="circle"></div>
							</div>
						</div>
					</div>
					<pager ng-if="!loading"
						page-size="accommodations.page_size"
						last-page="accommodations.last_page"
						current-page="accommodations.current_page"
						total="accommodations.total"
						page-change="loadPage(page)">
					</pager>



				</div>	

			</div>
		</div>
	</div>
	
	
	<div class="teal lighten-4">
		<div class="container" id='about_us'>
			<div class="section">
				<h3 class="white-text text-darken-4 center">About Us</h3>				
				<div class="row">
					<div class="col s12 l6 m12 center">
						<p class="left-align light">
							MOODHU Holidays Maldives is a travel company that delivers Travel and Tour services to business and leisure customers. "MOODHU" comes from our very own local language Dhivehi meaning "sea". MOODHU is not simply a name, but rather it is a one-word description of the Maldives. It is our first effort in creating a local experience for our customers. We take Maldives in its native form and in the local language to our customers.
						</p>
						<p class="left-align light">
							Considering the pace of change of the tourism industry luring many companies into the sector, we strongly differentiate ourselves from other businesses.
						</p>
						<p class="left-align light">
							Tourism is undergoing a rapid growth of unsurpassed nature. Generally there is a trend towards shorter stays, as individuals strive to experience as many cultures as possible. We market Maldives as one of the few places in the World where nature have remained unspoiled, a destination of nature - an oasis in the Indian Ocean which is an ideal escape for “lovers”.
						</p>
						<p class="left-align light">
							MOODHU guides customers in selecting trip based on pre-defined vacation criteria. These preferences are based on customer’s needs whether it is honeymoon, family holiday, fun & activity or budget.
						</p>
						<div class="left-align">
							<p class="left-align">
								Some notable services we provide are:
							</p>
							<ol class="left-align">
								<li> Reservations and Bookings for Resorts/Hotels/Guesthouse</li>
								<li> Safari Cruises</li>
								<li> Diving Tours</li>
								<li> Tours, Excursions and Activities</li>
								<li> Transportation Services</li>
								<li> M I C E events</li>
							</ol>
						</div>
					</div>
					<div class="col s12 l6 m12 center">
						<ul class="thumbnails">
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/1.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/2.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/3.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/4.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/5.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/6.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/7.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/8.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/9.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/10.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/11.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/12.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/13.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/14.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/15.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/16.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/17.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/18.jpg"></li>
								<li class="col m3 s6 l3"><img class="hoverable materialboxed" width="100" src="/about_images/19.jpg"></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="" id='about_maldives'>
		<div class="container">
			<div class="section">
				<h3 class="black-text text-darken-4 center">Welcome to the Maldives</h3>
				<div class="row">

					<div class="col l6 s12">

						<ul class="thumbnails">
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/12797827_1737679353144319_670786042_n.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/12822264_1681501555462526_1124079634_n.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/14578890020721.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/20131221_084319.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/20150213_072650.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/2961300751_5beafb75b9_b.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/53852664.MaldivesNov051372.JPG" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/Hukuru-Miskiiy-Maldives.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/IMG_0965.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/Maldives-National-Museum.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/Male-Market.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/Male1.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/National-Museum-Maldives.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/National-Museum-in-Sultan-Park.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/Pic-5-1074x483.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/download.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/dsc_3722.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/gse_multipart17967.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/images.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/location-maldives-hulhumale.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/mald7-tombx.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/male-capital-of-the-maldives-republic.jpg" />
							</li>
							<li class="col m3 s6 l3">
								<img class="hoverable materialboxed" src="/images/about_maldives/utmeemu-ganduvaru.jpg" />
							</li>
						</ul>

					</div>
					<div class="col l6 s12">
						<p class="black-text text-darken-4 ">
							Welcome to the Maldives, where sands are white as the smiles of the locals, where fish swim happily in the warm waters of the Indian Ocean, where the weather is a dream, and the deep rays of the sun wait to engulf you their arms.
						</p>
						<p class="black-text text-darken-4 ">
							In ancient times, the shores of the Maldives welcomed lost travellers. Still welcoming, these shores remain, providing a tranquil haven for visitors.
						</p>
						<p class="black-text text-darken-4 ">
							<strong>(in thaana script):</strong><br>
							Hindhu emaa kandu therein, Mala fehi ruhgas hedhey
							Meemagey ufan bimey, Dheebu Dhivehi mee
							Kula ali maa Kandu therein, Ali raiy mui hen dhirey
							Meemagey ufan bimey, dheebu Dhivehi mee
						</p>
						<p class="black-text text-darken-4 "><strong>Translation:</strong><br>
							In horizon of the vast Indian Ocean grow green palms
							This is my homeland, this is the Maldives
							From the clear blue seas, we grow like pearls,
						This is my homeland, this is the Maldives
						</p>
						<p class="black-text text-darken-4 ">
						(Dheebu Dhivehi Mee, Old Maldivian Folk song)
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="teal lighten-4">
		<div class="container" id='contact_us'>
			<div class="section">
				<h3 class="white-text text-darken-4 center">Contact Us</h3>
                <div class="row">
                    <div class="col s12 l5">
						<p class="left-align light">

							MOODHU Holidays Maldives Pvt Ltd<br>
							H. Nareen, 2 ND FLOOR<br>
							Majeedhi Magu<br>
							MALE', REP. OF MALDIVES<br>
							<br>
							TEL: + 960 3337794<br>
							FAX: + 960 3324216<br>
							<br>
							E-MAIL: <a href="mailto:reservation@moodhu.com.mv">reservation@moodhu.com.mv</a><br>
							URL: <a href="www.moodhu.com">www.moodhu.com</a><br>

						</p>
					</div>
					<div class="col s12 l7 map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1989.6166138370536!2d73.51474402847175!3d4.174499999586403!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNMKwMTAnMjguMiJOIDczwrAzMCc1Ni4wIkU!5e0!3m2!1sen!2smv!4v1478866930274" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="">
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col s12 l6">
						<!-- <a class="twitter-grid" data-partner="tweetdeck" href="https://twitter.com/moodhu_mv/timelines/812985971243241472">Website</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> -->
						<a class="twitter-grid" data-partner="tweetdeck" href="https://twitter.com/moodhuholidays/timelines/812985971243241472">Website</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>
					<div class="col s12 l6">
						<iframe src="//users.instush.com/collage/?cols=7&rows=7&sl=true&user_id=1993794618&username=moodhu_holidays_mv&sid=-1&susername=-1&tag=-1&stype=mine&grd=false&gpd=6&drp=false&bg=transparent&space=true&rd=false&pin=true&t=999999UPvRuTQw1mSmcL4tRkCMg5eivu8Sp7_9n0O2xgomXYCOIKJlVktCf2KX6z1jqG4MRi7EjszpshM" allowtransparency="true" frameborder="0" scrolling="no" style="display:block;border:none;overflow:visible;width:100%;height:733px;margin: -10px 0;" ></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- <div class="parallax-container valign-wrapper">
		<div class="section no-pad-bot">
			<div class="container">
				<div class="row center">
					<h5 class="header col s12 light">Who wants to miss white beaches as clear as milk?</h5>
				</div>
			</div>
		</div>
		<div class="parallax"><img src="/images/2.jpg" alt="Unsplashed background img 3"></div>
	</div> -->

<style type="text/css">


#universal-content div.buttonContainer{ top: auto!important;bottom: 10px;right: 0px !important;}
	#loader-wrapper {
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	    z-index: 1000;
	    margin-top: 80px;
	}
	#loader {
	    display: block;
	    position: relative;
	    left: 50%;
	    top: 50%;
	    width: 150px;
	    height: 150px;
	    margin: -75px 0 0 -75px;
	    border-radius: 50%;
	    border: 3px solid transparent;
	    border-top-color: #3498db;

	    -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
	    animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
	}

    #loader:before {
        content: "";
        position: absolute;
        top: 5px;
        left: 5px;
        right: 5px;
        bottom: 5px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #e74c3c;

        -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    #loader:after {
        content: "";
        position: absolute;
        top: 15px;
        left: 15px;
        right: 15px;
        bottom: 15px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #f9c922;

        -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
          animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

    @-webkit-keyframes spin {
        0%   { 
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
    @keyframes spin {
        0%   { 
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }

	

    #search 
    {
    	position: relative;
    }
@media only screen and (min-width: 991px)
{

   	#search .search-toolbar
   	{
	    width: calc(100% + 290px);
    	margin-left: -145px;
   	}
	
}

   	#search .display
   	{
   		transition: margin-top .3s ease-in;
   	}

   	@media only screen and (min-width: 993px)
   	{
   		#search .display
	   	{
	   		/*margin-top: 60px;*/
	   	}

   	}
   	@media only screen and (max-width: 991px)
   	{
   		#search .display
	   	{
	   		/*margin-top: 100px;*/
	   	}
   	}

	span.special_offer {
		position: absolute;
		background: rgba(255, 0, 0, 0.6);
		color: white;
		padding: 10px;
		top: 10px;
		box-shadow: 2px 4px 5px #5f5e5e;
		font-weight: bold;
	}

   	.yellow.lighten-4 {
	    background-color: rgba(255, 249, 196, 0.7) !important;
	}

   	#best-deals-form 
   	{
   		background: 
   	}

   	#best-deals-form .input-field label
   	{
   		color: #000;
   	}

   	#best-deals-form  .card-panel
   	{
   		padding: 10px;
   	}

	#best-deals-form input:not([type]), 
	#best-deals-form input[type=text], 
	#best-deals-form input[type=password], 
	#best-deals-form input[type=email], 
	#best-deals-form input[type=url], 
	#best-deals-form input[type=time], 
	#best-deals-form input[type=date], 
	#best-deals-form input[type=datetime], 
	#best-deals-form input[type=datetime-local], 
	#best-deals-form input[type=tel], 
	#best-deals-form input[type=number], 
	#best-deals-form input[type=search], 
	#best-deals-form textarea.materialize-textarea
	{
		border-bottom: 1px solid #000;
	}
	.center + img {
		width: 100% !important;
	}

	.footer .contact {
		list-style-type: none;
		margin: 0;
		text-align: left;
		color: #fff; }
	.footer .contact li {
		margin-bottom: 2rem; }
	.footer .contact li p {
		padding-left: 5rem; }
	.footer .contact i {
		position: absolute;
		background-color: #33383b;
		color: #fff;
		font-size: 2rem;
		border-radius: 50%;
		line-height: 1;
		margin: 0 0 0 -4rem;
		vertical-align: middle;
		padding: .25em .45em; }
    ul.thumbnails
    {
        margin-left: -10px;
    }

    ul.thumbnails li {
        float: left;
        padding: 8px !important;
        margin: -.3rem 0px !important;
    }
	img.materialboxed {
		object-fit: cover;
		height: 100px;
        width: calc(100% + 0.75rem);
	}
	img.activator {
	    object-fit: cover;
	}
</style>
	
