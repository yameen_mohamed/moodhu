@extends('layouts.detail')

@section('content')
    <div class="container below_icon_container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <h3>{{$model->getName()}}</h3>
                        <p>
                        @if(count($model->getCategories()))
                            @foreach($model->getCategories() as $cat)
                                <div class="chip">
                                    {{$cat->getName()}}
                                </div>
                            @endforeach
                        @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>

                @if(count($model->getPhotos()))
        <div class="slider">
            <ul class="slides">
                @foreach($model->getPhotos() as $photo)
                    <li>
                        <img src="{{$photo->getImageUrl()}}"> <!-- random image -->
                    </li>
                @endforeach
            </ul>
        </div>
                    @endif

    </div>
    <div>
        <div class="container">
            <div class="section">

                <p>{!!  $model->getDescription() !!}</p>

            </div>
        </div>
    </div>

@endsection

@section('nav')
    <li><a href="#search"> <i class="material-icons">search</i> </a></li>
@stop
