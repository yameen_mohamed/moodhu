@extends('layouts.detail')

@section('content')
    <div class="container below_icon_container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <h5>{{$model->getName()}} @if($model->getRating()) - @endif  @for ($i = 0; $i < $model->getRating(); $i++)
                                <i class="orange-text material-icons">star</i>
                            @endfor
                        </h5>
                            <small><i class="material-icons">location_on</i> {{$model->getDestination()->getName()}} / {{$model->getDestination()->getRegion()->getName()}}</small>
                    </div>
                </div>
            </div>
        </div>

        @if(count($model->getPhotos()))
            <div class="row">
                <ul class="bxslider">
                    @foreach($model->getPhotos() as $photo)
                        <li>
                            <img src="{{$photo->getImageUrl()}}"> <!-- random image -->
                        </li>
                    @endforeach
                </ul>

                <div class="bx-pager" id="bx-pager">
                    @foreach($model->getPhotos() as $index => $photo)
                        <a data-slide-index="{{$index}}" href="">
                            <img src="{{$photo->getImageUrl()}}">
                        </a>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="row">
            {!!   $model->getDescription()  !!}

        </div>


        <div class="row">
            <h5>Features</h5>
            <div class="divider"></div>
            

            @foreach($model->getProperties() as $attr=>$value)
                @if(is_array($value))
                    <h6><strong>{{ str_replace('_',' ',title_case($attr))}}</strong></h6>
                    <div class="row">
                        <ul>
                            
                        @foreach($value as $f => $v)
                            <li class="col l4 s12">
                                @if($attr == 'distances_from')
                                <i style="font-size: 15px" class="blue-text material-icons">transfer_within_a_station</i>
                                @else
                                <i style="font-size: 15px" class="blue-text material-icons">check_circle</i> 
                                @endif
                                {{ str_replace('_',' ',title_case($f))}}
                                @if($attr == 'distances_from')
                                    <span>({{$v}}m)</span>
                                @endif
                            </li>

                            
                        @endforeach
                        </ul>

                    </div>
                @else
                    <tr  style="vertical-align: to;text-indent: 20px;">
                        <td>{{str_replace('_',' ',title_case($attr))}}</td>
                        <td>{{ $value == 1 ? "Yes" : $value }}</td>
                    </tr>
                @endif
            @endforeach



        <div class="row">
        @if($model->hasPolicies())
            @foreach($model->getPolicies() as $attr=>$value)
                <h5>{{str_replace('_',' ',title_case($attr))}}</h5>
                <div class="divider"></div>
                <p>
                   {!! $value  !!} 
                </p>
            @endforeach
            @endif
        </div>

        @if($model->getLocation())
        <div class="row">
            <div id="map" style="width: 100%; height: 400px"></div>
        </div>
        @endif



    
        <div class="row">
            <h5>Click Preferred Room Type</h5>
            <div class="divider"></div>
            <div class="row">
            @foreach($model->getUnits() as $index=>$unit)
                <div class="col s12  l6 hoverable">
                    <h6 class="modal-trigger waves-effect" data-item="modal{{$index}}">
                        <i class="blue-text tiny material-icons">label_outline</i> {{$unit->getDescription()}}
                    </h6>
                    @include('partials.unit',compact('index', 'unit'))
                </div>
            @endforeach
            </div>
        </div>



    </div>

@endsection

@section('nav')
    <li><a href="#search"> <i class="material-icons">search</i> </a></li>
@stop

@section('scripts')
    @if($model->getLocation())
        <script type="text/javascript"
            src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyA_JcVYZElJzqoCP4-XJRXGBonWCYBdmUU"></script>
    @endif
    <script>
        $(document).ready(function(){
//            $('.modal').modal();
            $('.modal').modal(
                    {
                        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                            $('.unitSlider', modal).bxSlider({
                                pagerCustom: '#bx-pager_'+$(modal).attr('id'),
                                adaptiveHeight: true
                            });
                        },
                    }
            );
            $('.modal-trigger').click(function()
            {
               $('#' + $(this).data('item')).modal('open');
            });
            setTimeout(function()
            {

                $('.bxslider').bxSlider({
                    pagerCustom: '#bx-pager',
                    adaptiveHeight: true
                });


            }, 100);

            @if($model->getLocation())
            var myOptions = {
                zoom: 16,
                center: new google.maps.LatLng({{$model->getLocation()->getLatitude()}}, {{$model->getLocation()->getLongitude()}}),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                key: 'AIzaSyA_JcVYZElJzqoCP4-XJRXGBonWCYBdmUU'
            };

            var map = new google.maps.Map(document.getElementById("map"), myOptions);
            @endif
        });
    </script>
@stop
