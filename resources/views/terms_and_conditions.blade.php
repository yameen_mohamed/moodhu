@extends('layouts.detail')

@section('content')
<div class="container below_icon_container">

    <p class=MsoNormal><h3>TERMS AND CONDITIONS OF USE AND PURCHASE OF
PRODUCTS AND SERVICES</h3><span><br>
Welcome to the Online Payment System website of the Moodhu Holidays Maldives
Private Limited. Use of this site is governed by the Terms and Conditions set
forth. PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THIS
WEBSITE. The information and materials provided by Moodhu Holidays Maldives may
be used for informational purposes only. By using, accessing or downloading
materials from this website you agree to follow the terms and provisions as
outlined in this legal notice, which apply to all visits to this Online Payment
System website, both now and in the future. Moodhu Holidays Maldives may at any
time revise and update the Terms and Conditions. You are encouraged to periodically
visit this page to review the most current Terms and Conditions to which you
are bound. If you do not agree to these Terms and Condition of Use, please do
not use this website.<br>
<br>
<h5><span style='font-family:Tahoma'>PRODUCTS AND SERVICES OFFERED FROM
THIS SITE</span></h5><br>
The purpose of this Online Payment System website is to collect online payments
from customers for products sold or services rendered on behalf of Moodhu
Holidays Maldives. In particular Moodhu Holidays Maldives Private Limited
(www.moodhu.com) uses this site to collect payments from its customers for the
room bookings sold via its website and for other tourism related activities
conducted by the company.<span class=apple-converted-space>&nbsp;</span><br>
<br>
<h5><span style='font-family:Tahoma'>SETUP AND PAYMENT</span></h5><br>
Customer represents and warrants that (<span class=SpellE>i</span>) the credit
card information supplied is true, correct and complete and (ii) charges
incurred by the customer will be <span class=SpellE>honoured</span> by the
customer's credit card company and (iii) customer shall pay charges incurred by
customer at the amounts in effect at the time incurred, including all
applicable taxes. Customer shall be responsible for all charges incurred
through use of this website. For successful credit or debit card payments,
Moodhu Holidays Maldives will provide customer a receipt for the payment via
online and email. Customer is advised to keep a copy of the receipt for records.<br>
<br>
<h5><span style='font-family:Tahoma'>RESTRICTIONS</span></h5><br>
You may view, download and copy information and materials available on this
website solely for your personal, non-commercial use. You may also use such
material within your organization in connection with the support of Moodhu
Holidays MaldivesÕ products. As a condition of use, you agree not to modify or
revise any of the material in any manner, and to retain all copyright and other
proprietary notices as contained in the original materials on any copies of the
materials. No other use of the materials or information is authorized. Any
violation of the foregoing may result in civil and/or criminal liabilities. You
must also not misuse this website. You will not: commit or encourage a criminal
offence; transmit or distribute a virus, <span class=SpellE>trojan</span>,
worm, logic bomb or post any other material which is malicious, technologically
harmful, in breach of confidence or in any way offensive or obscene; hack into
any aspect of the website; corrupt data; cause annoyance to other users;
infringe upon the rights of any other person's proprietary rights; send any
unsolicited advertising or promotional material, commonly referred to as
&quot;spam&quot;; or attempt to affect the performance or functionality of any
computer facilities of or accessed through this Website.<br>
<br>
We will not be liable for any loss or damage caused by a distributed
denial-of-service attack, viruses or other technologically harmful material
that may infect your computer equipment, computer programs, data or other
proprietary material due to your use of this Website or to your downloading of
any material posted on it, or on any website linked to it.<br>
<br>
<h5><span style='font-family:Tahoma'>OWNERSHIP OF INFORMATION AND MATERIALS</span></h5><br>
The information and any materials (including white papers, press releases, data
sheets, product descriptions, and FAQs) available on or from this website are
the copyrighted works of Moodhu Holidays Maldives or its partners or its
service providers, and any unauthorized use of that information or materials
may violate copyright, trademark and other laws. Any rights not expressly
granted herein are reserved.<br>
<br>
<h5><span style='font-family:Tahoma'>TRADEMARK INFORMATION</span></h5><br>
Moodhu Holidays MaldivesÕ trademarks may be used only with written permission
from Moodhu Holidays Maldives Private Limited. All other trademarks, brands,
and names are the property of their respective owners. Except as expressly
specified in these terms and legal restrictions, nothing contained herein shall
be construed as conferring by implication, estoppel or otherwise any license or
right under any patent, trademark, copyright or any proprietary rights of
Moodhu Holidays Maldives or any third party.<br>
<br>
<h5><span style='font-family:Tahoma'>LINKS TO OTHER WEBSITES</span></h5><br>
As a convenience and to make the Moodhu Holidays MaldivesÕ website truly
service oriented we have included links to complementary sites on the Internet.
These sites are owned and operated by third parties. Moodhu Holidays Maldives
makes no representation and is not responsible for the availability of, or
content located on or through, these third party sites. A third party link from
the Moodhu Holidays Maldives website is not an indication that Moodhu Holidays
Maldives endorses the third party or its site, or has any affiliation with or
between Moodhu Holidays Maldives and the third party hosting site.<span
                    class=apple-converted-space>&nbsp;</span><br>
<br>
<h5><span style='font-family:Tahoma'>PRIVACY POLICY</span></h5><br>
Our privacy policy, which sets out how we will use your information, can be
found at Moodhu Holidays MaldivesÕ Online Payment System Privacy Policy. By
using this Website, you consent to the term-head described therein and warrant
that all data provided by you is accurate.<br>
<br>
<h5><span style='font-family:Tahoma'>MODIFICATION, REFUND AND CANCELLATION POLICY</span></h5><br>
Moodhu Holidays Maldives has agreed with accommodation suppliers that
reservations may be altered ONCE up to 72 business hours (Saturday - Thursday,
9am - 5pm Maldives time) before the confirmed date free of charge, unless
otherwise stated differently on the documents provided at the time of
confirming the booking. Moodhu Holidays Maldives, at its discretion, reserve
the right to charge $10.00 per reservation on any subsequent amendments and the
accommodation supplier may look to levy its own charge. Typically, no refunds
will be given by the accommodation supplier for changes within 72 business
hours of the arrival date in the Republic of Maldives.<br>
<br>
All requests<span class=apple-converted-space>&nbsp;</span><u>WILL BE CONFIRMED</u><span
                    class=apple-converted-space>&nbsp;</span>if the accommodation is available and
proof of sending a subsequent cancellation request is no guarantee it has been
received by Moodhu Holidays Maldives. All cancellations will generate a
cancellation number that must be quoted for any deduction from invoice to take
place. Moodhu Holidays Maldives will not be held responsible for any errors on
the part of the Customer in this procedure. Moodhu Holidays Maldives has agreed
a scale of charges with the accommodation suppliers. All cancellation charges
are based on the date of arrival in the Republic of Maldives (or the first date
at which Moodhu Holidays Maldives services are to be provided).&nbsp; This date
is specified on the booking confirmation.<br>
<br>
<h5><span style='font-family:Tahoma'>CONSUMER DATA PRIVACY POLICY</span></h5><br>
As contained in the Moodhu Holidays Maldives Online Payment System Privacy
Policy.<br>
<br>
<h5><span style='font-family:Tahoma'>SECURITY</span></h5><br>
As contained in the Moodhu Holidays MaldivesÕ Online Payment System Privacy
Policy.<br>
<br>
<h5><span style='font-family:Tahoma'>EDITING, DELETING, AND MODIFICATION OF
INFORMATION ON SITE</span></h5><br>
Moodhu Holidays Maldives reserves the right in its sole discretion to edit or
delete any information or content appearing on this site and subsidiaries
websites and to remove any goods and services for sale. Upon notice published,
Moodhu Holidays Maldives and its subsidiaries may modify this Terms and
Conditions, or prices, and may discontinue or revise any or all aspects of this
site in its sole discretion and without prior notice. Modification of this
Terms and Conditions will be deemed effective upon publication on this site
with respect to transactions occurring after said date.<br>
<br>
<h5><span style='font-family:Tahoma'>RIGHT TO REFUSE</span></h5><br>
Moodhu Holidays Maldives and its subsidiaries reserve the right in its sole
discretion to refuse sale of any goods or services at any time. Sale of any
goods or services is subject to availability.<br>
<br>
<h5><span style='font-family:Tahoma'>INDEMNIFICATION</span></h5><br>
Customer agrees to indemnify, defend and hold Moodhu Holidays Maldives and its
affiliates, licensors and suppliers harmless from any liability, loss, claim
and expense, including reasonable attorney's fees, related to a customer's
violation of this Terms and Conditions or use of this site.<br>
<br>
<h5><span style='font-family:Tahoma'>MARKETING MATERIALS</span></h5><br>
Moodhu Holidays Maldives and its subsidiaries may from time to time send the
customer information about new products, news, information about special offers
and other material materials via email, SMS or telephone.<br>
<br>
<h5><span style='font-family:Tahoma'>DISCLAIMER</span></h5><br>
Moodhu Holidays Maldives Internet team strive to provide you with useful,
accurate, and timely information on this website. Accordingly, Moodhu Holidays
Maldives have attempted to provide accurate information and materials on this
website but assume no responsibility for the accuracy and completeness of that
information or materials. Moodhu Holidays Maldives may change the content of
any information or materials available at this website, or to the products
described in them, at any time without notice. However, Moodhu Holidays
Maldives makes no commitment to update the information or materials on this
website which, as a result, may be out of date. Information and opinions
expressed in bulletin boards or other forums are not necessarily those of
Moodhu Holidays Maldives or its subsidiaries. Neither Moodhu Holidays Maldives,
nor its officers, directors, employees, agents, distributors, or affiliates are
responsible or liable for any loss damage (including, but not limited to,
actual, consequential, or punitive), liability, claim, or other injury or cause
related to or resulting from any information posted on Moodhu Holidays
Maldives' website. Moodhu Holidays Maldives reserves the right to revise these
terms and/or legal restrictions at any time. Customers are responsible for
reviewing this page from time to time to ensure compliance with the
then-current terms and legal restrictions because they will be binding on you.
Certain provisions of these terms and legal restrictions may be superseded by
expressly designated legal notices or terms located on particular pages of this
website. ALL INFORMATION AND MATERIALS AVAILABLE AT THIS WEBSITE ARE PROVIDED
&quot;AS IS&quot; WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR
IMPLIED, AND MOODHU HOLIDAYS MALDIVES AND ITS SUBSIDIARIES DISCLAIMS ALL
WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF
INTELLECTUAL PROPERTY OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE
PRACTICE. IN NO EVENT SHALL MOODHU HOLIDAYS MALDIVES BE LIABLE FOR ANY DAMAGES
WHATSOEVER (INCLUDING, WITHOUT LIMITATION, INDIRECT, SPECIAL, CONSEQUENTIAL OR
INCIDENTAL DAMAGES OR THOSE RESULTING FROM LOST PROFITS, LOST DATA OR BUSINESS
INTERRUPTION) ARISING OUT OF THE USE, INABILITY TO USE, OR THE RESULTS OF USE OF
THIS WEBSITE, ANY WEBSITES LINKED TO THIS WEBSITE, OR THE MATERIALS OR
INFORMATION CONTAINED AT ANY OR ALL SUCH WEBSITES, WHETHER BASED ON WARRANTY,
CONTRACT, TORT OR ANY OTHER LEGAL THEORY AND WHETHER OR NOT ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES. IF CUSTOMERÕS USE OF THE MATERIALS OR INFORMATION
ON THIS WEBSITE RESULTS IN THE NEED FOR SERVICING, REPAIR OR CORRECTION OF
EQUIPMENT OR DATA, YOU ASSUME ALL COSTS THEREOF.<br>
<br>
<h5><span style='font-family:Tahoma'>USE OF INFORMATION</span></h5><br>
Moodhu Holidays Maldives reserves the right, and customer authorizes Moodhu
Holidays Maldives, to the use and assignment of all information regarding
customerÕs use of this website and all information provided by customer,
subject to applicable law. All comments, feedback, information or materials
submitted to Moodhu Holidays Maldives through or in association with this
website shall be considered non-confidential and Moodhu Holidays MaldivesÕ
property. By submitting such comments, information, feedback, or materials to
Moodhu Holidays Maldives, you agree to a no-charge assignment to Moodhu
Holidays Maldives of worldwide rights to use, copy, modify, display and
distribute the submissions. Moodhu Holidays Maldives may use such comments,
information or materials in any way it chooses in an unrestricted basis.<br>
<br>
<h5><span style='font-family:Tahoma'>GOVERNING LAW, JURISDICTION, AND VENUE</span></h5><br>
This Agreement shall be governed by and construed in accordance with the laws
of the Republic of Maldives. The appropriate courts in the Maldives shall have
exclusive jurisdiction and venue over any dispute arising out of or relating to
this Agreement, and each party hereby consents to the jurisdiction and venue of
such courts.<span class=apple-converted-space>&nbsp;</span><br>
<br>
<h5><span style='font-family:Tahoma'>GENERAL PROVISIONS</span></h5><span
                    class=apple-converted-space><b>&nbsp;</b></span><br>
If any provision of this agreement is deemed void, unlawful or otherwise
unenforceable for any reason, that provision shall be severed from this
agreement and the remaining provisions of this agreement shall remain in force.
This contains the entire agreement between the customer and Moodhu Holidays
Maldives concerning the use of the site, and the agreement shall not be
modified, except in writing, signed by both parties.</span></p>

</div>

@stop


