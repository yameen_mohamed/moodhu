@section('content')
<form action="{{route($resource,$id)}}" method="{{isset($method) ? $method : 'POST'}}">
	<input name="_method" type="hidden" value="{{isset($method) ? $method : 'delete'}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<blockquote>
	  <p>You are about to delete a record from the server.</p>
	  <small>This action may not be recoverable!</small>
	</blockquote>
	<p>
		<i>Details</i>
		<p>
			<blockquote>
	  			{!! is_null($model) ? '-' : $model->info() !!}
	  		</blockquote>
		</p>
	</p>
	<p>
		<strong>Are you sure?</strong>
	</p>
	<input type="hidden" name="ref" value="{{Request::server('HTTP_REFERER')}}" >
	<input class="btn btn-danger" type="submit">
</form>  
@stop
@extends('welcome')