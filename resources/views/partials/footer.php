<style>
    ul.footer-nav.no-margin li {
        border: none;
    }
    
    .social-icon
    {
        position: relative;
        width: 50px;
        overflow: hidden;
        display: inline;
    }
    .social-icon i.fa:hover{
        background: #ff9800;

    }
    .social-icon i.fa {
        background: rgb(239, 218, 188);
        border-radius: 50%;
        width: 30px;
        height: 30px;
        color: #fff;
        line-height: 200%;
        text-align: center;
    }

    .social-icon .hidden {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
        width: 30px;
        overflow: hidden;
    }
    ul.horizontal {
        margin-left: -20px;
    }

    ul.horizontal li {
        float: left;
        height: 70px;
    }
    footer.page-footer h6 {
        font-weight: bold;
    }
    span.small {
        font-size: 12px !important;
        font-weight: 100;
    }
</style>
<footer class="page-footer yellow lighten-5">
    <div class="container">
        <div class="row" style="margin-bottom: 0;">
            <div class="col s12 l4">
                <div class="row">
                    <div class="">
                        <h6 class="orange-text">
                            Follow Us
                        </h6>
                    </div>
                    <div style="margin-top: 20px;">
                    <div class="social-icon">
                        <i class="fa fa-linkedin" aria-hidden="true" style="background: #1447bd;"></i>
                        <div class="hidden">

                            <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                                <script type="IN/FollowCompany" data-id="9194922"></script>
                        </div>
                    </div>
                    <div class="social-icon">
                        <i class="fa fa-instagram" aria-hidden="true" style="background: #795548;"></i>
                        <div class="hidden">

                            <style>.ig-b- { display: inline-block; }
                                .ig-b- img { visibility: hidden; }
                                .ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
                                .ig-b-v-24 { width: 137px; height: 24px; background: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24.png) no-repeat 0 0; }
                                @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                                    .ig-b-v-24 { background-image: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24@2x.png); background-size: 160px 178px; } }</style>
                            <a href="https://www.instagram.com/moodhu_holidays_mv/?ref=badge" class="ig-b- ig-b-v-24"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>
                        </div>
                    </div>
                    <div class="social-icon">
                        <i class="fa fa-facebook" aria-hidden="true" style="background: #1447bd;"></i>
                        <div class="hidden">
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=117478450770";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                                <div class="fb-like" data-href="https://business.facebook.com/moodhuholidaysmaldives.worldwide" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
                                <div class="fb-like" data-href="https://business.facebook.com/moodhuholidaysmaldives.locals" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
                        </div>
                    </div>
                        <div class="social-icon">
                            <i class="fa fa-twitter" aria-hidden="true" style="background: #03A9F4;"></i>
                            <div class="hidden">
                                <a href="https://twitter.com/moodhu_mv" class="twitter-follow-button" data-show-count="false">Follow @moodhu_mv</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 80px;margin-bottom: 0;">
                    <div class="">
                        <h6 class="orange-text">
                            Member of
                        </h6>

                        <ul class="horizontal">
                            <li class='valign-wrapper'>
                                <a href="http://www.visitmaldives.com/en/travel_agents" style="text-decoration: none;" target="_blank">
                                    <img height="70px" src="/images/maldives.png" alt="">
                                </a>
                            </li>
                            <li class='valign-wrapper'>
                                <a href="http://www.atamaldives.net" style="textdecoration: none;" target="_blank">
                                    <img height="30px" src="/images/ata.png" alt="">
                                </a>
                            </li>
                            <li class='valign-wrapper'>
                                <a href="http://www.matato.org/Members.aspx" style="textdecoration: none;" target="_blank">
                                    <img height="30px" src="/images/matato.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col l4 s12">
                <div class="row">
                    <div class="center">
                        <h6 class="orange-text">
                            Our Awards
                        </h6>
                    </div>
                </div>
                <div class="row">

                    <div class="col l12 s12 center">
                            <img style="width: 150px" class="activator" src="/awards/sata_pp.png" alt="">
                    </div>
                </div>
                <div class="row">
                    <small>
                            <p class="brown-text text-darken-4 center" style="margin: 0;line-height: 0">
                                Platinum Partner of Adaaran Resorts, Maldives 2016
                            </p>
                            <p class="brown-text text-darken-4 center" style="margin: 0; line-height: 3em">
                                SATA Leading Tour Operator - Maldives 2016
                            </p>
                    </small>
                </div>
            </div>
            
            <div class="col l4 s12">
                <div class="row">
                    <div class="nav-wrapper">
                        <ul class="footer-nav right">
                            <li><a class="blue-text text-darken-4" href="/#accommodations"><small>Accommodations</small></a></li>
                            <li><a class="blue-text text-darken-4" href="/#about_maldives"><small>Maldives</small></a></li>
                            <li><a class="blue-text text-darken-4" href="/#about_us"><small>About Us</small></a></li>
                            <li><a class="blue-text text-darken-4" href="/#contact_us"><small>Contact Us</small></a></li>
                        </ul>
                    </div>
                </div>
                <div class="ro" style="margin-top: 88px;">
                <small class="right">
                        <p class="" style="text-align: right;line-height: 15px;"> 
                         <span>
                             Our
                         </span>
                            <a class="" href="/terms_and_conditions">Terms & Conditions  </a>
                            <span>and</span>
                            <a class="" href="/privacy_policy">Privacy Policy</a> 
                            <span class="">
                                <br>
                            for Online Payments.
                            </span>
                            
                        </p>
                </small>
                   
                </div>
                <div class="row">
                    <div class="nav-wrapper">
                        <ul class="footer-nav right no-margin">
                            <li class="right">
                                <span class="right orange-text small">Powered By</span><br>
                                <img height="20px" src="/images/logo_MPG.jpg" alt="">
                            </li>
                            <li>
                                <span class="right orange-text small">We accept</span><br>                            
                                <img height="20px" src="/images/logo_visa.jpg" alt="">
                                <img height="20px" src="/images/logo_mastercard.jpg" alt="">
                                <img height="20px" src="/images/logo_amex.jpg" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container orange-text text-darken-4">
            All rights reserved by <a class="brown-text text-darken-4" href="../../index.html">
              <small>
                Moodhu Holidays Maldives
              </small>
            </a>
        </div>
    </div>
    <!-- <a href="http://www.beyondsecurity.com/vulnerability-scanner-verification/moodhu.com"><img src="https://seal.beyondsecurity.com/verification-images/moodhu.com/vulnerability-scanner-2.gif" alt="Website Security Test" border="0" /></a> -->
</footer>

<script type="text/javascript">
var _paq = _paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']); 
(function() {
  var u="//comelite.net/piwik/";
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setSiteId', '5']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
})();

(function(w,d,l){
  var s=d.createElement('script'),r=(Date.now()/1000|0),h=d.getElementsByTagName('script')[0];
  s.async=1;s.src=l+'?'+r;h.parentNode.insertBefore(s,h);
})(window,document,'//universalchat.herokuapp.com/chat.js/32/5');
</script>