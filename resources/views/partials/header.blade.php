<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<div class="navbar-fixd">
    <nav class="white" role="navigation">
        <div class="row nav-wrapper container">
            <a id="logo-container" href="/" class="brand-logo">
                <img height="180px" style="position: absolute;z-index: 11;padding: 10px;background: #fff;" src="/images/logo_x.png">
            </a>
            <ul class="right hide-on-med-and-down">

                @foreach(\App\Navigation::all() as $menu)
                <li><a href="{{$menu->link}}">{{$menu->title}}</a></li>
                @endforeach
                <li><a href="/#accommodations">Accommodations</a></li>
                <li><a href="/#about_maldives">Maldives</a></li>
                <li><a href="/#about_us">About Us</a></li>
                <li><a href="/#contact_us">Contact Us</a></li>
                <!-- <li><a href="/#search"> <i class="material-icons">book</i> </a></li> -->
            </ul>

            <ul id="nav-mobile" class="side-nav">
                <li style="height: 181px;border-bottom: 1px solid #ececec;">
                    <a href="/" style="background: #fff;">
                        <img height="180px" style="position: absolute;z-index: 11;padding: 10px;background: #fff;" src="/images/logo_x.png">
                    </a>
                </li>
                <li><a href="/#accommodations">Accommodations</a></li>
                <li><a href="/#about_maldives">Maldives</a></li>
                <li><a href="/#about_us">About Us</a></li>
                <li><a href="/#contact_us">Contact Us</a></li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</div>

