<!-- Modal Structure -->
<div id="modal{{$index}}" class="modal unit modal-fixed-footer">
    <div class="modal-content">
        <i class="modal-action modal-close material-icons right">close</i>
        <h4>{{$unit->getDescription()}}</h4>
        <div class="row">
            <div class="col l8 s12">
                @if(count($unit->getPhotos()))
                    <ul id="bxslider{{$index}}" class=" unitSlider">
                        @foreach($unit->getPhotos() as $photo)
                            <li>
                                <img style="min-height: 200px;width: 100%" src="{{$photo->getImageUrl()}}"> <!-- random image -->
                            </li>
                        @endforeach
                    </ul>

                    <div class="bx-pager" id="bx-pager_modal{{$index}}">
                        @foreach($unit->getPhotos() as $indexy => $photo)
                            <a data-slide-index="{{$indexy}}" href="">
                                <img src="{{$photo->getImageUrl()}}">
                            </a>
                        @endforeach
                    </div>

                    @push('sliders')
                        $('#bxslider{{$index}}').bxSlider({
                            pagerCustom: '#bx-pager{{$index}}',
                            adaptiveHeight: false,
                            width: '100%',
                        });
                    @endpush

                @endif
            </div>
            <div class="col l4 s12">
                @if(strlen($unit->getFacilities()))
                <div>
                    <strong>
                        Facilities:
                    </strong>
                    <div style="display: inline;">

                        {{ $unit->getFacilities() }}
                    </div>
                </div>
                @endif
                {{--<h5></h5>--}}
                {{--<table>--}}
                    {{--<tbody>--}}

                {{--@foreach($unit->getProperties() as $for=>$properties)--}}
                    {{--@foreach($properties as $key=>$property)--}}
                        {{--@if(!in_array($key, ['description','capacity']))--}}
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--<i style="font-size: 15px" class="blue-text material-icons">check_circle</i> {{ str_replace('_',' ',title_case($key))}}--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--@endforeach--}}

                    {{--</tbody>--}}
                {{--</table>--}}
                @if(count($unit->getSpecialOffers()))
                <h5>Special Offers</h5>
                <table>
                    <tbody>
                        @foreach($unit->getSpecialOffers() as $key => $special_offers)
                                <tr>
                                    <td>
                                        <i class="material-icons">stars</i> {{ $special_offers->getName() }}
                                    </td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a target="_blank" href="{{route('book',['UnitID' => $unit->getId()]).'?'.$_SERVER['QUERY_STRING'] }}" class="modal-action modal-close waves-effect waves-green btn">Inquire</a>
    </div>
</div>

