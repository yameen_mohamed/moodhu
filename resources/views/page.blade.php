@extends('layouts.detail')

@section('content')
    <div class="container below_icon_container">
 		<a href="{{route('page', $page->slug)}}">
    	<h3>
    	{{$page->title}}</h3>
 		</a>
 		<hr>
 		<span><small>Published On: {{$page->updated_at->diffForHumans()}}</small></span>
    	{!! $page->content !!}
    </div>
    <br>
    <br>
    <br>
@stop