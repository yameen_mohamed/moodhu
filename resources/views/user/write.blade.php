@extends('layouts.app')
@section('content')


<form method="post" action="{!! isset($model) ? route('users.update', $model->id) : route('users.store') !!}">
      <h3 class="heading">Update user</h3>
  <fieldset>
      <div class="row">
                <div class="col-md-6">
                <div class="form-group {!! $errors->has('name') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Name                    </label>

                        <input type="text" class="form-control" name="name" value="{!! old('name', isset($model) ? $model->name : null ) !!}" placeholder="Name">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>
                <div class="col-md-6">
                <div class="form-group {!! $errors->has('email') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Email                    </label>

                        <input type="text" class="form-control" name="email" value="{!! old('email', isset($model) ? $model->email : null ) !!}" placeholder="Email">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>

          <div class="col-md-6">
                <div class="form-group {!! $errors->has('mobile') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Mobile                    </label>

                        <input type="text" class="form-control" name="mobile" value="{!! old('mobile', isset($model) ? $model->mobile : null ) !!}" placeholder="Mobile Number">

                        @if ($errors->has('mobile'))
                            <span class="help-block">
                                <strong>{!! $errors->first('mobile') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>
                
          <div class="col-md-6">
                <div class="form-group {!! $errors->has('role') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Role                    </label>
                        <select class="form-control" name="role" >
                             <option value="0"> -- Role --</option>

                             <option @if(old('role', isset($model) ? $model->role : null ) == 'chair') selected=selected @endif value="chair">Chairman</option>
                             <option @if(old('role', isset($model) ? $model->role : null ) == 'admin') selected=selected @endif value="admin">Administrator</option>
                             <option @if(old('role', isset($model) ? $model->role : null ) == 'manager') selected=selected @endif value="manager">Manager</option>
                             <option @if(old('role', isset($model) ? $model->role : null ) == 'user') selected=selected @endif value="user">User</option>
                          </select>

                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{!! $errors->first('role') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>
            <div class="col-md-6">
                <div class="form-group {!! $errors->has('department') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Department                    </label>
                        <select class="form-control" name="department" >
                             <option value=""> -- Department --</option>

                             <option @if(old('department', isset($model) ? $model->department : null ) == 'Land') selected=selected @endif value="Land">Land</option>
                             <option @if(old('department', isset($model) ? $model->department : null ) == 'Marine') selected=selected @endif value="Marine">Marine</option>
                          </select>

                        @if ($errors->has('department'))
                            <span class="help-block">
                                <strong>{!! $errors->first('department') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>    
              
      </div>
      <div class="row">
        <div class="form-group">
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>Add
                </button>
            </div>
        </div>
      </div>
  </fieldset>
  {!! csrf_field() !!}
  @if(isset($model))
  <input type="hidden" name="_method" value="PUT"></input>
  @endif
</form>
<br>
@stop


