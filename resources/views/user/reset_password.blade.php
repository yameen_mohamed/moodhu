@extends('layouts.app')
@section('content')
	<form method="post" action="{!! route('users.post_reset_password') !!}">
      <h3 class="heading">Change password</h3>
	<fieldset>
		<div class="row">
		    <div class="col-md-6">
		        <div class="form-group {!! $errors->has('password') ? ' has-error' : '' !!}">
		            <label class=" control-label">
		              Password
		            </label>

		            <input type="password" class="form-control" name="password" value="{!! old('password', isset($model) ? $model->password : null ) !!}" placeholder="Password">

		            @if ($errors->has('password'))
		                <span class="help-block">
		                    <strong>{!! $errors->first('password') !!}</strong>
		                </span>
		            @endif
		        </div>
		  	</div>
		</div>
		<div class="row">
		    <div class="col-md-6">
		        <div class="form-group {!! $errors->has('password_confirmation') ? ' has-error' : '' !!}">
		            <label class=" control-label">
		              Confirm Password
		            </label>

		            <input type="password" class="form-control" name="password_confirmation" value="{!! old('password_confirmation', isset($model) ? $model->password_confirmation : null ) !!}" placeholder="Confirm Password">

		            @if ($errors->has('password_confirmation'))
		                <span class="help-block">
		                    <strong>{!! $errors->first('password_confirmation') !!}</strong>
		                </span>
		            @endif
		        </div>
		  	</div>
		</div>
		<div class="row">
	        <div class="form-group">
	            <div class="col-md-6">
	                <button type="submit" class="btn btn-primary">
	                    <i class="fa fa-btn fa-user"></i> Change Password
	                </button>
	            </div>
	        </div>
      	</div>
	</fieldset>
{!! csrf_field() !!}
  @if(isset($model))
  <input type="hidden" name="_method" value="PUT"></input>
  @endif
</form>
<br>
@stop