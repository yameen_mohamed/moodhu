@extends('layouts.app')
@section('content')
 <table class="table">  	<thead><tr>
 		<th>Name</th>
 		<th>Email</th>
 		<th>Approved</th>
 		<th>Role</th>
 		<th>Created On</th>
 		<th>Action</th>
 	</tr>
 	
	</thead>
 	<tbody>
 	@foreach($models as $model)
 	<tr>
 		<td>{{$model->name}}</td>
 		<td>{{$model->email}}</td>
 		<td>{{$model->approved ? 'Approved' : 'Pending' }}</td>
 		<td>{{$model->role}}</td>
 		<td>{{$model->created_at}}</td>
 		<td>
 			@if(!$model->approved)
 			<a href="/users/{{$model->id}}/activate" class="btn btn-success btn-sm glyphicon glyphicon-check"></a>
 			@else
 			<a href="/users/{{$model->id}}/activate" class="btn btn-danger btn-sm glyphicon glyphicon-remove"></a>
 			@endif
 			<a href="/users/{{$model->id}}/reset" class="btn btn-info btn-sm glyphicon glyphicon-lock"></a>
 			<a href="/users/{{$model->id}}/edit" class="btn btn-info btn-sm glyphicon glyphicon-pencil"></a>
 			<a href="/users/{{$model->id}}/destroy/confirm" class="btn btn-warning btn-sm glyphicon glyphicon-trash"></a>
 		</td>
 	</tr>
 	@endforeach	</tbody>
</table>
 {{ $models->appends(Request::get('q'))->render() }}
@stop
