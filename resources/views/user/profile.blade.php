@extends('layouts.app')
@section('content')
	<form method="post" action="{!! route('user.post_profile') !!}">
      <h3 class="heading">Update Profile</h3>
	<fieldset>
		<div class="row">
		    <div class="col-md-6">
		        <div class="form-group {!! $errors->has('email') ? ' has-error' : '' !!}">
		            <label class=" control-label">
		              Email
		            </label>

		            <input type="email" class="form-control" name="email" value="{!! old('email', isset($model) ? $model->email : null ) !!}" placeholder="Email">

		            @if ($errors->has('email'))
		                <span class="help-block">
		                    <strong>{!! $errors->first('email') !!}</strong>
		                </span>
		            @endif
		        </div>
		  	</div>
		</div>
		

		<div class="row">
		    <div class="col-md-6">
		        <div class="form-group {!! $errors->has('contact') ? ' has-error' : '' !!}">
		            <label class=" control-label">
		              Mobile
		            </label>

		            <input type="contact" class="form-control" name="contact" value="{!! old('contact', isset($model) ? $model->mobile : null ) !!}" placeholder="Mobile Number">

		            @if ($errors->has('contact'))
		                <span class="help-block">
		                    <strong>{!! $errors->first('contact') !!}</strong>
		                </span>
		            @endif
		        </div>
		  	</div>
		</div>

		<div class="row">
			<div class="col-md-6">
                <div class="form-group {!! $errors->has('department') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Department                    </label>
                        <select class="form-control" name="department" >
                             <option value=""> -- Department --</option>

                             <option @if(old('department', isset($model) ? $model->department : null ) == 'Land') selected=selected @endif value="Land">Land</option>
                             <option @if(old('department', isset($model) ? $model->department : null ) == 'Marine') selected=selected @endif value="Marine">Marine</option>
                          </select>

                        @if ($errors->has('department'))
                            <span class="help-block">
                                <strong>{!! $errors->first('department') !!}</strong>
                            </span>
                        @endif
                </div>
          </div> 
        </div>


		<div class="row">
	        <div class="form-group">
	            <div class="col-md-6">
	                <button type="submit" class="btn btn-primary">
	                    <i class="fa fa-btn fa-save"></i> Save Changes
	                </button>
	            </div>
	        </div>
      	</div>
	</fieldset>
	{!! method_field('put') !!}
{!! csrf_field() !!}
  @if(isset($model))
  <input type="hidden" name="_method" value="PUT"></input>
  @endif
</form>
<br>
@stop