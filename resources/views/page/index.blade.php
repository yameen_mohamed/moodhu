@extends('layouts.admin')
@section('content')
<h2>Pages
<a class="pull-right btn btn-sm btn-primary" href="{{route('pages.create')}}">New Page</a>
</h2>
 <table class="table">  	<thead><tr>
 		<th>Title</th>
 		<th>Slug</th>
 		<th>Created By</th>
 		<th>Status</th>
 		<th>Private Password</th>
 		<th>Created On</th>
 		<th>Action</th>
 	</tr>
 	
	</thead>
 	<tbody>
 	@foreach($models as $model)
 	<tr>
 		<td><a href="{{route('page', $model->slug)}}">{{$model->title}}</a></td>
 		<td>{{$model->slug}}</td>
 		<td>{{$model->user->name}}</td>
 		<td>{{$model->status}}</td>
 		<td>{{$model->private_password}}</td>
 		
 		<td>{{$model->created_at}}</td>
 		<td>
 			<a href="/admin/pages/{{$model->id}}/edit" class="btn btn-info btn-sm glyphicon glyphicon-pencil"></a>
 			<a href="/admin/pages/{{$model->id}}/destroy/confirm" class="btn btn-warning btn-sm glyphicon glyphicon-trash"></a>
 		</td>
 	</tr>
 	@endforeach	</tbody>
</table>

{!! $models->render() !!}
@stop
