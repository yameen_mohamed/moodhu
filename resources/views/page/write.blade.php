@extends('layouts.admin')
@section('content')


<form method="post" action="{!! isset($model->id) ? route('pages.update', $model->id) : route('pages.store') !!}">
	<div class="row">
		<div class="spread">
			<h3 class="heading">Create page</h3>
		</div>
	</div>

	<!-- {{ var_dump($errors) }} -->
	<fieldset>
			<div class="row">
								<div class="col-md-6">
								<div class="form-group {!! $errors->has('title') ? ' has-error' : '' !!}">
										<label class=" control-label">
											Title                    </label>

												<input type="text" class="form-control" name="title" value="{!! old('title', isset($model) ? $model->title : null ) !!}" placeholder="Title">

												@if ($errors->has('title'))
														<span class="help-block">
																<strong>{!! $errors->first('title') !!}</strong>
														</span>
												@endif
								</div>
					</div>
								<div class="col-md-6">
								<div class="form-group {!! $errors->has('slug') ? ' has-error' : '' !!}">
										<label class=" control-label">
											Slug                    </label>

												<input type="text" class="form-control" name="slug" value="{!! old('slug', isset($model) ? $model->slug : null ) !!}" placeholder="Slug">

												@if ($errors->has('slug'))
														<span class="help-block">
																<strong>{!! $errors->first('slug') !!}</strong>
														</span>
												@endif
								</div>
					</div>
								<div class="col-md-12">
								<div class="form-group {!! $errors->has('content') ? ' has-error' : '' !!}">
										<label class=" control-label">
											Content                    </label>

												<textarea id='page-content' rows=10 type="text" class="form-control" name="content" placeholder="Content">{!! old('content', isset($model) ? $model->content : null ) !!}</textarea>

												@if ($errors->has('content'))
														<span class="help-block">
																<strong>{!! $errors->first('content') !!}</strong>
														</span>
												@endif
								</div>
					</div>
								
							
			</div>
			<div class="row">
				<div class="form-group">
						<div class="col-md-6">
								<button type="submit" class="btn btn-primary">
										<i class="fa fa-btn fa-user"></i>Create
								</button>
						</div>
				</div>
			</div>
	</fieldset>
	{!! csrf_field() !!}
	@if(isset($model->id))
	<input type="hidden" name="_method" value="PUT"></input>
	@endif
</form>
@stop

@section('scripts')
<script type="text/javascript">

	tinymce.init({
	  selector: '#page-content',
	  height: 500,
	  theme: 'modern',
	  plugins: [
	    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
	    'searchreplace wordcount visualblocks visualchars code fullscreen',
	    'insertdatetime media nonbreaking save table contextmenu directionality',
	    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
	  ],
	  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
	  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
	  image_advtab: true,
	  templates: [
	    { title: 'Test template 1', content: 'Test 1' },
	    { title: 'Test template 2', content: 'Test 2' }
	  ],
	  content_css: [
	    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
	    '//www.tinymce.com/css/codepen.min.css'
	  ]
	 });
	// $('#page-content').wysihtml5({
	// 		events: {
	// 	        load: function(){
	// 	            var $body = $(this.composer.element);
	// 	            var $iframe = $(this.composer.iframe);
	// 	            iframeh = Math.max($body[0].scrollHeight, $body.height()) + 100;
	// 	            $('.wysihtml5-sandbox').height(iframeh);
	// 			},
	// 			change: function(){
	// 	            var $abody = $(this.composer.element);
	// 	            var $aiframe = $(this.composer.iframe);
	// 	            aiframeh = Math.max($abody[0].scrollHeight, $abody.height()) + 100;
	// 	            document.getElementsByClassName('wysihtml5-sandbox')[0].setAttribute('style','height: ' + aiframeh +'px !important');
	// 	        }
	// 	    }
	// });
</script>
@stop


