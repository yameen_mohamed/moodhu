@extends('layouts.detail')

@section('content')
<div class="container">
    <div class="row">
        <div class="slider">
            <ul class="slides">

                @foreach($model->photos as $photo)
                    <li>
                        <img src="{{$photo['image']}}"> <!-- random image -->
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <h3>{{$model->name}} -  @for ($i = 0; $i < $model->hotel['number_of_stars']; $i++)
                                             <i class="red-text material-icons">star</i>
                                        @endfor
                        <span>{{$model->getDestination()->getName()}}</span>
                    </h3>
                    <p>
                    @if(isset($model->categories))
                        @foreach($model->categories as $cat)
                            <div class="chip">
                                {{$cat}}
                              </div>
                        @endforeach
                     @endif
                    </p>
                </div>
            </div>
        </div>
    </div>



</div>
<div>
    <div class="container">
        <div class="section">

            <p>{!!  $model->description !!}</p>

        </div>
    </div>
</div>
<div class="orange">
    <div class="container">
        <div class="section">

            <h4>Distances</h4>
            @if(isset($model->distances_from))
            @foreach($model->distances_from as $from => $distance)
                <p>{{studly_case(title_case($from))}} - <i>{{$distance}}</i></p>
            @endforeach
                @endif
        </div>
    </div>
</div>
@endsection
