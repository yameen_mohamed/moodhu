@extends('layouts.detail')

@section('content')
    <div class="container below_icon_container">
    <p class=MsoNormal><h4><span>MOODHU HOLIDAYS MALDIVES ONLINE PAYMENT SYSTEM
WEBSITE PRIVACY POLICY</span></h4><span><br>
The Internet is an amazing tool. It has the power to change the way we live,
and we are starting to see that potential today. With only a few mouse-clicks,
you can follow the news, look up facts, buy goods and services, and communicate
with others from around the world. It is important to Moodhu Holidays Maldives
to help our customers retain their privacy when they take advantage of all the
Internet has to offer.<br>
<br>
We believe your business is no one else's. Your privacy is important to you and
to us. So we will protect the information you share with us. To protect your
privacy, Moodhu Holidays Maldives follows different principles in accordance
with worldwide practices for customer privacy and data protection. a) We will
not sell or give away your name, mail address, phone number, email address or
any other information to anyone. b) We will use adequate security measures to
protect your information from unauthorized users.<br>
<br>
<h5><span style='font-family:Tahoma'>NOTICE</span></h5><br>
When you use the website, we will be collecting information that personally
identifies you (personal information) or allows us to contact you. We use your
Personal Information for three primary purposes: a) For processing the online
payments, you make via the website. b) To help us create content most relevant
to you. c) To alert you to product upgrades, special offers, updated
information and other new services from Moodhu Holidays Maldives.<br>
<br>
<h5><span style='font-family:Tahoma'>CONSENT</span></h5><br>
You will not be able to use this website if you do not wish to provide personal
information, as we require your personal information to process online payments
you make via this website. By using the website you are giving us your consent
to use your personal information for the purposes stated above. Moodhu Holidays
Maldives occasionally allows its subsidiaries to offer our customers
information about their products and services, using email or SMS.<br>
<br>
<h5><span style='font-family:Tahoma'>ACCESS</span></h5><br>
We will provide you with the means to ensure that your personal information is
correct and current at the time of using this website. If you would like to
update any of the personal information held with us, you may email the updated
information to reservations@moodhu.com.<br>
<br>
<h5><span>SECURITY</span></h5><span
                    class=apple-converted-space><b>&nbsp;</b></span><br>
Moodhu Holidays Maldives has taken strong measures to protect the security of
your personal information and to ensure that your choices for its intended use
are honored. We take strong precautions to protect your data from loss, misuse,
unauthorized access or disclosure, alteration, or destruction. We guarantee
your e-commerce transactions to be safe and secure. When you place orders or
access your personal account information, you are utilizing secure server
software SSL, which encrypts your personal information before it is sent over
the Internet. SSL is one of the safest encryption technologies available. Your
personal information is never shared outside the company without your
permission, except under conditions explained above. Inside the company, data
is stored in password-controlled servers with limited access. Your information
may be stored and processed in Maldives or any other country where Moodhu
Holidays Maldives, its subsidiaries, affiliates or agents are located.<br>
<br>
<h5><span style='font-family:Tahoma'>ENFORCEMENT</span></h5><br>
If for some reason you believe Moodhu Holidays Maldives has not adhered to
these principles, please notify us by email at reservations@moodhu.com, and we
will do our best to determine and correct the problem promptly. Be certain the
words Privacy Policy are in the Subject line.<br>
<br>
<h5><span style='font-family:Tahoma'>WHAT WE DO WITH THE INFORMATION YOU
SHARE</span></h5><br>
When you use our website, you provide us with your contact information,
including your name and email address. We use this information to send you
updates about your order, questionnaires to measure your satisfaction with our
service and announcements about new and exciting services that we offer. When
you order from us, we ask for your credit card number and billing address. We
use this information only to bill you for the product(s)/services(s) you order
at that time. For your information, we do not save your credit card information
at any time and we do not have access to your credit card details at any time.
Your credit card details are captured and payment processed by Bank of Maldives
Plc on our behalf.<br>
<br>
We occasionally hire other companies to provide limited services on our behalf,
including packaging, mailing and delivering purchases, answering customer
questions about products or services, sending postal mail and processing event
registration. We will only provide those companies the information they need to
deliver the service, and they are prohibited from using that information for
any other purpose.<br>
Moodhu Holidays Maldives will disclose your personal information, without
notice, only if required to do so by law or in the good faith belief that such
action is necessary to: (a) conform to the edicts of the law or comply with
legal process served on Moodhu Holidays Maldives or the site; (b) protect and
defend the rights or property of Moodhu Holidays Maldives and its family of
Websites, and, (c) act in urgent circumstances to protect the personal safety
of users of Moodhu Holidays Maldives, its Websites, or the public.</span></p>

</div>
    @stop