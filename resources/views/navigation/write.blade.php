@extends('layouts.admin')
@section('content')


<form method="post" action="{!! isset($model->id) ? route('navigations.update', $model->id) : route('navigations.store') !!}">
  <div class="rw">
    <div class="spread">
      <h3 class="heading">Create navigation</h3>
    </div>
  </div>
  <fieldset>
      <div class="row">
                <div class="col-md-12">
                <div class="col-md-6 form-group {!! $errors->has('title') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Title                    </label>

                        <input type="text" class="form-control" name="title" value="{!! old('title', isset($model) ? $model->title : null ) !!}" placeholder="Title">

                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{!! $errors->first('title') !!}</strong>
                            </span>
                        @endif
                </div>
                </div>
                <div class="col-md-12">
                <div class="col-md-6 form-group {!! $errors->has('link') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Link                    </label>

                        <input type="text" class="form-control" name="link" value="{!! old('link', isset($model) ? $model->link : null ) !!}" placeholder="Link">

                        @if ($errors->has('link'))
                            <span class="help-block">
                                <strong>{!! $errors->first('link') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>
                <div class="col-md-12">
                <div class="col-md-6 form-group {!! $errors->has('parent_id') ? ' has-error' : '' !!}">
                    <label class=" control-label">
                      Parent                   </label>

                        <select class="form-control" name="parent_id">
                            <option value="">Select Parent Menu</option>

                          @foreach(\App\Navigation::all() as $menu)
                            <option value="{{$menu->id}}">{{$menu->title}}</option>
                          @endforeach
                        </select>
                        <!-- <input type="text" class="form-control" name="parent_id" value="{!! old('parent_id', isset($model) ? $model->parent_id : null ) !!}" placeholder="Parent Id"> -->

                        @if ($errors->has('parent_id'))
                            <span class="help-block">
                                <strong>{!! $errors->first('parent_id') !!}</strong>
                            </span>
                        @endif
                </div>
          </div>
          <div class="col-md-12">
              <div class="col-md-6">
                <div class="checkbox">
                  <label>
                    <input name="active" @if($menu->active) checked @endif type="checkbox"> Active
                    <input type="hidden" name="field[active]" value="active">
                  </label>
                </div>
              </div>
          </div>
      </div>
      <div class="rw">
        <div class="form-group">
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>Add
                </button>
            </div>
        </div>
      </div>
  </fieldset>
  {!! csrf_field() !!}
  @if(isset($model->id))
  <input type="hidden" name="_method" value="PUT"></input>
  @endif
</form>
@stop


