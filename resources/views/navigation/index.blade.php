@extends('layouts.admin')
@section('content')
<h2>Menus
<a class="pull-right btn btn-sm btn-primary" href="{{route('navigations.create')}}">New Menu</a>
</h2>
 <table class="table">  	<thead><tr>
 		<th>Title</th>
 		<th>Link</th>
 		<th>Parent Id</th>
 		<th>Active</th>
 		<th>Created On</th>
 		<th>Action</th>
 	</tr>
 	
	</thead>
 	<tbody>
 	@foreach($models as $model)
 	<tr>
 		<td>{{$model->title}}</td>
 		<td>{{$model->link}}</td>
 		<td>{{$model->parent_menu ? $model->parent_menu->title : '-'}}</td>
 		<td>{{$model->active ? 'Yes' : 'No'}}</td>
 		
 		<td>{{$model->created_at}}</td>
 		<td>
 			<a href="/admin/navigations/{{$model->id}}/edit" class="btn btn-info btn-sm glyphicon glyphicon-pencil"></a>
 			<a href="/admin/navigations/{{$model->id}}/destroy/confirm" class="btn btn-warning btn-sm glyphicon glyphicon-trash"></a>
 		</td>
 	</tr>
 	@endforeach	</tbody>
</table>

{!! $models->render() !!}

@stop
