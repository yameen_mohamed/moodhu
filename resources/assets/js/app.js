var app = angular.module('app',[])
	.filter('range', function(){
		return function(n,start) {
			var res = [];
			for (var i = (start|0); i < n; i++) {
				res.push(i);
			}
			return res;
		};
	});
app.directive('pickDate', function(){
	var restrictions = 'A';
	var isolatedScope  =  {
		ngModel: '=',
		format: '@',
		minDate: '@',
		maxDate: '@',
		from: '=',
		to: '=',
		onChange: '&',
	};

	return {
		restrict: restrictions,
		scope: isolatedScope,
		link: function(scope, element, attrs) {

			var from = [0,0,0]
			var to = true;


			function applyIt()
			{
				console.log('mindate', scope.minDate);
				if(scope.minDate)
				{
					var $pkr = $(element).pickadate({
						format: scope.format,
						min: new Date(scope.minDate),
						max:  scope.maxDate,
						editable: true,
						onSet: function() {
							var selected = this.get('select');

							if(selected)
							{
								//$(this).val(selected.obj);
								scope.ngModel = selected.year + '-' + (selected.month+1) + '-' + selected.date;
								scope.$apply();
							}

							this.close();
							if(scope.onChange)
							{
								scope.onChange({date: selected});
							}
						}
					});

					var picker = $pkr.pickadate('picker');


					$(element).on('focus', function()
					{
						picker.open();
					});
				}
			}

			applyIt();

			scope.$watch('minDate', function(){
				applyIt();
			});
			scope.$watch('maxDate', function(){
				applyIt();
			});

			//picker.set('select', scope.ngModel);
		}
	};
});

app.directive('pager', function(){
	return {
		restrict: 'E',
		replace: 'true',
		scope: {
			pageSize: '=',
			currentPage: '=',
			lastPage: '=',
			total: '=',
			pageChange: '&'
		},
		template: '<ul class="pagination">'
		+ '<li ng-click="changeToPreviousPage()" class="waves-effect" ng-class="{disabled: !hasPrevious()}">'
		+ '<a href="#!"><i class="material-icons">chevron_left</i></a>'
		+ '</li>'
		+ '<li class="waves-effect" ng-click="changePage(i)" ng-class="{active: isCurrent(i)}" ng-repeat="i in upperBound | range: lowerBound" class="waves-effect"><a>{{i}}</a></li>'
		+ '<li  ng-click="changeToNextPage()" ng-class="{disabled: !hasNext()}">'
		+ '<a href="#!"><i class="material-icons">chevron_right</i></a>'
		+ '</li>'
		+ '</ul>',
		link: function(scope, elem, attrs) {

			scope.hasNext = function()
			{
				console.log('scope.currentPage < scope.lastPage;' , scope.currentPage , scope.lastPage);
				return scope.currentPage < scope.lastPage;
			};

			scope.isCurrent = function(page)
			{
				return scope.currentPage == page;
			};

			scope.hasPrevious = function()
			{
				return scope.currentPage > 1;
			};

			scope.changeToPreviousPage = function()
			{
				var page = scope.currentPage - 1;
				scope.pageChange({page: page});
				scope.currentPage = page;
			};

			scope.changeToNextPage = function()
			{
				var page = scope.currentPage + 1;
				scope.pageChange({page: page});
				scope.currentPage = page;
			};

			scope.changePage = function(page)
			{
				scope.pageChange({page: page});
				scope.currentPage = page;
			};

			scope.$watch('currentPage', function(page)
			{
				Update();
			});

			function Update()
			{
				if(scope.currentPage+5 >= scope.lastPage)
				{
					scope.upperBound = scope.lastPage+1;
				}
				else
				{
					scope.upperBound = scope.currentPage+5;
				}

				console.log(scope.upperBound, scope.currentPage, scope.lastPage);

				if(scope.currentPage-5 <= 1)
				{
					scope.lowerBound = 1;
				}
				else
				{
					scope.lowerBound = scope.currentPage-5;
				}

				console.log("Bounds:",scope.lowerBound, scope.upperBound);
			}

			Update();

		}
	};
});