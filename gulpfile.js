const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss','./resources/assets/app.css')
      .scripts([
        './resources/assets/bower_components/jquery/dist/jquery.min.js',
        './resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js',
        './resources/assets/bower_components/tinymce/tinymce.min.js',
        './resources/assets/bower_components/tinymce/plugins/**/*.min.js',
        './resources/assets/bower_components/tinymce/themes/**/*.min.js',
        './resources/assets/bower_components/tinymce/skins/**/*.min.js',

        ],'public/js/admin.js')
       .scripts([
        './resources/assets/bower_components/angular/angular.min.js',
        './resources/assets/bower_components/jquery/dist/jquery.min.js',
       	'./resources/assets/bower_components/materialize/dist/js/materialize.min.js',
        './resources/assets/bower_components/bxslider-4/dist/jquery.bxslider.js',

        // './resources/assets/bower_components/chart.js/dist/Chart.min.js',
        './resources/assets/js/*.js'
       	])
       .styles(
        [
        './resources/assets/bower_components/bootstrap/dist/css/bootstrap.min.css',
        './resources/assets/bower_components/tinymce/skins/**/*.min.css',

        

        ], 'public/css/admin.css')
       .styles(
       	[
        './resources/assets/bower_components/materialize/dist/css/materialize.css',
       	'./resources/assets/bower_components/font-awesome/css/font-awesome.css',
            './resources/assets/bower_components/bxslider-4/dist/jquery.bxslider.min.css',

            './resources/assets/app.css'

       	]);
    mix.copy([
       	'./resources/assets/bower_components/materialize/dist/fonts',
        './resources/assets/bower_components/font-awesome/fonts',
       	'./resources/assets/bower_components/material-design-icons/iconfont',

    	]

    	, 'public/fonts');

    mix.copy([
        './resources/assets/bower_components/tinymce/skins/lightgray/fonts',

      ]

      , 'public/css/fonts');

    mix.copy([
        './resources/assets/bower_components/bxslider-4/dist/images/*.gif',
        ]
        , 'public/images');

    // mix.sass('./public/some.scss','./publoc/bootstrap.css')

});
